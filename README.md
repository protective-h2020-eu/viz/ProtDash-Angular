# ProtDash-Angular Dashboard

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.7.

It requires the installation of Angular CLI to run.

## Running

If you are running the project for the first time, you should run:

```
npm install
```

to install the dependencies.

___


You must point the Project to your neon-server installation for neon queries to work. First access the [environment file](/src/environments/environment.ts). You then change the IP and PORT to your neon-server IP and PORT.
```
export const environment = {
  production: false,
  neon: {
      hostname: 'localhost',
      url: 'http://Your_Neon_IP:PORT/neon'
  }
};

```
Data from CA MAIR is now also supported. A similar environment variable in the [environment file](/src/environments/environment.ts) can now be set for this too. This will allow you to access and visualize data from CA MAIR.
```
ca_mair: {
    hostname: 'YOUR_CA_MAIR_HOSTNAME',
    url: 'http://CA_MAIR_IP:CA_MAIR_PORT/api/'
}
```
___
To start the project run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

If you wish for others to be able to access the dashbaord by going to your IP, you should run
```
ng serve --host YOUR_IP_HERE
```
Others can then go to `http://YOUR_IP:4200/` to access the dashboard.

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
___
## Docker Image
Docker image available by executing:
```
docker pull registry.gitlab.com/protective-h2020-eu/viz/protdash-angular
```
## To Run Docker Container
Firstly, adjust the locations of your Neon Server deployment and your Ca Mair deployment in the docker-compose [file](docker-compose.yml) provided. Then run:
```
docker-compose up -d
```
### NOTE
Tested with Windows Docker installation. 

`Docker Version: 18.02.0-ce`. 

`docker-compose version 1.19.0, build 9e633ef3`
___
