FROM johnpapa/angular-cli:latest

RUN mkdir ./Prot-Dash-angular

COPY ./ ./Prot-Dash-angular/

RUN apk update
RUN apk add tini
ENTRYPOINT ["/sbin/tini", "--"]

WORKDIR ./Prot-Dash-angular

RUN mkdir node_modules
