#!/bin/sh
#   Use this script to start warden_filer container

if [ -n "$WAIT_FOR_HOST_AND_PORT" ]; then
	# for HOST_AND_PORT in ${WAIT_FOR_HOST_AND_PORT//,/ }; do
		HOST_AND_PORT=$WAIT_FOR_HOST_AND_PORT
	    echo "Started waiting for $HOST_AND_PORT"
        ./wait-for $HOST_AND_PORT --
	    echo "Finished waiting for $HOST_AND_PORT"
	# done
fi

WARDEN_FILER_CFG=warden_filer.cfg

cp /warden_client_conf/$WARDEN_FILER_CFG $WARDEN_FILER_CFG

sed -i "s|{{WARDEN_SERVER_URL}}|$WARDEN_SERVER_URL|g" $WARDEN_FILER_CFG
sed -i "s|{{WARDEN_CLIENT_NAME}}|$WARDEN_CLIENT_NAME|g" $WARDEN_FILER_CFG
sed -i "s|{{WARDEN_CLIENT_SECRET}}|$WARDEN_CLIENT_SECRET|g" $WARDEN_FILER_CFG

python warden_filer.py -c $WARDEN_FILER_CFG $WARDEN_FILER_FUNC

exec tail -f /dev/null
