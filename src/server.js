var express = require('express');
var MongoClient = require('mongodb').MongoClient;

var app = express();
var url = "mongodb://db:27017/";
console.log(url);


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.get('/', function (req, res) {
  res.send('Server for saving configs is running!');
});

app.post('/sendConfig', function (req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("mentat");
        dbo.collection("user_configs").update(
            {"userId": parseInt(req.query.userId)},
            {"userId": parseInt(req.query.userId),"widgetConf":JSON.parse(req.query.widgConf), "providerConf":JSON.parse(req.query.provConf)},
            { upsert : true },
            function(err, result) {
              console.log(err);
	      if (err) throw err;
              res.send(result);
              db.close();
            });
        });
});

app.get('/getConfig', function (req, res) {  
      MongoClient.connect(url, function(err, db) {
          if (err) throw err;
          var dbo = db.db("mentat");
          dbo.collection("user_configs").findOne({}, function(err, result) {
            if (err) throw err;
            res.send(result);
            db.close();
          });
        });
  });

app.listen(3000, function () {
  console.log('Server listening on port 3000');
});
