import { Type } from '@angular/core';

export interface CommonProviderDeclarationModel {
    id: string;
    name: string;
    provider?: Type<any>;
    providerConfigDefaults?: any;
}
