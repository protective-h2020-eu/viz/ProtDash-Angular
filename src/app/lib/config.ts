import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Observer } from 'rxjs/Observer';

export interface ConfigChange {
    previousValue: any;
    currentValue: any;
    firstChange: boolean;
}

export interface ConfigChanges {
    [key: string]: ConfigChange;
}

export class Config {
    private changes$: Subject<ConfigChanges>;
    private data: any = {};
    private initialChanges: ConfigChanges = {};

    static fromJSON(input: any) {
        return new Config(input);
    }

    constructor(defaults: any = {}) {
        this.changes$ = new Subject<ConfigChanges>();
        this.update(defaults);
    }

    update<T>(newValues: T) {
        const oldData = this.data;
        const newData = _.merge({}, oldData, newValues);
        const changes = this.calcChanges(oldData, newData);
        if (Object.keys(changes).length) {
            _.merge(this.initialChanges, changes);
            _.forIn(this.initialChanges, change => change.firstChange = true);
            this.changes$.next(changes);
        }
        this.data = newData;
    }

    changes(): Observable<ConfigChanges> {
        return Observable.create((observer: Observer<ConfigChanges>) => {
            // when user subscribes for the first time
            // we want to emit all the "up-to-date" changes
            // immidiatelly
            observer.next(this.initialChanges);
            // subscribe to internal changes so
            // we can forward them
            this.changes$.subscribe(changes => {
                observer.next(changes);
            },
            (error) => observer.error(error),
            () => observer.complete());
        });
    }

    /**
     * @deprecated Kept for testing purposes. Subscribe for changes instead.
    */
    getData<T>(): T {
        return _.cloneDeep(this.data);
    }

    private calcChanges(oldData: any, newData: any): ConfigChanges {
        const changes: ConfigChanges = {};

        // if new data is not defined
        if (newData === undefined || newData === null) {
            return changes;
        }

        const oldKeys = _.keys(oldData);
        const newKeys = _.keys(newData);
        const allKeys = _.concat(oldKeys, newKeys);
        const unqKeys = _.uniq(allKeys);
        unqKeys.forEach(key => {
            const oldVal = oldData[key];
            const newVal = newData[key];
            // if value has changed
            if (_.isEqual(oldVal, newVal) === false) {
                changes[key] = {
                    previousValue: oldVal,
                    currentValue: newVal,
                    firstChange: false
                };
            }
        });

        return changes;
    }

    toJSON() {
        return this.data;
    }
}
