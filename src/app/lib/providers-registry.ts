import { Type } from '@angular/core';

import { CommonProviderDeclarationModel } from './common-provider-declaration.model';
import { Config } from './config';
import { DataProviderDeclarationModel } from './data-provider-declaration';
import { DataProvider } from './data-provider.model';
import { MappingProvider } from './mapping-provider';
import { MappingProviderDeclarationModel } from './mapping-provider-declaration';
import { TypeHelpers } from './type-helpers';
import { ViewProviderDeclarationModel } from './view-provider-declaration';
import { ViewProvider } from './view-provider.model';

export class ProvidersRegistry {

    private static viewProviderDeclarations: ViewProviderDeclarationModel[] = [];
    private static dataProviderDeclarations: DataProviderDeclarationModel[] = [];
    private static mappingProvidersDeclarations: MappingProviderDeclarationModel[] = [];

    private static configs: { [id: string]: Config } = {};

    constructor() { }

    //
    //  View Providers Related
    //
    public static registerViewProvider(declaration: ViewProviderDeclarationModel, provider: Function) {
        this.registerProvider(this.viewProviderDeclarations, declaration, provider);
    }

    public static getViewProviderDeclarations(): ViewProviderDeclarationModel[] {
        return this.viewProviderDeclarations;
    }

    public static getViewProviderById(id: string): Type<ViewProvider> {
        return this.getProviderById(this.viewProviderDeclarations, id);
    }

    public static getViewProviderDeclarationByType(type: Type<ViewProvider>): ViewProviderDeclarationModel {
        return this.getProviderDeclarationByType(this.viewProviderDeclarations, type);
    }

    //
    //  Data Providers Related
    //
    public static registerDataProvider(declaration: DataProviderDeclarationModel, provider: Function) {
        this.registerProvider(this.dataProviderDeclarations, declaration, provider);
    }

    public static getDataProviderDeclarations(): DataProviderDeclarationModel[] {
        return this.dataProviderDeclarations;
    }

    public static getDataProviderById(id: string): Type<DataProvider> {
        return this.getProviderById(this.dataProviderDeclarations, id);
    }

    public static getDataProviderDeclarationByType(type: Type<DataProvider>): DataProviderDeclarationModel {
        return this.getProviderDeclarationByType(this.dataProviderDeclarations, type);
    }

    //
    //  Data Mapping Provider Related
    //
    public static registerDataMappingProvider(declaration: MappingProviderDeclarationModel, provider: Function) {
        this.registerProvider(this.mappingProvidersDeclarations, declaration, provider);
    }

    public static getMappingProviderById(id: string): Type<MappingProvider> {
        return this.getProviderById(this.mappingProvidersDeclarations, id);
    }

    public static getMappingProviderDeclarations(): MappingProviderDeclarationModel[] {
        return this.mappingProvidersDeclarations;
    }

    public static getMappingProviderDeclarationByType(type: Type<MappingProvider>): MappingProviderDeclarationModel {
        return this.getProviderDeclarationByType(this.mappingProvidersDeclarations, type);
    }


    //
    //  Configuration for providers
    //
    public static getDataProviderConfigByType(type: Type<DataProvider>) {
        return this.getConfigByDeclaration(this.getDataProviderDeclarationByType(type));
    }

    public static getViewProviderConfigByType(type: Type<ViewProvider>) {
        return this.getConfigByDeclaration(this.getViewProviderDeclarationByType(type));
    }

    public static getMappingProviderConfigByType(type: Type<MappingProvider>) {
        return this.getConfigByDeclaration(this.getMappingProviderDeclarationByType(type));
    }

    public static getConfigByDeclaration(declaration: CommonProviderDeclarationModel) {
        if (declaration) {
            return this.getConfigById(declaration.id);
        } else {
            return null;
        }
    }

    public static getConfigById(id: string): Config {
        return this.configs[id];
    }

    public static setConfigById(id: string, config: Config) {
        return this.configs[id] = config;
    }

    public static getConfigs() {
        return this.configs;
    }

    public static setConfigs(configs: { [id: string]: Config }) {
        return this.configs = configs;
    }

    //
    //  Generic Wrapper methods (any provider)
    //

    private static registerProvider(providerArr: CommonProviderDeclarationModel[],
        declaration: CommonProviderDeclarationModel,
        provider: Function) {
        // we must ensure unique id
        if (this.providerExists(providerArr, declaration.id)) {
            throw new Error('Provider with id "' + name + '" already exists. Pick a different id.');
        }

        // Check if there are any name collisions with existing providers
        const nameCollisions = this.checkNameCollisions(providerArr, declaration.name);
        if (nameCollisions.length) {
            const fmtNameCollisions = nameCollisions.map(v => `\t"${v.id}" — "${v.name}"`);
            let warnMsg = `WARNING: Name collision between providers:\n`;
            warnMsg += `\t "${declaration.id}" — "${declaration.name}"\n`;
            warnMsg += `WITH:\n`;
            warnMsg += `${fmtNameCollisions}`;
            console.warn(warnMsg);
        }

        // set provider value
        if (provider) {
            declaration.provider = <any>provider;
        }
        // set global config
        if (!this.configs[declaration.id]) {
            this.configs[declaration.id] = new Config(declaration.providerConfigDefaults || {});
        }

        providerArr.push(declaration);
    }

    private static checkNameCollisions(providerArr: CommonProviderDeclarationModel[], name: string): CommonProviderDeclarationModel[] {
        return providerArr.filter(v => v.name === name);
    }

    private static providerExists(providerArr: CommonProviderDeclarationModel[], id: string): boolean {
        return this.getProviderById(providerArr, id) !== null;
    }

    private static getProviderById(providerArr: CommonProviderDeclarationModel[], id: string): Type<any> {
        const declaration = providerArr.find(v => v.id === id);
        if (declaration) {
            return declaration.provider;
        } else {
            return null;
        }
    }

    private static getProviderDeclarationByType(providerArr: CommonProviderDeclarationModel[], type: Type<any>): any {
        return providerArr.find(v => TypeHelpers.typeEquals(v.provider, type));
    }

    public static serialize() {
        const serConfigs = {};
        Object.keys(this.configs).forEach(key => {
            serConfigs[key] = this.configs[key].toJSON();
        });
        return serConfigs;
    }

    public static populateFromJsonString(jsonStr: string) {
        const deserConfigs = JSON.parse(jsonStr);
        Object.keys(deserConfigs).forEach(key => {
            deserConfigs[key] = Config.fromJSON(deserConfigs[key]);
        });
        this.configs = deserConfigs;
    }
}
