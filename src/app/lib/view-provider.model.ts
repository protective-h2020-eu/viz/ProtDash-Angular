import { BaseEditorComponent } from '../view-providers/base-editor.component';
import { Type } from '@angular/core';

export interface ViewProvider {
    getDataType(): Type<any>;
}
