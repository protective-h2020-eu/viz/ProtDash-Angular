import { ProvidersRegistry } from './providers-registry';
import { DataProvider } from './data-provider.model';
import { Type } from '@angular/core';
import { ViewProvider } from './view-provider.model';
import { CommonProviderDeclarationModel } from './common-provider-declaration.model';
import { BaseEditorComponent } from '../view-providers/base-editor.component';

// tslint:disable-next-line:no-empty-interface
export interface ViewProviderDeclarationModel extends CommonProviderDeclarationModel {
    dataType: Type<any>;
    editorView?: Type<BaseEditorComponent>;
}

export function ViewProviderDeclaration(config: ViewProviderDeclarationModel): ClassDecorator {
    return function (target) {
        // console.log('registering view provider', config.name, config);
        ProvidersRegistry.registerViewProvider(config, target);
    };
}
