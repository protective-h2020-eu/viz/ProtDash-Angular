import { Type } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BaseEditorComponent } from '../view-providers/base-editor.component';

export interface DataProvider {
    /**
     * Allows views to query for data
     */
    query(params: any): Observable<any>;

    /**
     * Allows view to test data provider (e.g. username and password for DB)
     * before making any queries
     */
    testDataProvider(): Observable<boolean>;

    getDataType(): Type<any>;
}
