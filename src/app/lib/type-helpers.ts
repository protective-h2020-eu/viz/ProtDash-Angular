import { Type } from '@angular/core';

export class TypeHelpers {
    static typeEquals(type1: any, type2: any): boolean {
        return type1 === type2;
    }

    static instanceOf(obj: any, type: any): boolean {
        return obj instanceof type;
    }
}
