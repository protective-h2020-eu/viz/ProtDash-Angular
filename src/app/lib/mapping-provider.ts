import { Observable } from 'rxjs/Observable';
import { Type } from '@angular/core';
import { Observer } from 'rxjs/Observer';

export interface MappingProvider {
    input(): Observer<any>;
    output<T>(type: Type<T>): Observable<T>;
    supportsDataType<T>(type: Type<T>): boolean;
    supportsViewType<T>(type: Type<T>): boolean;
}
