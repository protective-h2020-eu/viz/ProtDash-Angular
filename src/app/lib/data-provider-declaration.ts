import { ProvidersRegistry } from './providers-registry';
import { DataProvider } from './data-provider.model';
import { Type } from '@angular/core';
import { CommonProviderDeclarationModel } from './common-provider-declaration.model';
import { BaseEditorComponent } from '../view-providers/base-editor.component';

// tslint:disable-next-line:no-empty-interface
export interface DataProviderDeclarationModel extends CommonProviderDeclarationModel {
    dataType: Type<any>;
    editorView?: Type<BaseEditorComponent>;
}

export function DataProviderDeclaration(config: DataProviderDeclarationModel): ClassDecorator {
    return function (target) {
        // console.log('registering data provider', config.name, config);
        ProvidersRegistry.registerDataProvider(config, target);
    };
}
