import { Type } from '@angular/core';
import { DataProvider } from './data-provider.model';
import { ProvidersRegistry } from './providers-registry';
import { ViewProvider } from './view-provider.model';
import { CommonProviderDeclarationModel } from './common-provider-declaration.model';

export interface MappingProviderDeclarationModel extends CommonProviderDeclarationModel {
    dataProviderDataTypes: Type<any>[];
    viewProviderDataTypes: Type<any>[];
}

export function MappingProviderDeclaration(config: MappingProviderDeclarationModel): ClassDecorator {
    return function(target) {
        // console.log('registering mapping provider', config.name, config);
        ProvidersRegistry.registerDataMappingProvider(config, target);
    };
}
