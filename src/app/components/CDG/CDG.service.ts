import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';

/* Naming NOTE
  The API's file field is `fileItem` thus, we name it the same below
  it's like saying <input type='file' name='fileItem' />
  on a standard file field
*/

@Injectable()
export class CDGService {
    public readonly CA_MAIR_URL = environment.ca_mair.url;
    private headers: HttpHeaders;
    apiBaseURL = this.CA_MAIR_URL;
    //   apiBaseURL = 'http://192.168.200.160:8080/api/';
    constructor(private http: HttpClient) {}

    fileUpload(fileItem: File, extraData?: Object): any {
        const apiCreateEndpoint = `${this.apiBaseURL}upload-cdg-file`;
        const formData: FormData = new FormData();

        // const testGetEndPoint = `${this.apiBaseURL}get-cdg-names`;
        // const testGet = new HttpRequest('GET', testGetEndPoint);

        formData.append('file', fileItem, fileItem.name);
        // extraData = 'file';
        if (extraData) {
            for (const key in extraData) {
                // Iterate and set other form data
                if (key) {
                    formData.append(key, extraData[key]);
                }
            }
        }
        const req = new HttpRequest('POST', apiCreateEndpoint, formData, {
            reportProgress: true // for progress data
        });
        return this.http.request(req);
    }

    optionalFileUpload(fileItem?: File, extraData?: object): any {
        const apiCreateEndpoint = `${this.apiBaseURL}upload-cdg-file`;
        const formData: FormData = new FormData(); //
        let fileName;
        if (extraData) {
            for (const key in extraData) {
                if (key) {
                    // iterate and set other form data
                    if (key === 'fileName') {
                        fileName = extraData[key];
                    }
                    formData.append(key, extraData[key]);
                }
            }
        }

        if (fileItem) {
            if (!fileName) {
                fileName = fileItem.name;
            }
            formData.append('image', fileItem, fileName);
        }
        const req = new HttpRequest('POST', apiCreateEndpoint, formData, {
            reportProgress: true // for progress data
        });
        return this.http.request(req);
    }
    list(): Observable<any> {
        const listEndpoint = `${this.apiBaseURL}get-cdg-names`;
        // const listEndpoint = '${this.apiBaseURL}files/';
        return this.http.get(listEndpoint);
    }
}
