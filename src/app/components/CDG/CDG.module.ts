import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { CDGService } from './CDG.service';
import { CdgtableComponent } from './cdgtable/cdgtable.component';
import { FileUploadComponent } from './FileUpload/FileUpload.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserModule
    ],
    providers: [CDGService, HttpClientModule],
    declarations: [FileUploadComponent, CdgtableComponent],
    entryComponents: [FileUploadComponent]
})
export class CDGModule {}
