import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CDGService } from './../CDG.service';

interface CDGs {
    name: string;
    checked: boolean;
}

@Component({
    selector: 'app-cdgtable',
    templateUrl: './cdgtable.component.html',
    styleUrls: ['./cdgtable.component.css']
})
export class CdgtableComponent implements OnInit {
    public CDG_Table: string[] = [];
    public CDG_Info: CDGs[] = [];
    public currentCDG: string = null;

    constructor(
        private fileUploadService: CDGService,
        private http: HttpClient,
        private router: Router
    ) {}

    ngOnInit() {
        this.fileUploadService.list().subscribe(data => {
            this.CDG_Table = data;

            for (const i in this.CDG_Table) {
                if (this.CDG_Table) {
                    this.CDG_Info.push({
                        name: this.CDG_Table[i],
                        checked: false
                    });
                }
            }
        });
    }
    onGraphTicked(event: boolean, CDGname: string) {}
    onNavigateToWidgets() {
        this.router.navigate(['context-awareness']);
    }
}
