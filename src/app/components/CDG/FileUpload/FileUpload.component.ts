import 'rxjs/add/operator/map';

import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';

import { environment } from '../../../../environments/environment';
import { CDGService } from '../CDG.service';

@Component({
    selector: 'app-file-upload',
    templateUrl: './FileUpload.component.html',
    styleUrls: ['./FileUpload.component.css']
})
export class FileUploadComponent implements OnInit, OnDestroy {
    public readonly CA_MAIR_URL = environment.ca_mair.url;
    statusCreateForm: FormGroup;
    fileDescription: FormControl;
    fileToUpload: File = null;
    fileString: string;
    uploadProgress: number;
    uploadPercent: string;
    uploadComplete = false;
    uploadingProgressing = false;
    fileUploadSub: any;
    serverResponse: any;
    errorArray: any;

    @ViewChild('myInput') myFileInput: any;

    constructor(
        private fileUploadService: CDGService,
        private http: HttpClient
    ) {}

    ngOnInit() {
        /* initilize the form and/or extra form fields
                Do not initialize the file field
            */

        const url = `${this.CA_MAIR_URL}/greeting`;

        this.http.get(url).subscribe(data => {
            console.log(data);
        });

        this.fileDescription = new FormControl('', [
            Validators.required,
            Validators.minLength(4),
            Validators.maxLength(280)
        ]);
        this.statusCreateForm = new FormGroup({
            description: this.fileDescription
        });
        this.fileString = 'No File Chosen';
    }
    ngOnDestroy() {
        if (this.fileUploadSub) {
            this.fileUploadSub.unsubscribe();
        }
    }
    handleProgress(event) {
        if (event.type === HttpEventType.DownloadProgress) {
            this.uploadingProgressing = true;
            this.uploadProgress = Math.round(100 * event.loaded / event.total);
            this.uploadPercent = String(this.uploadProgress) + '%';
        }

        if (event.type === HttpEventType.UploadProgress) {
            this.uploadingProgressing = true;
            this.uploadProgress = Math.round(100 * event.loaded / event.total);
            this.uploadPercent = String(this.uploadProgress) + '%';
        }

        if (event.type === HttpEventType.Response) {
            // console.log(event.body);
            this.uploadComplete = true;
            this.serverResponse = event.body;
            this.uploadPercent = 'UPLOAD COMPLETE!';
        }
    }
    handleSubmit(event: any, statusNgForm: NgForm, statusFormGroup: FormGroup) {
        event.preventDefault();
        if (statusNgForm.submitted) {
            const submittedData = statusFormGroup.value;
            this.fileUploadSub = this.fileUploadService
                .fileUpload(this.fileToUpload)
                .subscribe(
                    data => this.handleProgress(data),
                    error => {
                        this.uploadPercent = 'ERROR!!';
                        this.errorArray = error.error;
                        console.log('Server error: ', error.error);
                    }
                );

            statusNgForm.resetForm({});
        }
    }
    handleFileInput(files: FileList) {
        const fileItem = files.item(0);
        this.fileString = fileItem.name;
        this.fileToUpload = fileItem;
    }

    resetFileInput() {
        console.log(this.myFileInput.nativeElement.files);
        this.myFileInput.nativeElement.value = '';
        console.log(this.myFileInput.nativeElement.files);
    }
}
