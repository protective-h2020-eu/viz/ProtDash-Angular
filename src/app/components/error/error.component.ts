import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-error',
    templateUrl: './error.component.html',
    styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

    public errorType = '500';

    constructor(private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.errorType = params['errorType'];
        });
    }
}
