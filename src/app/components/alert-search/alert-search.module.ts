import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';

import { AlertSearchEditorComponent } from './alert-search-editor/alert-search-editor.component';
import { AlertSearchService } from './alert-search.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        BrowserModule,
        BsDatepickerModule.forRoot(),
        CollapseModule.forRoot(),
        TabsModule.forRoot(),
        Ng2SearchPipeModule,
        NgxPaginationModule
    ],
    providers: [AlertSearchService, HttpClientModule],
    declarations: [AlertSearchEditorComponent],
    entryComponents: [AlertSearchEditorComponent]
})
export class AlertSearchModule {}
