import * as neon from 'neon-framework';

export class NeonAlertDataHolder {
    constructor(public query: neon.query.Query, public data: any) {

    }
}
