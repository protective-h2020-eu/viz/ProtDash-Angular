import { MentatDataMapper } from '../mentat-table-data-mapper';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import * as neon from 'neon-framework';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

import { BaseEditorComponent } from '../../../view-providers/base-editor.component';
import { AlertSearchService } from './../alert-search.service';

// tslint:disable-next-line:max-line-length
@Component({
    selector: 'app-alert-search-editor',
    templateUrl: './alert-search-editor.component.html',
    styleUrls: ['./alert-search-editor.component.css']
})
export class AlertSearchEditorComponent extends BaseEditorComponent
    implements OnInit {
    public isNeonReady = false;
    public processing = false;
    public bsConfig: Partial<BsDatepickerConfig> = Object.assign(
        {},
        { containerClass: 'theme-dark-blue' }
    );

    public selectedDb: string = null;
    public selectedTable: string = null;

    public dbNames: string[] = [];
    public tableNames: string[] = [];
    public fieldNames: string[] = [];
    public categories: string[] = [];
    public detectors: string[] = [];

    public selectedSourceIpField: string = null;
    public selectedTargetIpField: string = null;
    public sourceIP: string = null;
    public targetIP: string = null;

    public selectedDetector: string = null;
    public selectedCategory: string = null;

    public selectedLimit = 100;

    public dateField: string = null;
    public granularity: string = null;
    public selectedTimeFilterField: string = null;
    public selectedTimeFilterFrom: Date = null;
    public selectedTimeFilterTo: Date = null;
    public timeFilters: {
        field: string;
        from: string;
        to: any;
    }[] = [];
    public filters: {
        field: string;
        operator: string;
        value: any;
    }[] = [];

    public ewmaAlpha: number = null;
    public hardcodedView = false;
    public lastResponse: any;
    public tableData: MentatDataMapper;

    // public term: string;
    public outputFileName: string;

    constructor(
        private neonDp: AlertSearchService,
        private cdr: ChangeDetectorRef
    ) {
        super();
    }

    ngOnInit() {
        // keep local reference so we don't have to cast it all the time
        this.connectToNeon();
    }

    public connectToNeon() {
        this.processing = true;
        this.neonDp.testDataProvider().subscribe(
            isReady => {
                // this.hideLoadingIndicator();
                this.isNeonReady = isReady;
                console.log('isNeonReady = ', this.isNeonReady);
                this.processing = false;
                if (isReady) {
                    this.init();
                    if (this.canSendQuery()) {
                        console.log('Can send query returns true');
                    }
                } else {
                    // this.displayErrorMessage('NEON is not ready yet');
                    console.log('NEON is not ready yet');
                }
            },
            error => {
                // this.displayErrorMessage(this.neonErrToStr(error));
                // this.hideLoadingIndicator();
                console.log('Error in connection: ', error);
                this.processing = false;
            }
        );
    }
    // Populate array with all detectors and all categories
    public getDetectorsAndCategories() {
        let neonQuery = new neon.query.Query();

        neonQuery = neonQuery
            .selectFrom('mentat', 'alerts_dates')
            .withFields('Category')
            .distinct();

        this.neonDp.query(neonQuery).subscribe(
            data => {
                this.categories = data.data.map(a => a.Category);
                // console.log('Data in category query', data.data.map(a => a.Category));
            },
            error => {
                console.log('Error in category query', error);
                this.categories = error;
            }
        );

        neonQuery = neonQuery
            .selectFrom('mentat', 'alerts_dates')
            .withFields('Node.Name')
            .distinct();

        this.neonDp.query(neonQuery).subscribe(
            data => {
                this.detectors = data.data.map(a => a['Node.Name']);
                const det = 'Node.Name';
                // console.log('Data in detector query', data.data.map(a => a['Node.Name']));
            },
            error => {
                console.log('Error in detector query', error);
                this.detectors = error;
            }
        );
    }
    private init() {
        this.updateDatabaseNames();
        // if already has database selected, then update table names
        this.cdr.detectChanges();
        if (this.selectedDb) {
            this.updateTableNames();
        }
        // if already has database and table selected, then populate fields
        if (this.selectedDb && this.selectedTable) {
            this.updateFieldNames();
        }
        this.getDetectorsAndCategories();
    }
    canSendQuery() {
        return (
            this.isNeonReady &&
            this.processing === false &&
            this.selectedDb &&
            this.selectedTable
        );
    }
    sendQuery() {
        this.processing = true;
        // this.clearErrorMessage();
        let neonQuery = new neon.query.Query();
        // select db and table
        neonQuery = neonQuery.selectFrom(this.selectedDb, this.selectedTable);

        const whereClauses: neon.query.WhereClause[] = [];

        // console.log('Filters before adding IPs: ', this.filters);
        if (this.sourceIP && this.selectedSourceIpField) {
            this.addSourceFilter();
            // console.log('Filters after adding IPs: ', this.filters);
        }
        if (this.targetIP && this.selectedTargetIpField) {
            this.addTargetFilter();
            // console.log('Filters after adding IPs: ', this.filters);
        }
        if (this.selectedCategory) {
            this.addCategoryFilter();
        }
        if (this.selectedDetector) {
            this.addDetectorFilter();
        }
        this.filters.forEach(filter => {
            if (filter.field && filter.operator) {
                // we have to manually set rhs as neon.query.WhereClause contructor only supports
                // strings
                const clause = new neon.query.WhereClause(
                    filter.field,
                    filter.operator,
                    null
                );
                // Else required to compare IP addresses
                if (typeof filter.value !== 'string') {
                    clause.rhs = this.trycast(filter.value);
                } else {
                    clause.rhs = filter.value;
                }
                whereClauses.push(clause);
            }
        });
        if (this.selectedTimeFilterField) {
            if (this.selectedTimeFilterFrom) {
                const fromClause = new neon.query.WhereClause(
                    this.selectedTimeFilterField,
                    '>=',
                    null
                );
                const date = new Date(this.selectedTimeFilterFrom);
                fromClause.rhs = this.trycast(date.toISOString());
                whereClauses.push(fromClause);
            }
            if (this.selectedTimeFilterTo) {
                const toClause = new neon.query.WhereClause(
                    this.selectedTimeFilterField,
                    '<=',
                    null
                );
                const date = new Date(this.selectedTimeFilterTo);
                toClause.rhs = this.trycast(date.toISOString());
                whereClauses.push(toClause);
            }
        }
        if (whereClauses.length > 0) {
            neonQuery = neonQuery.where(neon.query.and(...whereClauses));
        }
        // limit
        neonQuery.limit(this.selectedLimit);

        // this.showLoadingIndicator('Querying...');
        // this.neonDp.query(neonQuery).subscribe(
        //     data => {
        //         // data.ewmaAlpha = this.ewmaAlpha;
        //         // data.harcodedView = this.hardcodedView;
        //         this.processing = false;

        //         this.lastResponse = data;
        //         this.cdr.detectChanges();
        //     },
        //     error => {
        //         this.processing = false;
        //         console.log('Error in query', error);
        //         this.lastResponse = error;
        //     }
        // );
        this.neonDp.query(neonQuery).subscribe(
            data => {
                // data.ewmaAlpha = this.ewmaAlpha;
                // data.harcodedView = this.hardcodedView;
                this.processing = false;

                this.lastResponse = data;
                this.tableData = data.data.map(row => {
                    return new MentatDataMapper(row);
                });

                this.cdr.detectChanges();
            },
            error => {
                this.processing = false;
                console.log('Error in query', error);
                this.lastResponse = error;
            }
        );
    }
    addSourceFilter() {
        this.filters.push({
            field: this.selectedSourceIpField,
            operator: '=',
            value: this.sourceIP
        });
    }
    addTargetFilter() {
        this.filters.push({
            field: this.selectedTargetIpField,
            operator: '=',
            value: this.targetIP
        });
    }
    addCategoryFilter() {
        this.filters.push({
            field: 'Category',
            operator: '=',
            value: this.selectedCategory
        });
    }
    addDetectorFilter() {
        this.filters.push({
            field: 'Node.Name',
            operator: '=',
            value: this.selectedDetector
        });
    }
    resetFilters() {
        this.filters = [];
    }

    removeFilter(index) {
        this.filters.splice(index, 1);
    }
    updateDatabaseNames() {
        this.dbNames = [];
        this.neonDp.getDatabaseNames().subscribe(
            names => {
                console.log('In database names: ', names);
                this.dbNames = names;
            },
            error => {
                console.log('In database names error: ', error);
            }
        );
    }

    updateTableNames(event?: any) {
        this.tableNames = [];
        this.selectedDb = event;
        this.neonDp.getTableNames(this.selectedDb).subscribe(
            names => {
                console.log('In table names: ', names);
                this.tableNames = names;
            },
            error => {
                console.log('In table names error: ', error);
            }
        );
    }

    updateFieldNames(event?: any) {
        this.fieldNames = [];

        this.selectedTable = event;
        this.neonDp.getFields(this.selectedDb, this.selectedTable).subscribe(
            names => {
                this.fieldNames = names;
                // console.log('fieldNames are ', names);
            },
            error => {
                console.log('Error in update Field', error);
            }
        );
    }
    private neonErrToStr(error: any) {
        // if not defined
        if (!error) {
            return 'UNKNOW ERROR — Error object is empty';
        }
        // if string already, then nothing to convert
        if (typeof error === 'string') {
            return error;
        }
        // state of 0 indicates that the connection was not initiated (e.g. TIMEOUT)
        if (error.readyState === 0) {
            return (
                'Failed to connect to: "' + this.neonDp.NEON_SERVER_URL + '"'
            );
        } else if (error.responseText) {
            return error.responseText;
        } else if (error.statusText) {
            return error.statusText;
        } else {
            return JSON.stringify(error, null, 2);
        }
    }
    hasFields() {
        return this.fieldNames.length > 0;
    }
    /**
     * getFieldsTypes seems to throw error for mentat db, so we can securely
     * extract field types.
     */
    private trycast(value: any): any {
        // ISO date format yyyy-mm-ddTHH:mm:ss.SSSZ (2018-04-18T00:00:00.000Z)
        const dateRegex =
            '[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z';
        if (value.match(dateRegex)) {
            return value;
        }
        const number = parseFloat(value);
        if (isNaN(number)) {
            console.log('Trycast returning: ', value);
            return value;
        } else {
            return number;
        }
    }
    downloadDataFile() {
        const options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: true,
            useBom: true,
            noDownload: true,
            headers: [
                'Time Created',
                'Source Country',
                'Source IP',
                'Target IP',
                'Category',
                'Description',
                'Protocol',
                'Detector'
            ]
        };

        new Angular5Csv(this.tableData, this.outputFileName);
    }
}
