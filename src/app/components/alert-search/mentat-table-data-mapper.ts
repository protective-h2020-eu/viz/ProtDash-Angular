export class MentatDataMapper {
    public time: string;
    public country: string;
    public nodeNames: string;
    public sourceIp: string;
    // public targetIp: string;
    public targetString: string;
    public categ: string;
    public desc: string;
    // public protocol: string;
    public protoString: string;
    public detector: string;
    constructor(row: any) {
        this.time = row.DetectTime !== undefined ? row.DetectTime : 'NA';
        this.country =
        row._CESNET.SourceResolvedCountry !== undefined
                ? row._CESNET.SourceResolvedCountry[0]
                : 'NA';
        this.sourceIp =
        row.Source === undefined
                ? 'NA'
                : row.Source.length === 0
                    ? 'NA'
                    : row.Source[0].IP4 === undefined
                        ? 'NA'
                        : row.Source[0].IP4.length === 0
                            ? 'NA'
                            : row.Source[0].IP4[0].ip === undefined
                                ? 'NA'
                                : row.Source[0].IP4[0].ip;


        this.targetString = '';
        row.Target === undefined
                ? this.targetString = 'NA'
                : row.Target.length === 0
                    ? this.targetString = 'NA'
                    : row.Target[0].IP4 === undefined
                        ? this.targetString = 'NA'
                        : row.Target[0].IP4.length === 0
                            ? this.targetString = 'NA'
                            : row.Target[0].IP4[0].ip === undefined
                                ? this.targetString = 'NA'
                                : row.Target[0].IP4.forEach(tgt => {
                                    this.targetString += (tgt.ip + ' ');
                                });
        this.targetString = this.targetString.trim();

        this.categ = row.Category !== undefined ? row.Category[0] : 'NA';
        this.desc = row.Description !== undefined ? row.Description : 'NA';

        this.protoString = '';
        row.Target === undefined
        ? this.protoString = 'NA'
        : row.Target[0] === undefined
                ? this.protoString = 'NA'
                : row.Target[0].Proto === undefined
                    ? this.protoString = 'NA'
                    : row.Target[0].Proto.forEach(protocol => {
                        this.protoString += (protocol + ' ');
                    });
        this.protoString = this.protoString.trim();

        this.nodeNames = '';
        row.Node.forEach(element => {
            this.nodeNames += element.Name + ' ';
        });
        this.nodeNames = this.nodeNames.trim();

        // delete not needed objects
    }
}
