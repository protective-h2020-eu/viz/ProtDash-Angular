import { Injectable, Type } from '@angular/core';
import * as neon from 'neon-framework';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

import { environment } from './../../../environments/environment';
import { DataProvider } from './../../lib/data-provider.model';
import { NeonAlertDataHolder } from './neon-alert-data-holder';

@Injectable()
export class AlertSearchService implements DataProvider {
    public readonly NEON_SERVER_HOST = environment.neon.hostname;
    public readonly NEON_SERVER_URL = environment.neon.url;
    private datastore = neon.query.Connection.MONGO;
    private connection = new neon.query.Connection();
    private neonReady = false;

    query(neonQuery: neon.query.Query): Observable<NeonAlertDataHolder> {
        return new Observable((observer: Observer<NeonAlertDataHolder>) => {
            this.connection.executeQuery(
                neonQuery,
                function(result) {
                    observer.next(
                        new NeonAlertDataHolder(
                            neonQuery,
                            result.data
                        )
                    );
                    observer.complete();
                },
                error => {
                    observer.error(error);
                    observer.complete();
                }
            );
        });
    }
    testDataProvider(): Observable<boolean> {
        if (this.neonReady === false) {
            return new Observable((sub: Observer<boolean>) => {
                neon.setNeonServerUrl(this.NEON_SERVER_URL);
                neon.ready(() => {
                    this.connection.connect(
                        this.datastore,
                        this.NEON_SERVER_HOST
                    );
                    // test if we can fetch db list and if yes, then all is good
                    this.getDatabaseNames().subscribe(
                        list => {
                            this.neonReady = true;
                            sub.next(true);
                            sub.complete();
                        },
                        error => {
                            sub.error(error);
                            sub.complete();
                        }
                    );
                });
            });
        } else {
            return Observable.of(this.neonReady);
        }
    }

    getDataType(): Type<any> {
        return NeonAlertDataHolder;
    }

    public getDatabaseNames(): Observable<string[]> {
        return new Observable((observer: Observer<string[]>) => {
            // due to spelling mistake of "getDatbaseNames" method in original library
            // we have to cast it to <any> in order to call correctly spelled method
            (<any>this.connection).getDatabaseNames(
                response => {
                    observer.next(response);
                    observer.complete();
                },
                error => {
                    observer.error(error);
                    observer.complete();
                }
            );
        });
    }

    public getTableNames(dbName: string): Observable<string[]> {
        return new Observable((observer: Observer<any>) => {
            // yes, yes, there is no error callback this time
            // this is the way neon library is implemented...
            this.connection.getTableNames(dbName, response => {
                observer.next(response);
                observer.complete();
            });
        });
    }

    public getFields(dbName, tableName): Observable<string[]> {
        return new Observable((observer: Observer<any>) => {
            this.connection.getFieldNames(
                dbName,
                tableName,
                response => {
                    observer.next(response);
                    observer.complete();
                },
                error => {
                    observer.error(error);
                    observer.complete();
                }
            );
        });
    }
}
