import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProvidersRegistry } from '../../../lib/providers-registry';
import { ViewProviderDeclarationModel } from '../../../lib/view-provider-declaration';
import { WidgetsService } from '../dashboard-components/widgets.service';

@Component({
    selector: 'app-context-awareness',
    templateUrl: './context-awareness.component.html',
    styleUrls: ['./context-awareness.component.css']
})
export class ContextAwarenessComponent implements OnInit {
    public static DEF_WIDGETS_CREATED = false;
    public readonly DASHBOARD_ID = 'context-awareness';
    public selectedVPConfig: ViewProviderDeclarationModel = null;

    constructor(
        private widgetsService: WidgetsService,
        private router: Router
    ) {}

    ngOnInit() {}

    addWidget() {
        this.widgetsService.addWidget(this.DASHBOARD_ID, {
            name: 'Widget — ' + this.selectedVPConfig.name,
            viewProviderType: this.selectedVPConfig.provider
        });
    }

    availableViewProviders() {
        return ProvidersRegistry.getViewProviderDeclarations();
    }

    saveRestoreTest() {
        // for teting purpose navigate into main page
        this.router.navigate([]);

        const serConfigs = ProvidersRegistry.serialize();
        const serWidgets = this.widgetsService.serialize();

        const serWidgetsStr = JSON.stringify(serWidgets, null, 2);
        const serConfigsStr = JSON.stringify(serConfigs, null, 2);

        console.log(serWidgetsStr);
        console.log(serConfigsStr);

        this.widgetsService.removeAll(this.DASHBOARD_ID);

        setTimeout(() => {
            ProvidersRegistry.populateFromJsonString(serConfigsStr);
            this.widgetsService.populateFromJsonString(
                serWidgetsStr,
                this.DASHBOARD_ID
            );
        }, 3000);
    }
}
