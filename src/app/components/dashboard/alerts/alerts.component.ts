import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProvidersRegistry } from '../../../lib/providers-registry';
import { ViewProviderDeclarationModel } from '../../../lib/view-provider-declaration';
import { WidgetsService } from '../dashboard-components/widgets.service';

@Component({
    selector: 'app-alerts',
    templateUrl: './alerts.component.html',
    styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {
    public static DEF_WIDGETS_CREATED = false;
    public readonly DASHBOARD_ID = 'alerts';
    public selectedVPConfig: ViewProviderDeclarationModel = null;

    constructor(
        private widgetsService: WidgetsService,
        private router: Router
    ) {}

    ngOnInit() {
        // if (AlertsComponent.DEF_WIDGETS_CREATED === false) {
        //     // chart js
        //     this.widgetsService.addWidget(this.DASHBOARD_ID, {
        //         name: 'Chart JS Widget',
        //         viewProviderType: ChartJsViewComponent,
        //         dataProviderType: DemoDataProvider,
        //         mappingProviderType: DemoToSeriesMapping
        //     });
        //     this.widgetsService.addWidget(this.DASHBOARD_ID, {
        //         name: 'Chart JS Widget',
        //         viewProviderType: ChartJsViewComponent,
        //         dataProviderType: DemoDataProvider,
        //         mappingProviderType: DemoToSeriesMapping,
        //         viewProviderEditorConfig: new Config({
        //             chartType: 'pie'
        //         })
        //     });
        //     // table view for neon demo
        //     this.widgetsService.addWidget(this.DASHBOARD_ID, {
        //         name: 'Neon Table View',
        //         viewProviderType: BasicTableVpComponent,
        //         dataProviderType: NeonDataProvider,
        //         mappingProviderType: NeonToMultiMapping,
        //         dataProviderEditorConfig: new Config({
        //             dbName: 'test',
        //             tableName: 'earthquakes',
        //             aggregation: {
        //                 operator: 'avg',
        //                 field: 'mag',
        //                 groupByField: 'net'
        //             }
        //         })
        //     });
        //     this.widgetsService.addWidget(this.DASHBOARD_ID, {
        //         name: 'Force Graph View',
        //         viewProviderType: ForceDirectedGraphComponent,
        //         dataProviderType: ForceCDGService,
        //         mappingProviderType: JsonToD3Mapping,
        //         dataProviderEditorConfig: new Config({
        //             name: '{{allEdgesCDG}}'
        //         })
        //     });
        //     AlertsComponent.DEF_WIDGETS_CREATED = true;
        // }
    }

    addWidget() {
        this.widgetsService.addWidget(this.DASHBOARD_ID, {
            name: 'Widget — ' + this.selectedVPConfig.name,
            viewProviderType: this.selectedVPConfig.provider
        });
    }

    availableViewProviders() {
        return ProvidersRegistry.getViewProviderDeclarations();
    }

    saveRestoreTest() {
        // for teting purpose navigate into main page
        this.router.navigate([]);

        const serConfigs = ProvidersRegistry.serialize();
        const serWidgets = this.widgetsService.serialize();

        const serWidgetsStr = JSON.stringify(serWidgets, null, 2);
        const serConfigsStr = JSON.stringify(serConfigs, null, 2);

        console.log(serWidgetsStr);
        console.log(serConfigsStr);

        this.widgetsService.removeAll(this.DASHBOARD_ID);

        setTimeout(() => {
            ProvidersRegistry.populateFromJsonString(serConfigsStr);
            this.widgetsService.populateFromJsonString(
                serWidgetsStr,
                this.DASHBOARD_ID
            );
        }, 3000);
    }
}
