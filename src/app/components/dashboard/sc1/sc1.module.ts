import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { DashboardModule } from '../dashboard-components/dashboard.module';
import { HomeDashComponent } from '../home-dash/home-dash.component';
import { HomeSplashComponent } from '../home_splash/home_splash.component';
import { StatisticsComponent } from '../statistics/statistics.component';
import { AlertsComponent } from './../alerts/alerts.component';
import { ContextAwarenessComponent } from './../context-awareness/context-awareness.component';
import { MetaAlertsComponent } from './../meta-alerts/meta-alerts.component';
import { Sc1MainComponent } from './sc1-main/sc1-main.component';

@NgModule({
    declarations: [
        Sc1MainComponent,
        StatisticsComponent,
        AlertsComponent,
        HomeSplashComponent,
        MetaAlertsComponent,
        ContextAwarenessComponent,
        HomeDashComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        DashboardModule,
        BsDatepickerModule.forRoot()
    ]
})
export class Sc1Module { }
