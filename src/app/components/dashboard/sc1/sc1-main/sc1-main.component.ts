import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { DemoDataProvider } from '../../../../data-providers/demo-dp/demo-dp.service';
import { NeonDataProvider } from '../../../../data-providers/neon-dp/neon-dp.service';
import { Config } from '../../../../lib/config';
import { ProvidersRegistry } from '../../../../lib/providers-registry';
import { ViewProviderDeclarationModel } from '../../../../lib/view-provider-declaration';
import { DemoToSeriesMapping } from '../../../../mapping-providers/demo-to-chartjs';
import { NeonToMultiMapping } from '../../../../mapping-providers/neon-to-multi';
import { BasicTableVpComponent } from '../../../../view-providers/basic-table-vp/basic-table-vp.component';
import { ChartJsViewComponent } from '../../../../view-providers/chartjs-vp/chartjs-view-component/chartjs-vp.component';
import { DashboardGridComponent } from '../../dashboard-components/dashboard-grid/dashboard-grid.component';
import { WidgetItemFormatter } from '../../dashboard-components/dashboard-widget/widget-item-formatter';
import { WidgetsService } from '../../dashboard-components/widgets.service';

@Component({
    selector: 'app-sc1-main',
    templateUrl: './sc1-main.component.html',
    styleUrls: ['./sc1-main.component.css']
})
export class Sc1MainComponent implements OnInit {

    public static DEF_WIDGETS_CREATED = false;
    public readonly DASHBOARD_ID = 'sc1';
    public selectedVPConfig: ViewProviderDeclarationModel = null;

    constructor(private widgetsService: WidgetsService, private router: Router) { }

    ngOnInit() {
        if (Sc1MainComponent.DEF_WIDGETS_CREATED === false) {
            // chart js
            this.widgetsService.addWidget(this.DASHBOARD_ID, {
                name: 'Chart JS Widget',
                viewProviderType: ChartJsViewComponent,
                dataProviderType: DemoDataProvider,
                mappingProviderType: DemoToSeriesMapping
            });

            // table view for neon demo
            this.widgetsService.addWidget(this.DASHBOARD_ID, {
                name: 'Neon Table View',
                viewProviderType: BasicTableVpComponent,
                dataProviderType: NeonDataProvider,
                mappingProviderType: NeonToMultiMapping,
                dataProviderEditorConfig: new Config({
                    dbName: 'test',
                    tableName: 'earthquakes',
                    aggregation: {
                        operator: 'avg',
                        field: 'mag',
                        groupByField: 'net'
                    }
                })
            });
            Sc1MainComponent.DEF_WIDGETS_CREATED = true;
        }
    }

    addWidget() {
        this.widgetsService.addWidget(this.DASHBOARD_ID, {
            name: 'Widget — ' + this.selectedVPConfig.name,
            viewProviderType: this.selectedVPConfig.provider,
        });
    }

    availableViewProviders() {
        return ProvidersRegistry.getViewProviderDeclarations();
    }

    saveRestoreTest() {
        // for teting purpose navigate into main page
        this.router.navigate([]);

        const serConfigs = ProvidersRegistry.serialize();
        const serWidgets = this.widgetsService.serialize();

        const serWidgetsStr = JSON.stringify(serWidgets, null, 2);
        const serConfigsStr = JSON.stringify(serConfigs, null, 2);

        console.log(serWidgetsStr);
        console.log(serConfigsStr);

        this.widgetsService.removeAll(this.DASHBOARD_ID);

        setTimeout(() => {
            ProvidersRegistry.populateFromJsonString(serConfigsStr);
            this.widgetsService.populateFromJsonString(serWidgetsStr, this.DASHBOARD_ID);
        }, 3000);
    }
}
