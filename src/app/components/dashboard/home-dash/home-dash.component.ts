import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { DemoDataProvider } from '../../../data-providers/demo-dp/demo-dp.service';
import { NeonDataProvider } from '../../../data-providers/neon-dp/neon-dp.service';
import { Config } from '../../../lib/config';
import { ProvidersRegistry } from '../../../lib/providers-registry';
import { ViewProviderDeclarationModel } from '../../../lib/view-provider-declaration';
import { DemoToSeriesMapping } from '../../../mapping-providers/demo-to-chartjs';
import { NeonToMultiMapping } from '../../../mapping-providers/neon-to-multi';
import { BasicTableVpComponent } from '../../../view-providers/basic-table-vp/basic-table-vp.component';
import { ChartJsViewComponent } from '../../../view-providers/chartjs-vp/chartjs-view-component/chartjs-vp.component';
import { DashboardGridComponent } from '../dashboard-components/dashboard-grid/dashboard-grid.component';
import { WidgetItemFormatter } from '../dashboard-components/dashboard-widget/widget-item-formatter';
import { WidgetsService } from '../dashboard-components/widgets.service';
import { ForceDirectedGraphComponent } from '../../../view-providers/d3js-vp/force-directed-graph/force-directed-graph.component';
import { ForceCDGService } from '../../../data-providers/sample-json-dp/forceCDG-dp.service';
import { JsonToD3Mapping } from '../../../mapping-providers/Json-to-D3js';
import { PlotlyVpComponentComponent } from '../../../view-providers/plotly-vp/plotly-vp-component/plotly-vp-component.component';
import { NeonTimeseriesDataProvider } from '../../../data-providers/neon-timeseries-dp/neon-timeseries-dp.service';
import { NeonTimeseriesToPlotly } from '../../../mapping-providers/neon-timeseries-to-plotly';

@Component({
  selector: 'app-home-dash',
  templateUrl: './home-dash.component.html',
  styleUrls: ['./home-dash.component.css']
})
export class HomeDashComponent implements OnInit {
    
    public static DEF_WIDGETS_CREATED = false;
    public readonly DASHBOARD_ID = 'home-dash';
    public selectedVPConfig: ViewProviderDeclarationModel = null;

    constructor(
        private widgetsService: WidgetsService,
        private router: Router
    ) {}

    ngOnInit() {   
        // Calculate date from last 7 days
        let last7d = new Date();
        last7d.setDate(last7d.getDate() - 7);
        // Calculate date from last 5min
        let last5min = new Date();
        last5min.setMinutes(last5min.getMinutes() - 5);

        if (HomeDashComponent.DEF_WIDGETS_CREATED === false) {
           
            // Stacked graph of number of alerts per source per hour over the last 7 days
            this.widgetsService.addWidget(this.DASHBOARD_ID, {
                name: 'Alerts per source',
                viewProviderType: PlotlyVpComponentComponent,
                dataProviderType: NeonTimeseriesDataProvider,
                mappingProviderType: NeonTimeseriesToPlotly,
                dataProviderEditorConfig: new Config({
                    dbName: 'mentat',
                    tableName: 'alerts_dates',
                    granularity: 'hour',
                    dateField: 'DetectTime',
                    selectedGroupByField: 'Node.Name',
                    timeFilter:{
                        field: "DetectTime",
                        from: last7d,
                        to: new Date()
                    }
                })
            }); 

            // Stacked graph of number of alerts per source per hour over the last 7 days
            this.widgetsService.addWidget(this.DASHBOARD_ID, {
                name: 'Source status',
                viewProviderType: BasicTableVpComponent,
                dataProviderType: NeonTimeseriesDataProvider,
                mappingProviderType: NeonTimeseriesToPlotly,
                dataProviderEditorConfig: new Config({
                    dbName: 'mentat',
                    tableName: 'alerts_dates',
                    granularity: 'hour',
                    dateField: 'DetectTime',
                    selectedGroupByField: 'Node.Name',
                    timeFilter:{
                        field: "DetectTime",
                        from: last5min,
                        to: new Date()
                    }
                })
            }); 

            // Stacked graph of number of alerts per partner per hour over the last 7 days
            this.widgetsService.addWidget(this.DASHBOARD_ID, {
                name: 'Alerts per partner',
                viewProviderType: PlotlyVpComponentComponent,
                dataProviderType: NeonTimeseriesDataProvider,
                mappingProviderType: NeonTimeseriesToPlotly,
                dataProviderEditorConfig: new Config({
                    dbName: 'mentat',
                    tableName: 'alerts_dates',
                    granularity: 'hour',
                    dateField: 'DetectTime',
                    selectedGroupByField: 'Node.Name',
                    timeFilter:{
                        field: "DetectTime",
                        from: last7d,
                        to: new Date()
                    },
                    hardcodedView: true
                })
            }); 

            // Stacked graph of number of alerts per category per hour over the last 7 days
            this.widgetsService.addWidget(this.DASHBOARD_ID, {
                name: 'Alerts per category',
                viewProviderType: PlotlyVpComponentComponent,
                dataProviderType: NeonTimeseriesDataProvider,
                mappingProviderType: NeonTimeseriesToPlotly,
                dataProviderEditorConfig: new Config({
                    dbName: 'mentat',
                    tableName: 'alerts_dates',
                    granularity: 'hour',
                    dateField: 'DetectTime',
                    selectedGroupByField: 'Category',
                    timeFilter:{
                        field: "DetectTime",
                        from: last7d,
                        to: new Date()
                    }
                })
            }); 

            HomeDashComponent.DEF_WIDGETS_CREATED = true;
        }
    }

    addWidget() {
        this.widgetsService.addWidget(this.DASHBOARD_ID, {
            name: 'Widget — ' + this.selectedVPConfig.name,
            viewProviderType: this.selectedVPConfig.provider
        });
    }

    availableViewProviders() {
        return ProvidersRegistry.getViewProviderDeclarations();
    }
 }
