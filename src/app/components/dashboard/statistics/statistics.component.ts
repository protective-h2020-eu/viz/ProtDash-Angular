import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { DemoDataProvider } from '../../../data-providers/demo-dp/demo-dp.service';
import { NeonDataProvider } from '../../../data-providers/neon-dp/neon-dp.service';
import { Config } from '../../../lib/config';
import { ProvidersRegistry } from '../../../lib/providers-registry';
import { ViewProviderDeclarationModel } from '../../../lib/view-provider-declaration';
import { DemoToSeriesMapping } from '../../../mapping-providers/demo-to-chartjs';
import { NeonToMultiMapping } from '../../../mapping-providers/neon-to-multi';
import { BasicTableVpComponent } from '../../../view-providers/basic-table-vp/basic-table-vp.component';
import { ChartJsViewComponent } from '../../../view-providers/chartjs-vp/chartjs-view-component/chartjs-vp.component';
import { DashboardGridComponent } from '../dashboard-components/dashboard-grid/dashboard-grid.component';
import { WidgetItemFormatter } from '../dashboard-components/dashboard-widget/widget-item-formatter';
import { WidgetsService } from '../dashboard-components/widgets.service';
import { HttpParams, HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'app-statistics-main',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
    public static DEF_WIDGETS_CREATED = false;
    public readonly DASHBOARD_ID = 'statistics';
    public readonly NODE_URL = environment.node.url;
    public selectedVPConfig: ViewProviderDeclarationModel = null;
    public isLoadingData = false;
    constructor(
        private widgetsService: WidgetsService,
        private router: Router,
        private http:HttpClient
    ) {}

    ngOnInit() {}

    addWidget() {
        this.widgetsService.addWidget(this.DASHBOARD_ID, {
            name: 'Widget — ' + this.selectedVPConfig.name,
            viewProviderType: this.selectedVPConfig.provider
        });
    }

    availableViewProviders() {
        return ProvidersRegistry.getViewProviderDeclarations();
    }

    saveConfig(){
        this.router.navigate([]);
        
        const serConfigs = ProvidersRegistry.serialize();
        const serWidgets = this.widgetsService.serialize();

        const serWidgetsStr = JSON.stringify(serWidgets, null, 2);
        const serConfigsStr = JSON.stringify(serConfigs, null, 2);
        console.log(serWidgetsStr)
        const url = 'http://'+this.NODE_URL+':3000/sendConfig';

        const params = new HttpParams()
            .set('userId', "1")
            .set('provConf', serConfigsStr)
            .set('widgConf', serWidgetsStr);

        this.http.request('POST', url, {
            params,
            responseType: 'json',
        }).subscribe((val) => {
            console.log("Configuration persisted successfully", val);
            this.widgetsService.removeAll(this.DASHBOARD_ID);

            ProvidersRegistry.populateFromJsonString(serConfigsStr);
            this.widgetsService.populateFromJsonString(serWidgetsStr, this.DASHBOARD_ID);
        });        
    }

    loadConfig(){
        this.router.navigate([]);
        this.isLoadingData = true;
        
        const url = 'http://'+this.NODE_URL+':3000/getConfig';

        this.http.request('GET', url, {
            responseType: 'json'
        }).subscribe((data: any) => {
            console.log("Configuration retrieved successfully", data);
                
            const serWidgetsStr = JSON.stringify(data.widgetConf, null, 2);
            const serConfigsStr = JSON.stringify(data.providerConf, null, 2);

            this.widgetsService.removeAll(this.DASHBOARD_ID);

            setTimeout(() => {
                this.isLoadingData = false;
                ProvidersRegistry.populateFromJsonString(serConfigsStr);
                this.widgetsService.populateFromJsonString(serWidgetsStr, this.DASHBOARD_ID);
            }, 3000);
        });
    }
}
