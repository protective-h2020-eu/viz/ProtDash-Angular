import {
    ChangeDetectorRef,
    Component,
    ComponentFactoryResolver,
    ComponentRef,
    EventEmitter,
    Injector,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
    Type,
    ViewChild,
    ViewContainerRef,
    HostListener,
    ElementRef,
} from '@angular/core';

import { Config } from '../../../../lib/config';
import { DataProviderDeclarationModel } from '../../../../lib/data-provider-declaration';
import { DataProvider } from '../../../../lib/data-provider.model';
import { MappingProvider } from '../../../../lib/mapping-provider';
import { MappingProviderDeclarationModel } from '../../../../lib/mapping-provider-declaration';
import { ProvidersRegistry } from '../../../../lib/providers-registry';
import { ViewProvider } from '../../../../lib/view-provider.model';
import { BaseEditorComponent } from '../../../../view-providers/base-editor.component';
import { BaseViewProviderComponent } from '../../../../view-providers/base-view-provider.component';
import { WidgetControls } from './widget-controls.model';
import { WidgetItem } from './widget-item.model';
import { ObservableMedia } from '@angular/flex-layout';

@Component({
    selector: 'app-dashboard-widget',
    templateUrl: './dashboard-widget.component.html',
    styleUrls: ['./dashboard-widget.component.css']
})
export class DashboardWidgetComponent implements WidgetControls, OnInit, OnDestroy, OnChanges {
    /**
     * View mode
     */
    @Input() viewMode = 'widget';
    @Output() viewModeChange = new EventEmitter<string>();

    /**
     * ID of the widget
     */
    @Input() wid: string;

    /**
     * Name of the widget
     */
    @Input() name: string;
    @Output() nameChange = new EventEmitter<string>();

    /**
     * View provider of the widget
     */
    @Input() viewProviderType: Type<ViewProvider>;
    @Output() viewProviderTypeChange = new EventEmitter<Type<ViewProvider>>();

    /**
     * View Provider Editor Config Object
     */
    @Input() viewProdiverEditorConfig: Config;

    /**
     * Data provider of the widget
     */
    @Input() dataProviderType: Type<DataProvider>;
    @Output() dataProviderTypeChange = new EventEmitter<Type<DataProvider>>();

    /**
     * Data Provider Editor Config Object
     */
    @Input() dataProviderEditorConfig: Config;

    /**
     * Mapping Providider of the widget
     */
    @Input() mappingProviderType: Type<MappingProvider>;
    @Output() mappingProviderTypeChange = new EventEmitter<Type<MappingProvider>>();
    // store reference so we don't have to re-create it
    private mappingProvider: MappingProvider;

    /**
     * Emits event when Remove was clicked
     */
    @Output() removeClick = new EventEmitter<any>();

    /**
     * Emits event when Widget is resizing
     */
    @Output() widgetSizeChange = new EventEmitter<any>();

    /**
     * Reference to view provider
     */
    private viewProviderRef: ComponentRef<BaseViewProviderComponent>;

    /**
    * Reference to view provider config
    */
    private viewProviderEditorRef: ComponentRef<BaseEditorComponent>;

    /**
    * Reference to data provider config
    */
    private dataProviderEditorRef: ComponentRef<BaseEditorComponent>;

    /**
     * Flags for whether config views exists
     */
    private hasViewProviderEditor = false;
    private hasDataProviderEditor = false;

    /**
     * Reference to view provider container
     */
    @ViewChild('viewProviderContainer', { read: ViewContainerRef }) viewProviderContainer;

    /**
     * Reference to view provider container
     */
    @ViewChild('viewProviderEditorContainer', { read: ViewContainerRef }) viewProviderEditorContainer;

    /**
     * Reference to view provider container
     */
    @ViewChild('dataProviderEditorContainer', { read: ViewContainerRef }) dataProviderEditorContainer;

    /**
     * List of available data providers
     */
    public availableDataProviders: DataProviderDeclarationModel[] = [];

    /**
     * Lit of available mapping providers
     */
    public availableMappingProviders: MappingProviderDeclarationModel[] = [];

    /**
     * Errors / Warnings / Indicators
     */
    public errorMessage: string;
    public warningMessage: string;
    public loadingMessage: string;
    public isIndicatorVisible = false;
    private indicatorCounter = 0;

    /**
     * Resizing related
     */
    private isResizing = false;
    private resizeAllowed = true;

    constructor(
        private elementRef: ElementRef,
        private resolver: ComponentFactoryResolver,
        private injector: Injector,
        private cdr: ChangeDetectorRef,
        private media: ObservableMedia) { }

    displayErrorMessage(errorMessage: string) {
        this.errorMessage = errorMessage;
        this.cdr.detectChanges();
    }

    displayWarningMessage(warningMessage: string) {
        this.warningMessage = warningMessage;
        this.cdr.detectChanges();
    }

    clearErrorMessage() {
        this.errorMessage = null;
        this.cdr.detectChanges();
    }

    clearWarningMessage() {
        this.warningMessage = null;
        this.cdr.detectChanges();
    }

    showLoadingIndicator(message?: string) {
        this.indicatorCounter++;
        this.loadingMessage = message || 'Loading...';
        this.isIndicatorVisible = true;
        this.cdr.detectChanges();
    }

    hideLoadingIndicator() {
        this.indicatorCounter--;
        if (this.indicatorCounter <= 0) {
            this.isIndicatorVisible = false;
            this.indicatorCounter = 0;
            this.cdr.detectChanges();
        }
    }

    ngOnInit() {
        this.determineAvailableProviders();
        this.media.subscribe(change => {
            // prevent resize when viewing in column mode
            if (change.mqAlias === 'md'
                || change.mqAlias === 'sm'
                || change.mqAlias === 'xs') {
                this.resizeAllowed = false;
            } else {
                this.resizeAllowed = true;
            }
        });
    }

    ngOnDestroy(): void {
        if (this.viewProviderRef) {
            this.viewProviderRef.destroy();
        }
        if (this.viewProviderEditorRef) {
            this.viewProviderEditorRef.destroy();
        }
        if (this.dataProviderEditorRef) {
            this.dataProviderEditorRef.destroy();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        // console.log('widget changes', changes);

        const viewProviderChange = changes['viewProviderType'];
        const dataProviderChange = changes['dataProviderType'];
        const mappingProviderChange = changes['mappingProviderType'];
        const viewModeChange = changes['viewMode'];

        if (viewProviderChange && viewProviderChange.currentValue) {
            this.initializeViewProvider();
        }
        if (dataProviderChange && dataProviderChange.currentValue) {
            this.initializeDataProvider();
            this.determineAvailableProviders();
        }
        if (mappingProviderChange && mappingProviderChange.currentValue) {
            this.initializeMappingProvider();
        }
        if (viewModeChange && viewModeChange.currentValue) {
            this.propagateViewModeChange();
        }
    }

    initializeViewProvider() {
        //
        //  Clear containers
        //
        this.hasViewProviderEditor = false;
        this.viewProviderContainer.clear();
        this.viewProviderEditorContainer.clear();
        if (this.viewProviderEditorRef) {
            this.viewProviderEditorRef.destroy();
        }

        /**
         * Get View / Data Provider Declarations
         */
        const dpDeclaration = ProvidersRegistry.getDataProviderDeclarationByType(this.dataProviderType);
        const vpDeclaration = ProvidersRegistry.getViewProviderDeclarationByType(this.viewProviderType);

        /**
         * Get View / Data Provider Global Configs
         */
        const dpConfig = ProvidersRegistry.getConfigByDeclaration(dpDeclaration);
        const vpConfig = ProvidersRegistry.getConfigByDeclaration(vpDeclaration);

        /**
         * Resolve Data Provider (Service) using injector
         */
        const dataProvider = this.injector.get(this.dataProviderType, null);

        /**
         * Resolve view provider
         */
        const vpFactory = this.resolver.resolveComponentFactory(this.viewProviderType);
        this.viewProviderRef = this.viewProviderContainer.createComponent(vpFactory);
        // update values
        this.viewProviderRef.instance.dataProvider = dataProvider;
        this.viewProviderRef.instance.mappingProvider = this.mappingProvider;
        this.viewProviderRef.instance.viewProviderConfig = vpConfig;
        this.viewProviderRef.instance.widgetControls = this;

        // view provider
        const viewProvider = <ViewProvider>this.viewProviderRef.instance;

        // resolve view provider editor view
        this.initializeViewProviderEditor();

        // update inputs on the data provider editor side
        if (this.dataProviderEditorRef && this.dataProviderEditorRef.instance) {
            const dpEditor = this.dataProviderEditorRef.instance;
            dpEditor.viewProvider = viewProvider;
            dpEditor.dataProvider = dataProvider;
            dpEditor.mappingProvider = this.mappingProvider;
            dpEditor.viewProviderConfig = vpConfig;
        }
    }

    initializeDataProvider() {
        this.hasDataProviderEditor = false;
        this.dataProviderEditorContainer.clear();
        if (this.dataProviderEditorRef) {
            this.dataProviderEditorRef.destroy();
        }

        /**
         * Resolve Data Provider (Service) using injector
         */
        const viewProvider = this.viewProviderRef ? this.viewProviderRef.instance : null;
        const dataProvider = this.injector.get(this.dataProviderType, null);

        /**
         * Get View / Data Provider Declarations
         */
        const dpDeclaration = ProvidersRegistry.getDataProviderDeclarationByType(this.dataProviderType);
        const vpDeclaration = ProvidersRegistry.getViewProviderDeclarationByType(this.viewProviderType);

        /**
         * Get View / Data Provider Global Configs
         */
        const dpConfig = ProvidersRegistry.getConfigByDeclaration(dpDeclaration);
        const vpConfig = ProvidersRegistry.getConfigByDeclaration(vpDeclaration);

        // update inputs on the view provider side
        if (this.viewProviderRef && this.viewProviderRef.instance) {
            // get old values
            const oldMpValue = this.viewProviderRef.instance.mappingProvider;
            // update with new values
            this.viewProviderRef.instance.dataProvider = dataProvider;
            this.viewProviderRef.instance.mappingProvider = this.mappingProvider;
        }

        // update inputs on the data provider editor side
        if (this.viewProviderEditorRef && this.viewProviderEditorRef.instance) {
            // update values
            this.viewProviderEditorRef.instance.dataProvider = dataProvider;
            this.viewProviderEditorRef.instance.mappingProvider = this.mappingProvider;
            this.viewProviderEditorRef.instance.dataProviderConfig = dpConfig;
        }

        this.initializeDataProviderEditor();
    }

    initializeMappingProvider() {

        this.mappingProvider = this.mappingProviderType ? new this.mappingProviderType() : null;

        // update inputs on the view provider side
        if (this.viewProviderRef && this.viewProviderRef.instance) {
            // get old values
            const oldMpValue = this.viewProviderRef.instance.mappingProvider;
            // update with new values
            this.viewProviderRef.instance.mappingProvider = this.mappingProvider;
        }

        // update inputs on the data provider editor side
        if (this.viewProviderEditorRef && this.viewProviderEditorRef.instance) {
            // update values
            this.viewProviderEditorRef.instance.mappingProvider = this.mappingProvider;
        } else {
            this.initializeViewProviderEditor();
        }

        // update inputs on the data provider editor side
        if (this.dataProviderEditorRef && this.dataProviderEditorRef.instance) {
            this.dataProviderEditorRef.instance.mappingProvider = this.mappingProvider;
        } else {
            this.initializeDataProviderEditor();
        }
    }

    private initializeViewProviderEditor() {
        /**
         * Resolve Data Provider (Service) using injector
         */
        const viewProvider = this.viewProviderRef ? this.viewProviderRef.instance : null;
        const dataProvider = this.injector.get(this.dataProviderType, null);

        /**
         * Get View / Data Provider Declarations
         */
        const dpDeclaration = ProvidersRegistry.getDataProviderDeclarationByType(this.dataProviderType);
        const vpDeclaration = ProvidersRegistry.getViewProviderDeclarationByType(this.viewProviderType);

        /**
         * Get View / Data Provider Global Configs
         */
        const dpConfig = ProvidersRegistry.getConfigByDeclaration(dpDeclaration);
        const vpConfig = ProvidersRegistry.getConfigByDeclaration(vpDeclaration);

        const vpEditorView = vpDeclaration ? vpDeclaration.editorView : null;
        if (vpEditorView && viewProvider && this.mappingProvider) {
            // resolve component
            const vpEditorFactory = this.resolver.resolveComponentFactory(vpEditorView);
            this.viewProviderEditorRef = this.viewProviderEditorContainer.createComponent(vpEditorFactory);
            const vpEditor = this.viewProviderEditorRef.instance;
            // update values
            vpEditor.viewProvider = viewProvider;
            vpEditor.dataProvider = dataProvider;
            vpEditor.mappingProvider = this.mappingProvider;
            vpEditor.editorConfig = this.viewProdiverEditorConfig;
            vpEditor.viewProviderConfig = vpConfig;
            vpEditor.dataProviderConfig = dpConfig;
            vpEditor.widgetControls = this;
            this.hasViewProviderEditor = true;
        }
    }

    private initializeDataProviderEditor() {
        /**
         * Resolve Data Provider (Service) using injector
         */
        const viewProvider = this.viewProviderRef ? this.viewProviderRef.instance : null;
        const dataProvider = this.injector.get(this.dataProviderType, null);

        /**
         * Get View / Data Provider Declarations
         */
        const dpDeclaration = ProvidersRegistry.getDataProviderDeclarationByType(this.dataProviderType);
        const vpDeclaration = ProvidersRegistry.getViewProviderDeclarationByType(this.viewProviderType);

        /**
         * Get View / Data Provider Global Configs
         */
        const dpConfig = ProvidersRegistry.getConfigByDeclaration(dpDeclaration);
        const vpConfig = ProvidersRegistry.getConfigByDeclaration(vpDeclaration);

        const dpEditorView = dpDeclaration ? dpDeclaration.editorView : null;
        if (dpEditorView && dataProvider && this.mappingProvider) {
            // resolve component
            const dpEditorFactory = this.resolver.resolveComponentFactory(dpEditorView);
            this.dataProviderEditorRef = this.dataProviderEditorContainer.createComponent(dpEditorFactory);
            const dpEditor = this.dataProviderEditorRef.instance;
            // pass data provider to config view
            dpEditor.viewProvider = viewProvider;
            dpEditor.dataProvider = dataProvider;
            dpEditor.mappingProvider = this.mappingProvider;
            dpEditor.editorConfig = this.dataProviderEditorConfig;
            dpEditor.viewProviderConfig = vpConfig;
            dpEditor.dataProviderConfig = dpConfig;
            dpEditor.widgetControls = this;
            this.hasDataProviderEditor = true;
        }
    }


    propagateViewModeChange() {
        if (this.viewProviderRef && this.viewProviderRef.instance) {
            this.viewProviderRef.instance.viewMode = this.viewMode;
        }
    }

    isWidgetViewMode() {
        return this.viewMode === 'grid';
    }

    isFullViewMode() {
        return this.viewMode === 'view';
    }

    isEditViewMode() {
        return this.viewMode === 'edit';
    }

    onView() {
        this.viewModeChange.emit('view');
    }

    onEdit() {
        this.viewModeChange.emit('edit');
    }

    onRemove() {
        this.removeClick.emit({});
    }

    onChangeDataProvider(newProvider) {
        this.dataProviderType = newProvider;
        this.determineAvailableProviders();
        if (this.availableMappingProviders.length > 0) {
            const alreadySelected = this.availableMappingProviders.some((mpDecl) => mpDecl.provider === this.mappingProviderType);
            if (!alreadySelected) {
                this.mappingProviderType = this.availableMappingProviders[0].provider;
            }
        } else {
            this.mappingProviderType = null;
        }
        this.mappingProviderTypeChange.emit(this.mappingProviderType);
        this.dataProviderTypeChange.emit(this.dataProviderType);
        this.clearIndicators();
    }

    onChangeMappingProvider(newProvider) {
        this.mappingProviderTypeChange.emit(newProvider);
    }

    onResizeStart(event) {
        if (this.resizeAllowed && this.isWidgetViewMode()) {
            this.isResizing = true;
            event.preventDefault();
            event.stopPropagation();
        }
    }

    @HostListener('document:mousemove', ['$event']) onResize(event) {
        if (this.isResizing) {
            this.widgetSizeChange.emit(event);
        }
    }

    @HostListener('document:mouseup') onResizeEnd(event) {
        if (this.isResizing) {
            this.isResizing = false;
        }
    }

    onNameChange(event) {
        this.nameChange.emit(event.target.value);
    }

    canConfigureViewProvider() {
        return this.hasViewProviderEditor;
    }

    canConfigureDataProvider() {
        return this.hasDataProviderEditor;
    }

    clearIndicators() {
        // clear any errors or warnings
        this.clearErrorMessage();
        this.clearWarningMessage();
        this.hideLoadingIndicator();
    }

    determineAvailableProviders() {
        const vpDeclaration = ProvidersRegistry.getViewProviderDeclarationByType(this.viewProviderType);
        // if provider(s) was not specified
        // then we can't determine potential list of data providers
        // which support this view provider
        if (!vpDeclaration) {
            return [];
        }

        const dpDeclaration = ProvidersRegistry.getDataProviderDeclarationByType(this.dataProviderType);
        const dpDeclarations = ProvidersRegistry.getDataProviderDeclarations();
        const mpDeclarations = ProvidersRegistry.getMappingProviderDeclarations();

        const newMpDeclarations = [];
        if (dpDeclaration) {
            mpDeclarations.forEach(mpDeclItem => {
                if (mpDeclItem.viewProviderDataTypes.includes(vpDeclaration.dataType)
                    && mpDeclItem.dataProviderDataTypes.includes(dpDeclaration.dataType)) {
                    newMpDeclarations.push(mpDeclItem);
                }
            });
        }

        this.availableDataProviders = dpDeclarations;
        this.availableMappingProviders = newMpDeclarations;
    }
}
