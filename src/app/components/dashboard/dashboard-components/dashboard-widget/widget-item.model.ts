import { ViewProvider } from '../../../../lib/view-provider.model';
import { DataProvider } from '../../../../lib/data-provider.model';
import { Type } from '@angular/core';
import { MappingProvider } from '../../../../lib/mapping-provider';
import { Config } from '../../../../lib/config';

export interface WidgetItem {

    /**
     * ID
     */
    id: string;

    /**
     * Widget Name
     */
    name: string;

    /**
     * View provider (component)
     */
    viewProviderType: Type<ViewProvider>;

    /**
     * Data Provider for the view
     */
    dataProviderType: Type<DataProvider>;

    /**
     * Mapping provider
     */
    mappingProviderType: Type<MappingProvider>;

    /**
     * Configuration for view provider editor
     */
    viewProviderEditorConfig: Config;

    /**
     * Configuration for data provider editor
     */
    dataProviderEditorConfig: Config;

    grid: {
        order: number;
        width: number;
    };
}
