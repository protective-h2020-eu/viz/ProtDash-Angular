export interface WidgetControls {
    displayErrorMessage(errorMessage: string);
    displayWarningMessage(warningMessage: string);
    clearErrorMessage();
    clearWarningMessage();
    showLoadingIndicator(message ?: string);
    hideLoadingIndicator();
}
