import { Config } from '../../../../lib/config';
import { ProvidersRegistry } from '../../../../lib/providers-registry';
import { WidgetItem } from './widget-item.model';

export class WidgetItemFormatter {

    /**
     * Converts WidgetItem to JSON
     */
    public static toJSON(widget: WidgetItem): any {
        // data provider config
        const dpConfig = ProvidersRegistry.getDataProviderDeclarationByType(widget.dataProviderType);
        // vewi provider config
        const vpConfig = ProvidersRegistry.getViewProviderDeclarationByType(widget.viewProviderType);
        // mapping provider config
        const mpConfig = ProvidersRegistry.getMappingProviderDeclarationByType(widget.mappingProviderType);

        return {
            id: widget.id,
            name: widget.name,
            grid: widget.grid,
            dataProvider: dpConfig && dpConfig.id,
            viewProvider: vpConfig && vpConfig.id,
            mappingProvider: mpConfig && mpConfig.id,
            viewProviderEditorConfig: widget.viewProviderEditorConfig.toJSON(),
            dataProviderEditorConfig: widget.dataProviderEditorConfig.toJSON()
        };
    }

    /**
     * Restores Widget Item (and providers class references)
     * from JSON object produces by toJSON
     * @param input JSON input produced by toJSON
     */
    public static fromJSON(input: any): WidgetItem {
        // data provider type
        const dpType = ProvidersRegistry.getDataProviderById(input.dataProvider);
        // vewi provider type
        const vpType = ProvidersRegistry.getViewProviderById(input.viewProvider);
        // mapping provider type
        const mpType = ProvidersRegistry.getMappingProviderById(input.mappingProvider);

        return {
            id: input.id,
            name: input.name,
            grid: input.grid,
            dataProviderType: dpType,
            viewProviderType: vpType,
            mappingProviderType: mpType,
            viewProviderEditorConfig: Config.fromJSON(input.viewProviderEditorConfig),
            dataProviderEditorConfig: Config.fromJSON(input.dataProviderEditorConfig)
        };
    }
}
