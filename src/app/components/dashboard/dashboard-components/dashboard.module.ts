import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DashboardWidgetComponent } from './dashboard-widget/dashboard-widget.component';
import { DashboardGridComponent } from './dashboard-grid/dashboard-grid.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsService } from './widgets.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        BsDropdownModule.forRoot(),
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        FlexLayoutModule
    ],
    providers: [WidgetsService],
    declarations: [DashboardWidgetComponent, DashboardGridComponent],
    exports: [DashboardWidgetComponent, DashboardGridComponent]
})
export class DashboardModule { }
