import { Component, Input, OnChanges, OnInit, SimpleChanges, ElementRef, HostListener, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { WidgetItem } from '../dashboard-widget/widget-item.model';
import { WidgetsService } from '../widgets.service';

@Component({
    selector: 'app-dashboard-grid',
    templateUrl: './dashboard-grid.component.html',
    styleUrls: ['./dashboard-grid.component.css']
})
export class DashboardGridComponent implements OnInit, OnChanges {

    /**
     * Id of a dashboard
     */
    @Input() dashboard: string;

    /**
     * Array of widgets which will be rendered by the grid
     */
    public widgets: WidgetItem[];

    /**
     * Current View Mode
     */
    public viewMode = 'grid';

    /**
     * Currently selected widget
     */
    public selectedWidget: WidgetItem;

    constructor(
        private elementRef: ElementRef,
        private widgetsService: WidgetsService,
        private route: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.widgetsService.widgets(this.dashboard).subscribe(widgets => {
            this.widgets = widgets;
        });
        this.route.queryParams.subscribe(params => {
            this.setViewMode(params.mode);
            if (params.wid) {
                this.selectedWidget = this.widgets.find(v => v.id === params.wid);
                if ((this.isEditorView() || this.isFullView()) && !this.selectedWidget) {
                    // navigate to grid view in case element is no longer there
                    this.router.navigate([]);
                }
            }
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        // console.log('grid changes', changes);
    }

    onViewModeChange(newMode: string, wid: string) {
        this.router.navigate([], {
            queryParams: {
                mode: newMode,
                wid: wid
            }
        });
    }

    onRemoveClicked(wid: string) {
        this.router.navigate([]);
        this.widgetsService.removeWidget(this.dashboard, wid);
    }

    setViewMode(mode: string) {
        if (mode === 'grid') {
            this.viewMode = 'grid';
        } else if (mode === 'view') {
            this.viewMode = 'view';
        } else if (mode === 'edit') {
            this.viewMode = 'edit';
        } else {
            // default view is grid viewА
            this.viewMode = 'grid';
        }
    }

    isGridView() {
        return this.viewMode === 'grid';
    }

    isFullView() {
        return this.viewMode === 'view';
    }

    isEditorView() {
        return this.viewMode === 'edit';
    }

    isWidgetVisible(widget: WidgetItem) {
        if (this.isFullView() || this.isEditorView()) {
            return this.selectedWidget === widget;
        } else {
            return true;
        }
    }

    resizeWidget(event: MouseEvent, widget: WidgetItem) {
        const rect = this.elementRef.nativeElement.getBoundingClientRect();
        const perX = ((event.movementX / rect.width) * 100);

        // console.log(this.name);
        // console.log('client', event.clientX, event.clientY);
        // console.log('page', event.pageX, event.pageY);
        // console.log('offset', event.offsetX, event.offsetY);
        // console.log(rect);
        // console.log('calc', perX, event.pageY - this.rect.top);
        // console.log('-----');
        // clamp values
        widget.grid.width = this.clampWidgetSize(20, widget.grid.width + perX, 100);
    }

    private clampWidgetSize(min, value, max) {
        return Math.min(Math.max(value, min), max);
    }
}
