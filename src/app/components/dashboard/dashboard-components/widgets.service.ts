import { Injectable, Type } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { Config } from '../../../lib/config';
import { DataProvider } from '../../../lib/data-provider.model';
import { MappingProvider } from '../../../lib/mapping-provider';
import { ViewProvider } from '../../../lib/view-provider.model';
import { WidgetItemFormatter } from './dashboard-widget/widget-item-formatter';
import { WidgetItem } from './dashboard-widget/widget-item.model';

@Injectable()
export class WidgetsService {

    private subjectMap = new Map<string, BehaviorSubject<WidgetItem[]>>([]);
    private widgetMap = new Map<string, WidgetItem[]>();

    constructor() {

    }

    private createModelToWidget(model: WidgetCreateModel) {
        return {
            id: String(Date.now() + Math.random()),
            name: model.name,
            grid: Object.assign({}, {
                order: 0,
                width: 50
            }, model.grid),
            dataProviderType: model.dataProviderType || null,
            viewProviderType: model.viewProviderType || null,
            mappingProviderType: model.mappingProviderType || null,
            viewProviderEditorConfig: model.viewProviderEditorConfig || new Config(),
            dataProviderEditorConfig: model.dataProviderEditorConfig || new Config()
        };
    }

    private ensureMapsFor(dashboardName: string) {
        if (this.widgetMap.has(dashboardName) === false) {
            this.widgetMap.set(dashboardName, []);
        }
        if (this.subjectMap.has(dashboardName) === false) {
            this.subjectMap.set(dashboardName, new BehaviorSubject([]));
        }
    }

    private getOrderingNumber(widgets: WidgetItem[]): number {
        const sorted = widgets.sort(v => v.grid.order);
        if (sorted.length) {
            return sorted[sorted.length - 1].grid.order + 1;
        } else {
            return 0;
        }
    }

    public addWidget(dashboardName: string, widget: WidgetCreateModel) {
        this.ensureMapsFor(dashboardName);
        const widgets = this.widgetMap.get(dashboardName);
        const model = this.createModelToWidget(widget);
        model.grid.order = this.getOrderingNumber(widgets);
        widgets.push(model);
        this.subjectMap.get(dashboardName).next(widgets);
    }

    public widgets(dashboardName: string): Observable<WidgetItem[]> {
        this.ensureMapsFor(dashboardName);
        return this.subjectMap.get(dashboardName).asObservable();
    }

    public removeWidget(dashboardName: string, widgetId: string) {
        this.ensureMapsFor(dashboardName);
        const widgets = this.widgetMap.get(dashboardName);
        const indexOf = widgets.map(v => v.id).indexOf(widgetId);
        if (indexOf > -1) {
            widgets.splice(indexOf, 1);
            this.subjectMap.get(dashboardName).next(widgets);
        }
    }

    public removeAll(dashboardName: string) {
        this.ensureMapsFor(dashboardName);
        this.widgetMap.set(dashboardName, []);
        this.subjectMap.get(dashboardName).next([]);
    }

    public serialize() {
        const serMap = {};
        this.widgetMap.forEach((widgets, dashboardName) => {
            serMap[dashboardName] = widgets.map(widget => WidgetItemFormatter.toJSON(widget));
        });
        return serMap;
    }

    public populateFromJsonString(jsonStr: string, dashboardID: string) {
        const serMap = JSON.parse(jsonStr);
        Object.keys(serMap).forEach((key) => {
            if (dashboardID === key){
                const widgets = serMap[key].map(widget => WidgetItemFormatter.fromJSON(widget));
                this.widgetMap.set(key, widgets);
                this.subjectMap.get(key).next(widgets);
            }            
        });
    }
}

export interface WidgetCreateModel {
    /**
     * Widget Name
     */
    name: string;

    /**
     * View provider (component)
     */
    viewProviderType: Type<ViewProvider>;

    /**
     * Data Provider for the view
     */
    dataProviderType?: Type<DataProvider>;

    /**
     * Mapping provider
     */
    mappingProviderType?: Type<MappingProvider>;

    /**
     * Configuration for view provider editor
     */
    viewProviderEditorConfig?: Config;

    /**
     * Configuration for data provider editor
     */
    dataProviderEditorConfig?: Config;

    /**
     * Grid related
     */
    grid?: {
        order?: number;
        width?: number;
    };
}
