import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routes';
import { AlertSearchModule } from './components/alert-search/alert-search.module';
import { CDGModule } from './components/CDG/CDG.module';
import { Sc1Module } from './components/dashboard/sc1/sc1.module';
import { ErrorComponent } from './components/error/error.component';
import { MainComponent } from './components/main/main.component';
import { DataProvidersModule } from './data-providers/data-providers.module';
import { MappingProvidersModule } from './mapping-providers/mapping-providers.module';
import { ViewProvidersModule } from './view-providers/view-providers.module';

@NgModule({
    declarations: [AppComponent, MainComponent, ErrorComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        DataProvidersModule,
        ViewProvidersModule,
        MappingProvidersModule,
        Sc1Module,
        CDGModule,
        AlertSearchModule,
        BsDropdownModule.forRoot(),
        AccordionModule.forRoot()
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
