import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AlertSearchEditorComponent } from './components/alert-search/alert-search-editor/alert-search-editor.component';
import { CdgtableComponent } from './components/CDG/cdgtable/cdgtable.component';
import { FileUploadComponent } from './components/CDG/FileUpload/FileUpload.component';
import { ContextAwarenessComponent } from './components/dashboard/context-awareness/context-awareness.component';
import { HomeDashComponent } from './components/dashboard/home-dash/home-dash.component';
import { MetaAlertsComponent } from './components/dashboard/meta-alerts/meta-alerts.component';
import { StatisticsComponent } from './components/dashboard/statistics/statistics.component';
import { ErrorComponent } from './components/error/error.component';

const appRoutes: Routes = [
    { path: '', component: HomeDashComponent, pathMatch: 'full' },
    { path: 'statistics', component: StatisticsComponent },
    { path: 'alerts', component: AlertSearchEditorComponent },
    { path: 'meta-alerts', component: MetaAlertsComponent },
    { path: 'context-awareness', component: ContextAwarenessComponent },
    { path: 'upload', component: FileUploadComponent },
    { path: 'CDG_table', component: CdgtableComponent },
    { path: 'error/:errorType', component: ErrorComponent },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: '/error/404'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
