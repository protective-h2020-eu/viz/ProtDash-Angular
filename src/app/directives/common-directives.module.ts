import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResizeSensorDirective } from './resize-sensor.directive';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [ResizeSensorDirective],
    exports: [ResizeSensorDirective]
})
export class CommonDirectivesModule { }
