import { AfterViewInit, Directive, ElementRef, EventEmitter, NgZone, OnDestroy, OnInit, Output } from '@angular/core';
import ResizeObserver from 'resize-observer-polyfill';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[resizeSensor]'
})
export class ResizeSensorDirective implements OnInit, OnDestroy, AfterViewInit {

    private observer: ResizeObserver;

    @Output() computedResize = new EventEmitter<{ width: number; height: number; }>();

    constructor(private elementRef: ElementRef, private zone: NgZone) { }

    ngOnInit(): void {
    }

    ngOnDestroy() {
        if (this.observer) {
            this.zone.runOutsideAngular(() => {
                this.observer.disconnect();
            });
        }
    }

    ngAfterViewInit() {
        // very important to run this outside of Angular Zone
        this.zone.runOutsideAngular(() => {
            this.observer = new ResizeObserver((entries, observer) => {
                for (const entry of entries) {
                    if (this.elementRef.nativeElement === entry.target) {
                        const { width, height } = entry.contentRect;
                        this.zone.run(() => {
                            this.computedResize.emit({
                                width: width,
                                height: height
                            });
                        });
                    }
                }
            });

            this.observer.observe(this.elementRef.nativeElement);
        });
    }
}
