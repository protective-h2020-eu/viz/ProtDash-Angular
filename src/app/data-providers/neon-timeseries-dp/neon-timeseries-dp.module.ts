import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NeonTimeseriesDataProvider } from './neon-timeseries-dp.service';

import { NeonTimeseriesDpEditorComponent } from './neon-timeseries-dp-editor/neon-timeseries-dp-editor.component';
import { FormsModule } from '@angular/forms';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        CollapseModule.forRoot(),
        BsDatepickerModule.forRoot()
    ],
    providers: [NeonTimeseriesDataProvider],
    declarations: [
      NeonTimeseriesDpEditorComponent
    ],
    entryComponents: [NeonTimeseriesDpEditorComponent]
})
export class NeonTimeseriesDpModule { }
