export interface NeonTimeseriesDpEditorConfig {
    dbName?: string;
    tableName?: string;
    granularity?: string;
    dateField?: string;
    selectedGroupByField?: any;
    filters?: {
        field?: string;
        operator?: string;
        value?: any
    }[];
    timeFilter?:{
        field?: string;
        from?: Date;
        to?: Date;
    };
    ewmaAlpha?: number;
    harcodedView?: boolean;
}
