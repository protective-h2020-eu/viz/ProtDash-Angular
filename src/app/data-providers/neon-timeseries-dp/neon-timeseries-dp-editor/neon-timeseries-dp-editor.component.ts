import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as neon from 'neon-framework';

import { ConfigChanges } from '../../../lib/config';
import { BaseEditorComponent } from '../../../view-providers/base-editor.component';
import { NeonTimeseriesDataProvider } from '../neon-timeseries-dp.service';
import { NeonTimeseriesDpEditorConfig } from './neon-timeseries-dp-editor-config.model';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
    selector: 'app-neon-timeseries-dp-editor',
    templateUrl: './neon-timeseries-dp-editor.component.html',
    styleUrls: ['./neon-timeseries-dp-editor.component.css']
})
export class NeonTimeseriesDpEditorComponent extends BaseEditorComponent implements OnInit {

  public isNeonReady = false;
  public processing = false;
  public neonDp: NeonTimeseriesDataProvider;
  public bsConfig: Partial<BsDatepickerConfig> = Object.assign({}, { containerClass: 'theme-dark-blue' });
  //
  //  UI Bindings
  //
  public selectedDb: string = null;
  public selectedTable: string = null;
  public dateField: string = null;
  public filters: {
      field: string;
      operator: string;
      value: any;
  }[] = [];
  public granularity: string = null;
  public selectedLimit = 50000;
  public selectedGroupByField: string = null;

  public selectedTimeFilterField: string = null;
  public selectedTimeFilterFrom: Date = null;
  public selectedTimeFilterTo: Date = null;

  public ewmaAlpha:number = null;
  public hardcodedView:boolean = false;
  //
  //  For select population
  //
  public dbNames: string[] = [];
  public tableNames: string[] = [];
  public fieldNames: string[] = [];

  //
  //  FOR TESTING PURPOSES
  //
  public lastResponse: any;

  constructor(private cdr: ChangeDetectorRef) {
      super();
  }

  ngOnInit() {
    // keep local reference so we don't have to cast it all the time
    this.neonDp = <NeonTimeseriesDataProvider>this.dataProvider;
    this.connectToNeon();
  }

  public connectToNeon() {
    this.showLoadingIndicator();
    this.clearErrorMessage();
    this.processing = true;
    this.neonDp.testDataProvider().subscribe(isReady => {
        this.hideLoadingIndicator();
        this.isNeonReady = isReady;
        this.processing = false;
        if (isReady) {
            this.init();
            if (this.canSendQuery()) {
                this.sendQuery();
            }
        } else {
            this.displayErrorMessage('NEON is not ready yet');
        }
    }, (error) => {
        this.displayErrorMessage(this.neonErrToStr(error));
        this.hideLoadingIndicator();
        this.processing = false;
    });
}

private init() {
    this.updateDatabaseNames();
    // if already has database selected, then update table names
    if (this.selectedDb) {
        this.updateTableNames();
    }
    // if already has database and table selected, then populate fields
    if (this.selectedDb && this.selectedTable) {
        this.updateFieldNames();
    }
}

onEditorConfigChanges(changes: ConfigChanges) {
    const dbNameChange = changes['dbName'];
    const tableNameChange = changes['tableName'];
    const dateFieldChange = changes['dateField'];
    const filtersChange = changes['filters'];
    const timeFilterChange = changes['timeFilter'];
    const granularityChange = changes['granularity'];
    const selectedAggFieldChange = changes['selectedGroupByField']
    const selectedEwmaAlphaChange = changes['ewmaAlpha']
    const hardcodedViewChange = changes['hardcodedView']

    if (dbNameChange) {
        this.selectedDb = dbNameChange.currentValue;
        if (!dbNameChange.firstChange && dbNameChange.currentValue) {
            this.updateTableNames();
        }
    }
    if (tableNameChange) {
        this.selectedTable = tableNameChange.currentValue;
        if (!tableNameChange.firstChange && tableNameChange.currentValue) {
            this.updateFieldNames();
        }
    }
    if (filtersChange){
        const filters = filtersChange.currentValue;
        console.log(filters)
        if (filters && filtersChange.firstChange){
            this.filters = filters;
        }
    }
    if (timeFilterChange){
        const timeFilter = timeFilterChange.currentValue;
        if (timeFilter) {
            // still accept null though
            if (timeFilter.field !== undefined) {
                this.selectedTimeFilterField = timeFilter.field;
            }
            if (timeFilter.from !== undefined) {
                this.selectedTimeFilterFrom = timeFilter.from;
            }
            if (timeFilter.to !== undefined) {
                this.selectedTimeFilterTo = timeFilter.to;
            }
        }
    }
    if (dateFieldChange){
        this.dateField = dateFieldChange.currentValue;
    }
    if (granularityChange){
        this.granularity = granularityChange.currentValue;
    }
    if (selectedAggFieldChange){
        this.selectedGroupByField = selectedAggFieldChange.currentValue;
    }
    if (selectedEwmaAlphaChange){
        this.ewmaAlpha = selectedEwmaAlphaChange.currentValue;
    }
    if (hardcodedViewChange){
        this.hardcodedView = hardcodedViewChange.currentValue;
    }
}

hasFields() {
    return this.fieldNames.length > 0;
}

hasTables() {
    return this.tableNames.length > 0;
}

canSendQuery() {
    return this.isNeonReady
        && this.processing === false
        && this.selectedDb
        && this.selectedTable;
}

addFilter() {
    this.filters.push({
        field: null,
        operator: '>',
        value: 0
    });
}

removeFilter(index) {
    this.filters.splice(index, 1)
    let splicedFilters = this.filters;

    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        filters: null
    });
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        filters: splicedFilters
    });

    console.log(this.editorConfig.getData())
}

sendQuery() {
    this.processing = true;
    this.clearErrorMessage();
    let neonQuery = new neon.query.Query();
    // select db and table
    neonQuery = neonQuery.selectFrom(this.selectedDb, this.selectedTable);

    //in case granularity was specified
    if (this.granularity && this.dateField){

        let groupByClause = [];

        let yearGroupClause = new neon.query.GroupByFunctionClause('year', this.dateField, 'year');
        let monthGroupClause = new neon.query.GroupByFunctionClause('month', this.dateField, 'month');
        let dayGroupClause = new neon.query.GroupByFunctionClause('dayOfMonth', this.dateField, 'day');
        let hourGroupClause = new neon.query.GroupByFunctionClause('hour', this.dateField, 'hour');

        if(this.granularity === 'hour') {
            groupByClause = [yearGroupClause, monthGroupClause, dayGroupClause, hourGroupClause];
        }else if (this.granularity === 'dayOfMonth') {
            groupByClause = [yearGroupClause, monthGroupClause, dayGroupClause];
        }else if (this.granularity === 'month') {
            groupByClause = [yearGroupClause, monthGroupClause];
        }else if (this.granularity === 'year') {
            let yearGroupClause = new neon.query.GroupByFunctionClause('year', this.dateField, 'year');
            groupByClause = [yearGroupClause];
        }

        // Group by category too
        if (this.selectedGroupByField){
            groupByClause.push(this.selectedGroupByField);
        }

        // Add the clauses
        neonQuery = neonQuery.groupBy(groupByClause);

        // Count all the fields
        neonQuery = neonQuery.aggregate('count', '*', 'count');
        neonQuery = neonQuery.aggregate('max', this.dateField, 'date').sortBy('date', 1);
    }
    // filters (where clauses)
    const whereClauses: neon.query.WhereClause[] = [];
    this.filters.forEach(filter => {
        if (filter.field && filter.operator) {
            // we have to manually set rhs as neon.query.WhereClause contructor only supports
            // strings
            const clause = new neon.query.WhereClause(
                filter.field,
                filter.operator,
                null
            );
            clause.rhs = this.trycast(filter.value);
            whereClauses.push(clause);
        }
    });
    if (this.selectedTimeFilterField){
        if (this.selectedTimeFilterFrom){
            const fromClause = new neon.query.WhereClause(this.selectedTimeFilterField, '>=', null);
            var date = new Date(this.selectedTimeFilterFrom);
            fromClause.rhs = this.trycast(date.toISOString());
            whereClauses.push(fromClause);
        }
        if (this.selectedTimeFilterTo){
            const toClause = new neon.query.WhereClause(this.selectedTimeFilterField, '<=', null);
            var date = new Date(this.selectedTimeFilterTo);
            toClause.rhs = this.trycast(date.toISOString());
            whereClauses.push(toClause);
        }
    }
    if (whereClauses.length > 0) {
        neonQuery = neonQuery.where(neon.query.and(...whereClauses));
    }
    // limit
    neonQuery.limit(this.selectedLimit);

    this.showLoadingIndicator('Querying...');
    this.neonDp.query(neonQuery).subscribe(data => {
        data.ewmaAlpha = this.ewmaAlpha;
        data.harcodedView = this.hardcodedView;
        this.processing = false;
        this.hideLoadingIndicator();
        this.mappingProvider.input().next(data);
        this.lastResponse = data;
    }, (error) => {
        this.processing = false;
        this.hideLoadingIndicator();
        this.displayErrorMessage(this.neonErrToStr(error));
        this.lastResponse = error;
    });
}

onDbSelected(dbName: string) {
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        dbName: dbName,
        tableName: null
    });
}

onTableSelected(tableName: string) {
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        tableName: tableName
    });
}

onFilterFieldSelected(field: string) {
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        filters: this.filters
    });
}

onFilterOperatorSelected(operator: string) {
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        filters: this.filters
    });
}

onFilterValueSelected(value: string) {
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        filters: this.filters
    });
}

onTimeFilterFieldSelected(field: string) {
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        timeFilter: {
            field: field
        }
    });
}

onTimeFilterFromSelected(from: Date) {
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        timeFilter: {
            from: from
        }
    });
}

onTimeFilterToSelected(to: Date) {
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        timeFilter: {
            to: to
        }
    });
}
onDateFieldSelected(dateField: string){
 this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
    dateField: dateField
 })
}

onGranularitySelected(granularity: string){
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        granularity: granularity
    });
}

onAggGroupByFieldSelected(selectedGroupByField: string){
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        selectedGroupByField: selectedGroupByField
    });
}

onEwmaAlphaSelected(ewmaAlpha: number){
    this.editorConfig.update<NeonTimeseriesDpEditorConfig>({
        ewmaAlpha: ewmaAlpha
    });
}

updateDatabaseNames() {
    this.dbNames = [];
    //this.filters = [];
    this.showLoadingIndicator();
    this.clearErrorMessage();
    this.neonDp.getDatabaseNames().subscribe(
        names => {
            this.hideLoadingIndicator();
            this.dbNames = names;
        },
        error => {
            this.hideLoadingIndicator();
            this.displayErrorMessage(this.neonErrToStr(error));
        }
    );
}

updateTableNames() {
    this.tableNames = [];
   //this.filters = [];
    this.showLoadingIndicator();
    this.clearErrorMessage();
    this.neonDp.getTableNames(this.selectedDb).subscribe(
        names => {
            this.hideLoadingIndicator();
            this.tableNames = names;
        },
        error => {
            this.hideLoadingIndicator();
            this.displayErrorMessage(this.neonErrToStr(error));
        }
    );
}

updateFieldNames() {
    this.fieldNames = [];
    //this.filters = [];
    this.showLoadingIndicator();
    this.clearErrorMessage();
    this.neonDp.getFields(this.selectedDb, this.selectedTable).subscribe(
        names => {
            this.hideLoadingIndicator();
            this.fieldNames = names;
        },
        error => {
            this.hideLoadingIndicator();
            this.displayErrorMessage(this.neonErrToStr(error));
        }
    );
}

/**
 * getFieldsTypes seems to throw error for mentat db, so we can securely
 * extract field types.
 */
private trycast(value: any): any {
    // ISO date format yyyy-mm-ddTHH:mm:ss.SSSZ (2018-04-18T00:00:00.000Z)
    const dateRegex = '[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}Z';
    if (value.match(dateRegex)){
        return value;
    }
    const number = parseFloat(value);
    if (isNaN(number)) {
        return value;
    } else {
        return number;
    }
}

private neonErrToStr(error: any) {
    // if not defined
    if (!error) {
        return 'UNKNOW ERROR — Error object is empty';
    }
    // if string already, then nothing to convert
    if (typeof error === 'string') {
        return error;
    }
    // state of 0 indicates that the connection was not initiated (e.g. TIMEOUT)
    if (error.readyState === 0) {
        return (
            'Failed to connect to: "' + this.neonDp.NEON_SERVER_URL + '"'
        );
    } else if (error.responseText) {
        return error.responseText;
    } else if (error.statusText) {
        return error.statusText;
    } else {
        return JSON.stringify(error, null, 2);
    }
}
}



