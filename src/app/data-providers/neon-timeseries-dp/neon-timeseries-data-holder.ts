import * as neon from 'neon-framework';

export class NeonTimeseriesDataHolder {
    constructor(public query: neon.query.Query, public data: any, public ewmaAlpha: any, public harcodedView: boolean) {

    }
}
