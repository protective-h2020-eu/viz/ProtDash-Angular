import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as neon from 'neon-framework';

import { ConfigChanges } from '../../../lib/config';
import { BaseEditorComponent } from '../../../view-providers/base-editor.component';
import { NeonDataProvider } from '../neon-dp.service';
import { NeonDpEditorConfig } from './neon-dp-editor-config.model';

@Component({
    selector: 'app-neon-dp-editor',
    templateUrl: './neon-dp-editor.component.html',
    styleUrls: ['./neon-dp-editor.component.css']
})
export class NeonDpEditorComponent extends BaseEditorComponent implements OnInit {

    public isNeonReady = false;
    public processing = false;
    public neonDp: NeonDataProvider;

    //
    //  UI Bindings
    //
    public selectedDb: string = null;
    public selectedTable: string = null;
    public selectedAggOperator: string = null;
    public selectedAggField: string = null;
    public selectedField: string = null;
    public selectedGroupByField: string = null;
    public selectedLimit = 1000;
    public filters: {
        field: string;
        operator: string;
        value: any;
    }[] = [];

    //
    //  For select population
    //
    public dbNames: string[] = [];
    public tableNames: string[] = [];
    public fieldNames: string[] = [];
    public selectedFieldList: string[] = [];

    //
    //  FOR TESTING PURPOSES
    //
    public lastResponse: any;
   
    constructor(private cdr: ChangeDetectorRef) {
        super();
    }

    ngOnInit() {
        // keep local reference so we don't have to cast it all the time
        this.neonDp = <NeonDataProvider>this.dataProvider;
        this.connectToNeon();
    }

    public connectToNeon() {
        this.showLoadingIndicator();
        this.clearErrorMessage();
        this.processing = true;
        this.neonDp.testDataProvider().subscribe(isReady => {
            this.hideLoadingIndicator();
            this.isNeonReady = isReady;
            this.processing = false;
            if (isReady) {
                this.init();
                if (this.canSendQuery()) {
                    this.sendQuery();
                }
            } else {
                this.displayErrorMessage('NEON is not ready yet');
            }
        }, (error) => {
            this.displayErrorMessage(this.neonErrToStr(error));
            this.hideLoadingIndicator();
            this.processing = false;
        });
    }

    private init() {
        this.updateDatabaseNames();
        // if already has database selected, then update table names
        if (this.selectedDb) {
            this.updateTableNames();
        }
        // if already has database and table selected, then populate fields
        if (this.selectedDb && this.selectedTable) {
            this.updateFieldNames();
        }
    }

    onEditorConfigChanges(changes: ConfigChanges) {
        const dbNameChange = changes['dbName'];
        const tableNameChange = changes['tableName'];
        const aggregationChange = changes['aggregation'];
        const selectedFieldChange = changes['field'];
        const filtersChange = changes['filters'];

        if (dbNameChange) {
            this.selectedDb = dbNameChange.currentValue;
            if (!dbNameChange.firstChange && dbNameChange.currentValue) {
                this.updateTableNames();
            }
        }
        if (tableNameChange) {
            this.selectedTable = tableNameChange.currentValue;
            if (!tableNameChange.firstChange && tableNameChange.currentValue) {
                this.updateFieldNames();
            }
        }
        if (aggregationChange) {
            const aggregation = aggregationChange.currentValue;
            if (aggregation) {
                // still accept null though
                if (aggregation.field !== undefined) {
                    this.selectedAggField = aggregation.field;
                }
                if (aggregation.operator !== undefined) {
                    this.selectedAggOperator = aggregation.operator;
                }
                if (aggregation.groupByField !== undefined) {
                    this.selectedGroupByField = aggregation.groupByField;
                }
            }
        }
        if (filtersChange){
            const filters = filtersChange.currentValue;
            if (filters && filtersChange.firstChange) {
                this.updateFilters(filters);           
            }
        }
        if (selectedFieldChange) {
            const field = selectedFieldChange.currentValue;
            this.selectedField = field ? field : null;
        }      
    }

    hasFields() {
        return this.fieldNames.length > 0;
    }
    hasFieldsInList() {
        return this.selectedFieldList.length > 0;
    }

    hasTables() {
        return this.tableNames.length > 0;
    }

    canSendQuery() {
        return this.isNeonReady
            && this.processing === false
            && this.selectedDb
            && this.selectedTable;
    }

    addFilter() {
        this.filters.push({
            field: null,
            operator: '>',
            value: 0
        });
    }

    removeFilter(index) {
        this.filters.splice(index, 1);
    }

    /*
    * Adds field to selection for query
    * If field already present boolean 'duplicate' set to false
    */
    addFieldToList(element: string) {
        this.selectedFieldList.push(element);
    }

    removeFieldFromList(element: string) {
        const indexToRemove = this.selectedFieldList.indexOf(element);
        this.selectedFieldList.splice(indexToRemove, 1);
    }

    onFieldSelected(event: boolean, field: string) {
        if (event) {
            this.addFieldToList(field);
        } else {
            this.removeFieldFromList(field);
        }
        this.cdr.detectChanges();
    }

    aggregationInProgress() {
        return (this.selectedAggField || this.selectedAggOperator || this.selectedGroupByField);
    }

    sendQuery() {
        this.processing = true;
        this.clearErrorMessage();
        // console.log('this.selectedDb', this.selectedDb);
        // console.log('this.selectedTable', this.selectedTable);
        // console.log('this.selectedAggOperator', this.selectedAggOperator);
        // console.log('this.selectedAggField', this.selectedAggField);
        // console.log('this.selectedGroupByField', this.selectedGroupByField);
        // console.log('this.filters', this.filters);
        let neonQuery = new neon.query.Query();
        // select db and table
        neonQuery = neonQuery.selectFrom(this.selectedDb, this.selectedTable);

        if (this.selectedFieldList.length > 0) {
            neonQuery = neonQuery
                .selectFrom(this.selectedDb, this.selectedTable)
                .withFields(this.selectedFieldList);
        }
        // in case aggregation was specified
        if (this.selectedAggOperator && this.selectedAggField) {
            neonQuery = neonQuery.aggregate(
                this.selectedAggOperator,
                this.selectedAggField
            );
        }
        // in case group by was specified
        if (this.selectedGroupByField) {
            neonQuery = neonQuery.groupBy(this.selectedGroupByField);
        }
        // filters (where clauses)
        const whereClauses: neon.query.WhereClause[] = [];
        this.filters.forEach(filter => {
            if (filter.field && filter.operator) {
                // we have to manually set rhs as neon.query.WhereClause contructor only supports
                // strings
                const clause = new neon.query.WhereClause(
                    filter.field,
                    filter.operator,
                    null
                );
                clause.rhs = this.trycast(filter.value);
                whereClauses.push(clause);
            }
        });
        if (whereClauses.length > 0) {
            neonQuery = neonQuery.where(neon.query.and(...whereClauses));
        }
        // limit
        neonQuery.limit(100);

        this.showLoadingIndicator('Querying...');
        this.neonDp.query(neonQuery).subscribe(data => {
            this.processing = false;
            this.hideLoadingIndicator();
            this.mappingProvider.input().next(data);
            this.lastResponse = data;
        }, (error) => {
            this.processing = false;
            this.hideLoadingIndicator();
            this.displayErrorMessage(this.neonErrToStr(error));
            this.lastResponse = error;
        });
    }

    onDbSelected(dbName: string) {
        this.editorConfig.update<NeonDpEditorConfig>({
            dbName: dbName,
            tableName: null
        });
    }

    onTableSelected(tableName: string) {
        this.editorConfig.update<NeonDpEditorConfig>({
            tableName: tableName
        });
    }

    onAggFieldSelected(field: string) {
        this.editorConfig.update<NeonDpEditorConfig>({
            aggregation: {
                field: field
            }
        });
    }

    onAggOperatorSelected(operator: string) {
        this.editorConfig.update<NeonDpEditorConfig>({
            aggregation: {
                operator: operator
            }
        });
    }

    onAggGroupByFieldSelected(field: any) {
        this.editorConfig.update<NeonDpEditorConfig>({
            aggregation: {
                groupByField: field
            }
        });
    }

    onFilterFieldSelected(field: string) {
        this.editorConfig.update<NeonDpEditorConfig>({
            filters: [{
                field: field
            }]
        });
    }

    updateDatabaseNames() {
        this.dbNames = [];
        //this.filters = [];
        this.showLoadingIndicator();
        this.clearErrorMessage();
        this.neonDp.getDatabaseNames().subscribe(
            names => {
                this.hideLoadingIndicator();
                this.dbNames = names;
            },
            error => {
                this.hideLoadingIndicator();
                this.displayErrorMessage(this.neonErrToStr(error));
            }
        );
    }

    updateTableNames() {
        this.tableNames = [];
       //this.filters = [];
        this.showLoadingIndicator();
        this.clearErrorMessage();
        this.neonDp.getTableNames(this.selectedDb).subscribe(
            names => {
                this.hideLoadingIndicator();
                this.tableNames = names;
            },
            error => {
                this.hideLoadingIndicator();
                this.displayErrorMessage(this.neonErrToStr(error));
            }
        );
    }

    updateFieldNames() {
        this.fieldNames = [];
        //this.filters = [];
        this.showLoadingIndicator();
        this.clearErrorMessage();
        this.neonDp.getFields(this.selectedDb, this.selectedTable).subscribe(
            names => {
                this.hideLoadingIndicator();
                this.fieldNames = names;
            },
            error => {
                this.hideLoadingIndicator();
                this.displayErrorMessage(this.neonErrToStr(error));
            }
        );
    }

    updateFilters(filters: Array<any>){
        filters.forEach(filter => {
            if (filter.field !== undefined && filter.operator !== undefined && filter.value !== undefined) {
                this.filters.push({
                    field: filter.field,
                    operator: filter.operator,
                    value: filter.value
                });
            }
        }); 
    }
    /**
     * getFieldsTypes seems to throw error for mentat db, so we can securely
     * extract field types.
     */
    private trycast(value: any): any {
        const number = parseFloat(value);
        if (isNaN(number)) {
            return value;
        } else {
            return number;
        }
    }

    private neonErrToStr(error: any) {
        // if not defined
        if (!error) {
            return 'UNKNOW ERROR — Error object is empty';
        }
        // if string already, then nothing to convert
        if (typeof error === 'string') {
            return error;
        }
        // state of 0 indicates that the connection was not initiated (e.g. TIMEOUT)
        if (error.readyState === 0) {
            return (
                'Failed to connect to: "' + this.neonDp.NEON_SERVER_URL + '"'
            );
        } else if (error.responseText) {
            return error.responseText;
        } else if (error.statusText) {
            return error.statusText;
        } else {
            return JSON.stringify(error, null, 2);
        }
    }
}
