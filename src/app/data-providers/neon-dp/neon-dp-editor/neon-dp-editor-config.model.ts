export interface NeonDpEditorConfig {
    dbName?: string;
    tableName?: string;
    filters?: {
            field?: string;
            operator?: string;
            value?: any
        }[];
    aggregation?: {
        field?: string;
        operator?: string;
        groupByField?: any
    };
}
