import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NeonDataProvider } from './neon-dp.service';
import { NeonDpEditorComponent } from './neon-dp-editor/neon-dp-editor.component';
import { FormsModule } from '@angular/forms';
import { CollapseModule } from 'ngx-bootstrap/collapse';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        CollapseModule.forRoot()
    ],
    providers: [NeonDataProvider],
    declarations: [
        NeonDpEditorComponent
    ],
    entryComponents: [NeonDpEditorComponent]
})
export class NeonDpModule { }
