import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DemoDpModule } from './demo-dp/demo-provider.module';
import { NeonDpModule } from './neon-dp/neon-dp.module';
import { NeonTimeseriesDpModule } from './neon-timeseries-dp/neon-timeseries-dp.module';
import { SampleJsonDpModule } from './sample-json-dp/d3js-dp.module';

@NgModule({
    imports: [
        CommonModule,
        DemoDpModule,
        NeonDpModule,
        SampleJsonDpModule,
        NeonTimeseriesDpModule
    ],
    declarations: [],
    providers: []
})
export class DataProvidersModule {}
