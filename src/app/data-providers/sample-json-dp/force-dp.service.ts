import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

import { HttpClient } from '@angular/common/http';
import { Injectable, Type } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataProviderDeclaration } from '../../lib/data-provider-declaration';
import { DataProvider } from '../../lib/data-provider.model';
import { ForceDpEditorComponent } from './force-dp-editor/force-dp-editor.component';
import { JsonDataHolder } from './json-dp-holder';

@Injectable()
@DataProviderDeclaration({
    id: '@dp/sample-graph',
    name: 'Sample Graph Data',
    dataType: JsonDataHolder,
    editorView: ForceDpEditorComponent
})
export class ForceDpService implements DataProvider {

  constructor(private http: HttpClient) { }

    query(query: any): Observable<JsonDataHolder> {
        return this.http.get('/assets/miserables.json').
        map <any, JsonDataHolder>(data => {
            return new JsonDataHolder(data);
        });
    }
    testDataProvider(): Observable<boolean> {
        return Observable.of(true);
    }
    getDataType(): Type<JsonDataHolder> {
        return JsonDataHolder;
    }

}
