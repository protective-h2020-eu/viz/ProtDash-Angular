import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { BaseEditorComponent } from '../../../view-providers/base-editor.component';
import { HeatmapDpService } from '../heatmap-dp.service';

@Component({
  selector: 'app-heatmap-dp-editor',
  templateUrl: './heatmap-dp-editor.component.html',
  styleUrls: ['./heatmap-dp-editor.component.css']
})
export class HeatmapDpEditorComponent extends BaseEditorComponent implements OnInit, OnDestroy, OnChanges {

    private dataSub: Subscription;
    constructor() {
        super();
    }

    ngOnInit() {
        this.sendQuery();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['dataProvider'] && changes['dataProvider'].currentValue) {

        }
        if (changes['mappingProvider'] && changes['mappingProvider'].currentValue) {
        }
    }


    ngOnDestroy() {
        // important to call parent for clean-up purposes
        super.ngOnDestroy();
        if (this.dataSub) {
            this.dataSub.unsubscribe();
        }
    }

    sendQuery() {
        if (this.dataProvider.constructor.name === 'HeatmapDpService') {
            const demoDp = <HeatmapDpService>this.dataProvider;
            const dataType = this.dataProvider.getDataType();
            const supportsDataType = this.mappingProvider.supportsDataType(dataType);
            if (supportsDataType) {
                this.dataSub = this.dataProvider.query({}).subscribe(
                    data => {
                        this.hideLoadingIndicator();
                        this.mappingProvider.input().next(data);
                    },
                    error => {
                        this.hideLoadingIndicator();
                        this.displayErrorMessage(error.message);
                });
            }
        }
    }

}
