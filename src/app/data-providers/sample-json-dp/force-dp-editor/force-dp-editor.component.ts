import {
    Component,
    OnChanges,
    OnDestroy,
    OnInit,
    SimpleChanges
} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { BaseEditorComponent } from '../../../view-providers/base-editor.component';
import { ForceDpService } from '../force-dp.service';
import { ForceDpService2 } from '../force2-dp.service';
import { ForceCDGService } from '../forceCDG-dp.service';
import { CDGService } from './../../../components/CDG/CDG.service';

interface CDGs {
    name: string;
    checked: boolean;
}

@Component({
    selector: 'app-force-dp-editor',
    templateUrl: './force-dp-editor.component.html',
    styleUrls: ['./force-dp-editor.component.css']
})
export class ForceDpEditorComponent extends BaseEditorComponent
    implements OnInit, OnDestroy, OnChanges {
    public CDG_Table: string[] = [];
    public CDG_Info: CDGs[] = [];
    public currentCDG: string = null;
    private dataSub: Subscription;
    public name: string;
    constructor(private cdg: CDGService) {
        super();
    }

    ngOnInit() {
        // this.showLoadingIndicator();
        this.cdg.list().subscribe(data => {
            this.CDG_Table = data;

            for (const i in this.CDG_Table) {
                if (this.CDG_Table) {
                    this.CDG_Info.push({
                        name: this.CDG_Table[i],
                        checked: false
                    });
                }
            }
        });

        this.sendQuery();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['dataProvider'] && changes['dataProvider'].currentValue) {
        }
        if (
            changes['mappingProvider'] &&
            changes['mappingProvider'].currentValue
        ) {
        }
    }

    ngOnDestroy() {
        // important to call parent for clean-up purposes
        super.ngOnDestroy();
        if (this.dataSub) {
            this.dataSub.unsubscribe();
        }
    }
    sendQuery(name?: string) {
        if (this.dataProvider.constructor.name === 'ForceDpService') {
            this.name = 'ForceDpService';
            const demoDp = <ForceDpService>this.dataProvider;
            const dataType = this.dataProvider.getDataType();
            const supportsDataType = this.mappingProvider.supportsDataType(
                dataType
            );
            if (supportsDataType) {
                this.showLoadingIndicator('Querying...');
                this.dataSub = this.dataProvider.query({}).subscribe(
                    data => {
                        this.hideLoadingIndicator();
                        this.mappingProvider.input().next(data);
                    },
                    error => {
                        this.hideLoadingIndicator();
                        this.displayErrorMessage(error.message);
                    }
                );
            }
        } else if (this.dataProvider.constructor.name === 'ForceDpService2') {
            this.name = 'ForceDpService2';
            const demoDp = <ForceDpService2>this.dataProvider;
            const dataType = this.dataProvider.getDataType();
            const supportsDataType = this.mappingProvider.supportsDataType(
                dataType
            );
            if (supportsDataType) {
                this.showLoadingIndicator('Querying...');
                this.dataSub = this.dataProvider.query({}).subscribe(
                    data => {
                        this.hideLoadingIndicator();
                        this.mappingProvider.input().next(data);
                    },
                    error => {
                        this.hideLoadingIndicator();
                        this.displayErrorMessage(error.message);
                    }
                );
            }
        } else if (this.dataProvider.constructor.name === 'ForceCDGService') {
            this.name = 'ForceCDGService';
            const demoDp = <ForceCDGService>this.dataProvider;
            const dataType = this.dataProvider.getDataType();
            const supportsDataType = this.mappingProvider.supportsDataType(
                dataType
            );
            if (supportsDataType) {
                this.showLoadingIndicator('Querying...');
                this.dataSub = this.dataProvider.query(name).subscribe(
                    data => {
                        this.hideLoadingIndicator();
                        this.mappingProvider.input().next(data);
                    },
                    error => {
                        this.hideLoadingIndicator();
                        this.displayErrorMessage(error.message);
                    }
                );
            }
        }
    }
}
