import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

import { HttpClient } from '@angular/common/http';
import { Injectable, Type } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataProviderDeclaration } from '../../lib/data-provider-declaration';
import { DataProvider } from '../../lib/data-provider.model';
import { JsonDataHolder } from './json-dp-holder';
import { SankeyDpEditorComponent } from './sankey-dp-editor/sankey-dp-editor.component';

@Injectable()
@DataProviderDeclaration({
    id: '@dp/sankey-chart',
    name: 'Sankey Data',
    dataType: JsonDataHolder,
    editorView: SankeyDpEditorComponent
})
export class SankeyDpService implements DataProvider {
    constructor(private http: HttpClient) {}

    query(query: any): Observable<JsonDataHolder> {
        return this.http
            .get('/assets/energy.json')
            .map<any, JsonDataHolder>(data => {
                return new JsonDataHolder(data);
            });
    }
    testDataProvider(): Observable<boolean> {
        return Observable.of(true);
    }
    getDataType(): Type<JsonDataHolder> {
        return JsonDataHolder;
    }
}
