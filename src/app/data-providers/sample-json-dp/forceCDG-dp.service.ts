import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

import { HttpClient } from '@angular/common/http';
import { Injectable, Type } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';
import { DataProviderDeclaration } from '../../lib/data-provider-declaration';
import { DataProvider } from '../../lib/data-provider.model';
import { ForceDpEditorComponent } from './force-dp-editor/force-dp-editor.component';
import { JsonDataHolder } from './json-dp-holder';

@Injectable()
// To be commented back in later
// @DataProviderDeclaration({
//     id: '@dp/CDG-graph',
//     name: 'CDG Graph Data',
//     dataType: JsonDataHolder,
//     editorView: ForceDpEditorComponent
// })
export class ForceCDGService implements DataProvider {
    // private apiBaseURL = 'http://192.168.200.160:8080/api/get-graph-by-cdg/';
    public readonly CA_MAIR_URL = environment.ca_mair.url;
    // private apiBaseURL = 'http://localhost:8080/api/get-graph-by-cdg/';
    private apiBaseURL = `${this.CA_MAIR_URL}get-graph-by-cdg/`;
    // console.log('URL: ', this.apiBaseURL);

    constructor(private http: HttpClient) {}

    query(query: any): Observable<JsonDataHolder> {
        console.log('URL: ', this.apiBaseURL);
        if (query === undefined) {
            // Same as image
            query = encodeURIComponent('{{allEdgesCDG}}');
        }
        const apiCreateEndpoint = `${this.apiBaseURL}${query}`;
        return this.http
            .get(apiCreateEndpoint)
            .map<any, JsonDataHolder>(data => {
                return new JsonDataHolder(data);
            });
    }
    testDataProvider(): Observable<boolean> {
        return Observable.of(true);
    }
    getDataType(): Type<JsonDataHolder> {
        return JsonDataHolder;
    }
}
