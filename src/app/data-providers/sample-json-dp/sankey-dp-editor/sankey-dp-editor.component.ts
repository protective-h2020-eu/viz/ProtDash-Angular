import { SankeyDpService } from './../sankey-dp.service';
import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { BaseEditorComponent } from '../../../view-providers/base-editor.component';

@Component({
  selector: 'app-sankey-dp-editor',
  templateUrl: './sankey-dp-editor.component.html',
  styleUrls: ['./sankey-dp-editor.component.css']
})
export class SankeyDpEditorComponent extends BaseEditorComponent implements OnInit, OnDestroy, OnChanges {

    private dataSub: Subscription;
    constructor() {
        super();
    }

    ngOnInit() {
        this.sendQuery();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['dataProvider'] && changes['dataProvider'].currentValue) {

        }
        if (changes['mappingProvider'] && changes['mappingProvider'].currentValue) {
        }
    }


    ngOnDestroy() {
        // important to call parent for clean-up purposes
        super.ngOnDestroy();
        if (this.dataSub) {
            this.dataSub.unsubscribe();
        }
    }
    sendQuery() {
        if (this.dataProvider.constructor.name === 'SankeyDpService') {
            const demoDp = <SankeyDpService>this.dataProvider;
            const dataType = this.dataProvider.getDataType();
            const supportsDataType = this.mappingProvider.supportsDataType(dataType);
            if (supportsDataType) {
                this.showLoadingIndicator('Querying...');
                this.dataSub = this.dataProvider.query({}).subscribe(
                    data => {
                        this.hideLoadingIndicator();
                        this.mappingProvider.input().next(data);
                    },
                    error => {
                        this.hideLoadingIndicator();
                        this.displayErrorMessage(error.message);
                    });
            }
        }
    }

}
