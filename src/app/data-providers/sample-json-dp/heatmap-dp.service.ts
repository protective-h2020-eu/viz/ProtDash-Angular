import { HttpClient } from '@angular/common/http';
import { Injectable, Type } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataProviderDeclaration } from '../../lib/data-provider-declaration';
import { DataProvider } from './../../lib/data-provider.model';
import { HeatmapDpEditorComponent } from './heatmap-dp-editor/heatmap-dp-editor.component';
import { JsonDataHolder } from './json-dp-holder';

@Injectable()
@DataProviderDeclaration({
    id: '@dp/heatmap',
    name: 'Heatmap Data',
    dataType: JsonDataHolder,
    editorView: HeatmapDpEditorComponent
})
export class HeatmapDpService implements DataProvider {
    constructor(private http: HttpClient) {}

    query(query: any): Observable<JsonDataHolder> {
        return this.http
            .get('/assets/heatmap.json')
            .map<any, JsonDataHolder>(data => {
                return new JsonDataHolder(data);
            });
    }
    testDataProvider(): Observable<boolean> {
        return Observable.of(true);
    }
    getDataType(): Type<JsonDataHolder> {
        return JsonDataHolder;
    }
}
