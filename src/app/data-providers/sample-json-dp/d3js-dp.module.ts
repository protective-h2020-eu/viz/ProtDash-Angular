import { GraphDataService } from './../../view-providers/d3js-vp/force-directed-graph/graph-data.service';
import { ForceCDGService } from './forceCDG-dp.service';
import { FormsModule } from '@angular/forms';
import { SankeyDpService } from './sankey-dp.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForceDpEditorComponent } from './force-dp-editor/force-dp-editor.component';
import { ForceDpService } from './force-dp.service';
import { ForceDpService2 } from './force2-dp.service';
import { SankeyDpEditorComponent } from './sankey-dp-editor/sankey-dp-editor.component';
import { HeatmapDpEditorComponent } from './heatmap-dp-editor/heatmap-dp-editor.component';
import { HeatmapDpService } from './heatmap-dp.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  providers: [ForceDpService, ForceDpService2, SankeyDpService, HeatmapDpService, ForceCDGService, GraphDataService],
  declarations: [ForceDpEditorComponent, SankeyDpEditorComponent, HeatmapDpEditorComponent],
  entryComponents: [ForceDpEditorComponent, SankeyDpEditorComponent, HeatmapDpEditorComponent]
})
export class SampleJsonDpModule { }
