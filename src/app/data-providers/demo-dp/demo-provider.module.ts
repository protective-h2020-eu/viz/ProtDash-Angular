import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DemoDpEditorComponent } from './demo-dp-editor/demo-dp-editor.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { DemoDataProvider } from './demo-dp.service';
import { DemoTwoDataProvider } from './demo2-dp.service';
import { ProvidersRegistry } from '../../lib/providers-registry';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    declarations: [
        DemoDpEditorComponent
    ],
    providers: [DemoDataProvider, DemoTwoDataProvider],
    entryComponents: [DemoDpEditorComponent]
})
export class DemoDpModule {}
