import { Component, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { BaseEditorComponent } from '../../../view-providers/base-editor.component';
import { DemoDataProvider } from '../demo-dp.service';

@Component({
    selector: 'app-demo-dp-editor',
    templateUrl: './demo-dp-editor.component.html',
    styleUrls: ['./demo-dp-editor.component.css']
})
export class DemoDpEditorComponent extends BaseEditorComponent implements OnInit, OnDestroy, OnChanges {

    private dataSub: Subscription;

    constructor() {
        super();
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        // important to call parent for clean-up purposes
        super.ngOnDestroy();
        if (this.dataSub) {
            this.dataSub.unsubscribe();
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['dataProvider'] && changes['dataProvider'].currentValue) {

        }
        if (changes['mappingProvider'] && changes['mappingProvider'].currentValue) {
            this.sendQuery();
        }
    }

    sendQuery() {
        const demoDp = <DemoDataProvider>this.dataProvider;
        const dataType = this.dataProvider.getDataType();
        const supportsDataType = this.mappingProvider.supportsDataType(dataType);
        if (supportsDataType) {
            this.dataSub = this.dataProvider.query({}).subscribe(data => {
                this.mappingProvider.input().next(data);
            });
        }
    }
}
