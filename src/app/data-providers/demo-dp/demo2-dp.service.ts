import 'rxjs/add/observable/of';

import { HttpClient } from '@angular/common/http';
import { Injectable, Type } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataProviderDeclaration } from '../../lib/data-provider-declaration';
import { DataProvider } from '../../lib/data-provider.model';
import { BaseEditorComponent } from '../../view-providers/base-editor.component';
import { DemoDpEditorComponent } from './demo-dp-editor/demo-dp-editor.component';
import { DemoTwoDataHolder } from './demo-two-data.model';

@Injectable()
@DataProviderDeclaration({
    id: '@dp/demo-fixed',
    name: 'Demo Data Provider (Fixed Values)',
    dataType: DemoTwoDataHolder,
    editorView: DemoDpEditorComponent
})
export class DemoTwoDataProvider implements DataProvider {

    constructor(private http: HttpClient) {

    }

    query(query: any): Observable<DemoTwoDataHolder> {
        // emit data
        const demoTwoData = new DemoTwoDataHolder();
        demoTwoData.numbers = [10, 15, 30, 100, 30, 15, 10];
        return Observable.of(demoTwoData);
    }

    testDataProvider(): Observable<boolean> {
        return Observable.of(true);
    }

    getDataType(): Type<DemoTwoDataHolder> {
        return DemoTwoDataHolder;
    }
}
