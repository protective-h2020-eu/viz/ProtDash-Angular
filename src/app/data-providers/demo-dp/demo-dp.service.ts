import 'rxjs/add/observable/of';

import { HttpClient } from '@angular/common/http';
import { Injectable, Type } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { DataProviderDeclaration } from '../../lib/data-provider-declaration';
import { DataProvider } from '../../lib/data-provider.model';
import { BaseEditorComponent } from '../../view-providers/base-editor.component';
import { DemoDataHolder } from './demo-data.model';
import { DemoDpEditorComponent } from './demo-dp-editor/demo-dp-editor.component';

@Injectable()
@DataProviderDeclaration({
    id: '@dp/demo-random',
    name: 'Demo Data Provider (Random Values)',
    dataType: DemoDataHolder,
    editorView: DemoDpEditorComponent
})
export class DemoDataProvider implements DataProvider {

    private source = new Subject<number[]>();

    constructor(private http: HttpClient) {
    }

    query(query: any): Observable<DemoDataHolder> {
        // emit data
        const demoData = new DemoDataHolder();
        demoData.data = [100, 80, 60, 40, 30, 15, 10].map(v => v * (1 + Math.random()));
        return Observable.of(demoData);
    }

    testDataProvider(): Observable<boolean> {
        return Observable.of(true);
    }

    getDataType(): Type<DemoDataHolder> {
        return DemoDataHolder;
    }
}
