import { Component, OnInit, Input, Type, OnChanges, SimpleChanges, SimpleChange, OnDestroy } from '@angular/core';
import { DataProvider } from '../lib/data-provider.model';
import { ViewProvider } from '../lib/view-provider.model';
import { BaseEditorComponent } from './base-editor.component';
import { MappingProvider } from '../lib/mapping-provider';
import { ProvidersRegistry } from '../lib/providers-registry';
import { Config, ConfigChanges } from '../lib/config';
import { Subscription } from 'rxjs/Subscription';
import { Observer } from 'rxjs/Observer';
import { WidgetControls } from '../components/dashboard/dashboard-components/dashboard-widget/widget-controls.model';


export abstract class BaseViewProviderComponent implements ViewProvider, OnChanges, OnDestroy {

    private _dataProvider: DataProvider;
    private _mappingProvider: MappingProvider;
    private _viewMode: string;
    private _viewProviderConfig: Config;

    //
    //  Subscription(s)
    //
    private _viewProviderConfigSub: Subscription;
    private _mappingDataSub: Subscription;

    //
    //  Variables available to children
    //
    public isViewDataTypeSupported = false;

    //
    //  Errors / Warnings messages for widget
    //
    public widgetControls: WidgetControls;

    /**
     * Angular doesn't run change detection on dynamically created componets
     * therefore we invoke it manually whenver property is changed
     */
    get mappingProvider() {
        return this._mappingProvider;
    }

    set mappingProvider(value: MappingProvider) {
        const oldValue = this._mappingProvider;
        this._mappingProvider = value;
        this.ngOnChanges({
            'mappingProvider': new SimpleChange(oldValue, value, false)
        });
    }

    get dataProvider() {
        return this._dataProvider;
    }

    set dataProvider(value: DataProvider) {
        const oldValue = this._dataProvider;
        this._dataProvider = value;
        this.ngOnChanges({
            'dataProvider': new SimpleChange(oldValue, value, false)
        });
    }

    get viewMode() {
        return this._viewMode;
    }

    set viewMode(value: string) {
        const oldValue = this._viewMode;
        this._viewMode = value;
        this.ngOnChanges({
            'viewMode': new SimpleChange(oldValue, value, false)
        });
    }

    //
    //  Global View Provider Config
    //
    get viewProviderConfig() {
        return this._viewProviderConfig;
    }
    set viewProviderConfig(value: Config) {
        const oldValue = this._viewProviderConfig;
        this._viewProviderConfig = value;
        this.ngOnChanges({
            'viewProviderConfig': new SimpleChange(oldValue, value, false)
        });
        if (this._viewProviderConfigSub) {
            this._viewProviderConfigSub.unsubscribe();
        }
        this._viewProviderConfigSub = this._viewProviderConfig.changes().subscribe(changes => {
            this.onViewProviderConfigChanges(changes);
        });
    }

    constructor() { }

    /**
     * Returns data type for a view provider
     */
    abstract getDataType(): Type<any>;

    /**
     * Notifies component about new data available
     * @param data Data returned by mapping provider
     */
    abstract onData(data: any);

    /**
     * SHOULD clear view provider (e.g. in case of a change to data provider)
     */
    abstract clearView();

    ngOnChanges(changes: SimpleChanges) {
        const mappingChange = changes['mappingProvider'];
        const dataProviderChange = changes['dataProvider'];
        if (mappingChange && mappingChange.currentValue) {
            this.updateMappingProvider();
        }
        if (dataProviderChange) {
            this.clearView();
        }
    }

    ngOnDestroy() {
        if (this._viewProviderConfigSub) {
            this._viewProviderConfigSub.unsubscribe();
        }
    }

    onViewProviderConfigChanges(changes: ConfigChanges) {

    }

    private updateMappingProvider() {
        // if previous subscription exists then unsubscribe
        if (this._mappingDataSub) {
            this._mappingDataSub.unsubscribe();
        }
        const viewType = this.getDataType();
        this.isViewDataTypeSupported = this.mappingProvider.supportsViewType(viewType);
        // check if data mappers supports view's data type
        if (this.isViewDataTypeSupported) {
            // console.log('View type is supported');
            this._mappingDataSub = this.mappingProvider.output(this.getDataType())
                .subscribe(data => {
                    this.onData(data);
                });
        }
    }

    displayErrorMessage(errorMessage: string) {
        this.widgetControls.displayErrorMessage(errorMessage);
    }

    displayWarningMessage(warningMessage: string) {
        this.widgetControls.displayWarningMessage(warningMessage);
    }

    clearErrorMessage() {
        this.widgetControls.clearErrorMessage();
    }

    clearWarningMessage() {
        this.widgetControls.clearWarningMessage();
    }

    showLoadingIndicator(message?: string) {
        this.widgetControls.showLoadingIndicator(message);
    }

    hideLoadingIndicator() {
        this.widgetControls.hideLoadingIndicator();
    }
}
