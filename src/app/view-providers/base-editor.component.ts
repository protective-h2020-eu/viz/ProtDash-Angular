import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange, Output, EventEmitter, OnDestroy } from '@angular/core';
import { DataProvider } from '../lib/data-provider.model';
import { MappingProvider } from '../lib/mapping-provider';
import { ViewProvider } from '../lib/view-provider.model';
import { Config, ConfigChanges } from '../lib/config';
import { Subscription } from 'rxjs/Subscription';
import { Observer } from 'rxjs/Observer';
import { WidgetControls } from '../components/dashboard/dashboard-components/dashboard-widget/widget-controls.model';


export abstract class BaseEditorComponent implements OnInit, OnChanges, OnDestroy {

    private _viewProvider: ViewProvider;
    private _dataProvider: DataProvider;
    private _mappingProvider: MappingProvider;
    private _editorConfig: Config;
    private _dataProviderConfig: Config;
    private _viewProviderConfig: Config;

    //
    //  Subscriptions
    //
    private _viewProviderConfigSub: Subscription;
    private _dataProviderConfigSub: Subscription;
    private _editorConfigSub: Subscription;

    //
    //  Errors / Warnings messages for widget
    //
    public widgetControls: WidgetControls;

    /**
     * Angular doesn't run change detection on dynamically created componets
     * therefore we invoke it manually whenver property is changed
     */
    get viewProvider() {
        return this._viewProvider;
    }

    set viewProvider(value: ViewProvider) {
        const oldValue = this._viewProvider;
        this._viewProvider = value;
        this.ngOnChanges({
            'viewProvider': new SimpleChange(oldValue, value, false)
        });
    }

    //
    //  Data Provider
    //
    get dataProvider() {
        return this._dataProvider;
    }

    set dataProvider(value: DataProvider) {
        const oldValue = this._dataProvider;
        this._dataProvider = value;
        this.ngOnChanges({
            'dataProvider': new SimpleChange(oldValue, value, false)
        });
    }

    //
    //  Mapping Provider
    //
    get mappingProvider() {
        return this._mappingProvider;
    }

    set mappingProvider(value: MappingProvider) {
        const oldValue = this._mappingProvider;
        this._mappingProvider = value;
        this.ngOnChanges({
            'mappingProvider': new SimpleChange(oldValue, value, false)
        });
    }

    //
    //  Editor Config
    //
    get editorConfig() {
        return this._editorConfig;
    }
    set editorConfig(value: Config) {
        const oldValue = this._editorConfig;
        this._editorConfig = value;
        this.ngOnChanges({
            'editorConfig': new SimpleChange(oldValue, value, false)
        });
        // unsubscribe from old config
        if (this._editorConfigSub) {
            this._editorConfigSub.unsubscribe();
        }
        // subscribe to new changes
        this._editorConfigSub = this._editorConfig.changes().subscribe(changes => {
            this.onEditorConfigChanges(changes);
        });
    }

    //
    //  Global Data Provider Config
    //
    get dataProviderConfig() {
        return this._dataProviderConfig;
    }
    set dataProviderConfig(value: Config) {
        const oldValue = this._dataProviderConfig;
        this._dataProviderConfig = value;
        this.ngOnChanges({
            'dataProviderConfig': new SimpleChange(oldValue, value, false)
        });
        if (this._dataProviderConfigSub) {
            this._dataProviderConfigSub.unsubscribe();
        }
        this._dataProviderConfigSub = this._dataProviderConfig.changes().subscribe(changes => {
            this.onDataProviderConfigChanges(changes);
        });
    }

    //
    //  Global View Provider Config
    //
    get viewProviderConfig() {
        return this._viewProviderConfig;
    }
    set viewProviderConfig(value: Config) {
        const oldValue = this._viewProviderConfig;
        this._viewProviderConfig = value;
        this.ngOnChanges({
            'viewProviderConfig': new SimpleChange(oldValue, value, false)
        });
        if (this._viewProviderConfigSub) {
            this._viewProviderConfigSub.unsubscribe();
        }
        this._viewProviderConfigSub = this._viewProviderConfig.changes().subscribe(changes => {
            this.onViewProviderConfigChanges(changes);
        });
    }

    constructor() { }

    onEditorConfigChanges(configChanges: ConfigChanges) {

    }

    onViewProviderConfigChanges(changes: ConfigChanges) {

    }

    onDataProviderConfigChanges(changes: ConfigChanges) {

    }

    ngOnInit() {

    }

    ngOnChanges(changes: SimpleChanges): void {
        // do nothing
    }

    ngOnDestroy() {
        if (this._viewProviderConfigSub) {
            this._viewProviderConfigSub.unsubscribe();
        }
        if (this._dataProviderConfigSub) {
            this._dataProviderConfigSub.unsubscribe();
        }
        if (this._editorConfigSub) {
            this._editorConfigSub.unsubscribe();
        }
    }

    displayErrorMessage(errorMessage: string) {
        this.widgetControls.displayErrorMessage(errorMessage);
    }

    displayWarningMessage(warningMessage: string) {
        this.widgetControls.displayWarningMessage(warningMessage);
    }

    clearErrorMessage() {
        this.widgetControls.clearErrorMessage();
    }

    clearWarningMessage() {
        this.widgetControls.clearWarningMessage();
    }

    showLoadingIndicator(message?: string) {
        this.widgetControls.showLoadingIndicator(message);
    }

    hideLoadingIndicator() {
        this.widgetControls.hideLoadingIndicator();
    }
}
