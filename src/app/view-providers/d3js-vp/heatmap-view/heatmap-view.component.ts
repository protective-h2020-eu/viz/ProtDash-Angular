import { AfterViewInit, Component, ElementRef, NgZone, OnInit, Renderer2, Type, ViewChild } from '@angular/core';
import * as colorbrewer from 'colorbrewer';
import * as d3 from 'd3';

import { ConfigChanges } from '../../../lib/config';
import { ViewProviderDeclaration } from '../../../lib/view-provider-declaration';
import { BaseViewProviderComponent } from './../../base-view-provider.component';
import { HeatmapVPData } from './heatmap-vp-data.model';

@Component({
    selector: 'app-heatmap-view',
    templateUrl: './heatmap-view.component.html',
    styleUrls: ['./heatmap-view.component.css']
})
@ViewProviderDeclaration({
    id: '@vp/heatmap-view',
    name: 'Heatmap View Provider',
    dataType: HeatmapVPData
})
export class HeatmapViewComponent extends BaseViewProviderComponent
    implements OnInit, AfterViewInit {
    private svgRef: HTMLCanvasElement;
    private height: number;
    private width: number;
    private firstTime = true;
    private heatmapData: any;
    public dpSet = true;
    private margin: {
        top: number,
        right: number,
        bottom: number,
        left: number
    };
    // private sankey: any;
    // private energy: any;
    private render: any = function() {};

    @ViewChild('svg')
    set svg(elementRef: ElementRef) {
        if (elementRef) {
            this.svgRef = elementRef.nativeElement;
        }
    }

    constructor(private zone: NgZone, private renderer: Renderer2) {
        super();
    }

    ngOnInit() {}
    ngAfterViewInit() {
        this.zone.runOutsideAngular(() => {
            this.initializeD3();
        });
    }
    private initializeD3() {
        if (!this.heatmapData || !this.svgRef) {
            return;
        }
        // const margin = { top: 50, right: 0, bottom: 100, left: 30 };
        if (this.firstTime) {
            const margin = { top: 50, right: 0, bottom: 100, left: 30 };
            this.margin = margin;
            this.width = 960 - margin.left - margin.right;
            this.height = 430 - margin.top - margin.bottom;
            this.firstTime = false;
        } else {
            const margin = {
                top: this.height * (1 / 16),
                right: this.width * (1 / 5),
                bottom: 0,
                left: this.width * (1 / 16)
            };
            this.margin = margin;
            this.width = this.width - margin.left - margin.right;
            this.height = this.height - margin.top - margin.bottom;
        }
        const gridSize = Math.floor(this.width / 24),
            legendElementWidth = gridSize * 2,
            buckets = 9,
            //   colors = ['#ffffd9', '#edf8b1', '#c7e9b4', '#7fcdbb', '#41b6c4',
            //   '#1d91c0', '#225ea8', '#253494', '#081d58'], // alternatively colorbrewer.YlGnBu[9]
            colors = colorbrewer.YlGnBu[9],
            days = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            times = [
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
                '10',
                '11',
                '12',
                '13',
                '14',
                '15',
                '16',
                '17',
                '18',
                '19',
                '20',
                '21',
                '22',
                '23',
                '24'
            ];
        // dataset = '/assets/data.tsv';

        const svg = d3
            .select(this.svgRef)
            .attr('width', this.width + this.margin.left + this.margin.right)
            .attr('height', this.height + this.margin.top + this.margin.bottom)
            .append('g')
            .attr(
                'transform',
                'translate(' + this.margin.left + ',' + this.margin.top + ')'
            );

        const dayLabels = svg
            .selectAll('.dayLabel')
            .data(days)
            .enter()
            .append('text')
            .text(function(d) {
                return d;
            })
            .attr('x', 0)
            .attr('y', function(d, i) {
                return i * gridSize;
            })
            .style('text-anchor', 'end')
            .attr('transform', 'translate(-6,' + gridSize / 1.5 + ')')
            .attr('class', function(d, i) {
                return i >= 0 && i <= 4
                    ? 'dayLabel mono axis axis-workweek'
                    : 'dayLabel mono axis';
            });

        const timeLabels = svg
            .selectAll('.timeLabel')
            .data(times)
            .enter()
            .append('text')
            .text(function(d) {
                return d;
            })
            .attr('x', function(d, i) {
                return i * gridSize;
            })
            .attr('y', 0)
            .style('text-anchor', 'middle')
            .attr('transform', 'translate(' + gridSize / 2 + ', -6)')
            .attr('class', function(d, i) {
                return i >= 7 && i <= 16
                    ? 'timeLabel mono axis axis-worktime'
                    : 'timeLabel mono axis';
            });

        const heatmapChart = () => {
            const data = this.heatmapData;
            // Likely a problem here with the quantile function
            const colorScale = d3
                .scaleQuantile()
                .domain([
                    0,
                    buckets - 1,
                    d3.max(data, function(d) {
                        return d.value;
                    })
                ])
                .range(colors);
            const cards = svg.selectAll('.hour').data(data, function(d) {
                return d.day + ':' + d.hour;
            });
            cards.append('title');
            cards
                .enter()
                .append('rect')
                .attr('x', function(d) {
                    return (d.hour - 1) * gridSize;
                })
                .attr('y', function(d) {
                    return (d.day - 1) * gridSize;
                })
                .attr('rx', 4)
                .attr('ry', 4)
                .attr('class', 'hour bordered')
                .attr('width', gridSize)
                .attr('height', gridSize)
                .style('fill', colors[0])
                .transition()
                .duration(1000)
                .style('fill', function(d) {
                    return colorScale(d.value);
                });
            cards.select('title').text(function(d) {
                return d.value;
            });
            cards.exit().remove();

            const legend = svg
                .selectAll('.legend')
                .data([0].concat(colorScale.quantiles()), function(d) {
                    return d;
                });

            legend
                .enter()
                .append('g')
                .attr('class', 'legend');

            legend
                .append('rect')
                .attr('x', function(d, i) {
                    return legendElementWidth * i;
                })
                .attr('y', this.height)
                .attr('width', legendElementWidth)
                .attr('height', gridSize / 2)
                .style('fill', function(d, i) {
                    return colors[i];
                });

            legend
                .append('text')
                .attr('class', 'mono')
                .text(function(d) {
                    return '≥ ' + Math.round(d);
                })
                .attr('x', function(d, i) {
                    return legendElementWidth * i;
                })
                .attr('y', this.height + gridSize);

            legend.exit().remove();
        };
        heatmapChart();

        // const datasetpicker = d3.select('#dataset-picker').selectAll('.dataset-button')
        //     .data(datasets);

        // datasetpicker.enter()
        //     .append('input')
        //     .attr('value', function(d: any, i: number) { return 'Dataset ' + (i + 1); })
        //     .attr('type', 'button')
        //     .attr('class', 'dataset-button')
        //     .on('click', function(d, i) {
        //     heatmapChart(datasets[i]);
        // });
    }
    private updateD3Projection() {
        const svg = d3.select(this.svgRef);
        // remove all current content of the svg
        // svg.remove();
        svg.selectAll('*').remove();
        // Draw the svg again with the new width and height
        this.initializeD3();
    }

    resizeSvg(event: any) {
        // Get the new width and height from the click event
        this.width = event.width;
        this.height = event.height;
        this.firstTime = false;

        // run any d3 updates outside angular zone
        if (this.height > 0 && this.width > 0) {
            this.zone.runOutsideAngular(() => {
                this.updateD3Projection();
            });
        }
    }

    onViewProviderConfigChanges(changes: ConfigChanges) {}

    getDataType(): Type<any> {
        return HeatmapVPData;
    }

    onData(data: any) {
        this.clearView();
        this.firstTime = true;
        this.heatmapData = data.data;
        this.zone.runOutsideAngular(() => {
            this.updateD3Projection();
        });
    }

    clearView() {
        const svg = d3.select(this.svgRef);
        // remove all current content of the svg
        svg.selectAll('*').remove();
    }
}
