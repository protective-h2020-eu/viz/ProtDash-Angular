export class HeatmapVPData {
    data: {
        day: number;
        hour: number;
        value: number;
    }[];

    constructor(data) {
        this.data = data;
    }
}
