import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CommonDirectivesModule } from './../../directives/common-directives.module';
// tslint:disable-next-line:max-line-length
import { ForceDirectedGraphVpEditorComponent } from './force-directed-graph/force-directed-graph-vp-editor/force-directed-graph-vp-editor.component';
import { ForceDirectedGraphComponent } from './force-directed-graph/force-directed-graph.component';

@NgModule({
    imports: [CommonModule, CommonDirectivesModule],
    declarations: [
        ForceDirectedGraphComponent,
        ForceDirectedGraphVpEditorComponent
    ],
    entryComponents: [
        ForceDirectedGraphComponent,
        ForceDirectedGraphVpEditorComponent
    ]
})
export class ForceGraphViewsModule {}
