export class SankeyVPData {

        nodes: {
            name: string;
        } [];
        links: {
            source: number;
            target: number;
            value: number;
        } [];

    constructor(nodes, links) {
        this.nodes = nodes;
        this.links = links;
    }
}
