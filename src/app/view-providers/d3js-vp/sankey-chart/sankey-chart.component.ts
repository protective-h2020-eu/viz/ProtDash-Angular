import {
    AfterViewInit,
    Component,
    ElementRef,
    NgZone,
    OnInit,
    Renderer2,
    Type,
    ViewChild
} from '@angular/core';
import * as d3 from 'd3';
import * as d3Sankey from 'd3-sankey';

import { ConfigChanges } from '../../../lib/config';
import { ViewProviderDeclaration } from '../../../lib/view-provider-declaration';
import { BaseViewProviderComponent } from '../../base-view-provider.component';
import { SankeyVPData } from './sankey-vp-data.model';

@Component({
    selector: 'app-sankey-chart',
    templateUrl: './sankey-chart.component.html',
    styleUrls: ['./sankey-chart.component.css']
})
@ViewProviderDeclaration({
    id: '@vp/sankey-view',
    name: 'Sankey Chart View Provider',
    dataType: SankeyVPData
})
export class SankeyChartComponent extends BaseViewProviderComponent
    implements OnInit, AfterViewInit {
    private svgRef: HTMLCanvasElement;
    private height: number;
    private width: number;
    private sankey: any;
    // private energy: any;
    private sankeyData: any;
    public dpSet = true;
    private render: any = function() {};

    @ViewChild('svg')
    set svg(elementRef: ElementRef) {
        if (elementRef) {
            this.svgRef = elementRef.nativeElement;
        }
    }

    constructor(private zone: NgZone, private renderer: Renderer2) {
        super();
    }

    ngOnInit() {}

    ngAfterViewInit() {
        this.zone.runOutsideAngular(() => {
            this.initializeD3();
        });
    }

    private initializeD3() {
        if (!this.sankeyData || !this.svgRef) {
            return;
        }
        const svg = d3.select(this.svgRef);

        const formatNumber = d3.format(',.0f'),
            format = function(d: any) {
                return formatNumber(d) + ' TWh';
            },
            color = d3.scaleOrdinal(d3.schemeCategory10);

        this.sankey = d3Sankey
            .sankey()
            .nodeWidth(15)
            .nodePadding(10)
            .extent([[1, 1], [this.width - 1, this.height - 6]]);

        let link = svg
            .append('g')
            .attr('class', 'links')
            .attr('fill', 'none')
            .attr('stroke', '#000')
            .attr('stroke-opacity', 0.2)
            .selectAll('path');
        let node = svg
            .append('g')
            .attr('class', 'nodes')
            .attr('font-family', 'sans-serif')
            .attr('font-size', 10)
            .selectAll('g');

        const energy = this.sankeyData;
        this.sankey(energy);
        link = link
            .data(energy.links)
            .enter()
            .append('path')
            .attr('d', d3Sankey.sankeyLinkHorizontal())
            .attr('stroke-width', function(d: any) {
                return Math.max(1, d.width);
            });
        link.append('title').text(function(d: any) {
            return (
                d.source.name + ' → ' + d.target.name + '\n' + format(d.value)
            );
        });
        node = node
            .data(energy.nodes)
            .enter()
            .append('g');
        node
            .append('rect')
            .attr('x', function(d: any) {
                return d.x0;
            })
            .attr('y', function(d: any) {
                return d.y0;
            })
            .attr('height', function(d: any) {
                return Math.abs(d.y1 - d.y0);
            })
            .attr('width', function(d: any) {
                return Math.abs(d.x1 - d.x0);
            })
            .attr('fill', function(d: any) {
                return color(d.name.replace(/ .*/, ''));
            })
            .attr('stroke', '#000');
        node
            .append('text')
            .attr('x', function(d: any) {
                return d.x0 - 6;
            })
            .attr('y', function(d: any) {
                return (d.y1 + d.y0) / 2;
            })
            .attr('dy', '0.35em')
            .attr('text-anchor', 'end')
            .text(function(d: any) {
                return d.name;
            })
            .filter(function(d: any) {
                return d.x0 < this.width / 2;
            })
            .attr('x', function(d: any) {
                return d.x1 + 6;
            })
            .attr('text-anchor', 'start');
        node.append('title').text(function(d: any) {
            return d.name + '\n' + format(d.value);
        });
    }

    private updateD3Projection() {
        const svg = d3.select(this.svgRef);
        // remove all current content of the svg
        svg.selectAll('*').remove();
        // Draw the svg again with the new width and height
        this.initializeD3();
    }

    resizeSvg(event: any) {
        // Get the new width and height from the click event
        this.width = event.width;
        this.height = event.height;

        // run any d3 updates outside angular zone
        if (this.height > 0 && this.width > 0) {
            this.zone.runOutsideAngular(() => {
                this.updateD3Projection();
            });
        }
    }

    getDataType(): Type<any> {
        return SankeyVPData;
    }
    onViewProviderConfigChanges(changes: ConfigChanges) {}

    onData(data: any) {
        this.clearView();
        this.sankeyData = data;
        this.zone.runOutsideAngular(() => {
            this.initializeD3();
        });
    }

    clearView() {
        const svg = d3.select(this.svgRef);
        // remove all current content of the svg
        svg.selectAll('*').remove();
    }
}
