import {
    AfterViewInit,
    Component,
    ElementRef,
    NgZone,
    OnChanges,
    OnInit,
    Renderer2,
    SimpleChange,
    SimpleChanges,
    Type,
    ViewChild
} from '@angular/core';
import * as colorbrewer from 'colorbrewer';
import * as d3 from 'd3';

import { ConfigChanges } from '../../../lib/config';
import { ViewProviderDeclaration } from '../../../lib/view-provider-declaration';
import { BaseViewProviderComponent } from '../../base-view-provider.component';
import { ForceDirectedGraphVpEditorComponent } from './force-directed-graph-vp-editor/force-directed-graph-vp-editor.component';
import { ForceVPData } from './force-vp-data.model';
import { GraphDataService } from './graph-data.service';

// import * as versor from './versor.js';

@Component({
    selector: 'app-graph-view',
    templateUrl: './force-directed-graph.component.html',
    styleUrls: ['./force-directed-graph.component.css']
})
@ViewProviderDeclaration({
    id: '@vp/graph-view',
    name: 'Force Directed Graph View Provider',
    dataType: ForceVPData,
    editorView: ForceDirectedGraphVpEditorComponent
})
export class ForceDirectedGraphComponent extends BaseViewProviderComponent
    implements OnInit, AfterViewInit, OnChanges {
    private svgRef: HTMLCanvasElement;
    private height: number;
    private width: number;
    private simulation: any;
    private forceData: any;
    private _viewLabels: boolean;
    public dpSet = true;
    private render: any = function() {};

    @ViewChild('svg')
    set svg(elementRef: ElementRef) {
        if (elementRef) {
            this.svgRef = elementRef.nativeElement;

            this.zone.runOutsideAngular(() => {
                this.initializeD3();
            });
        }
    }

    constructor(
        private zone: NgZone,
        private renderer: Renderer2,
        private el: ElementRef,
        private graphDataService: GraphDataService
    ) {
        super();
    }

    ngOnInit() {}
    get viewLabels() {
        return this._viewLabels;
    }
    set viewLabels(value: boolean) {
        const oldValue = this._viewLabels;
        this._viewLabels = value;
        this.ngOnChanges({
            viewLabels: new SimpleChange(oldValue, value, false)
        });
    }
    ngOnChanges(changes: SimpleChanges) {
        // important to call parent function as
        // it handles mapping provider change for us
        super.ngOnChanges(changes);
        const mappingChange = changes['mappingProvider'];
        const labelChange = changes['viewLabels'];
        // if (mappingChange && mappingChange.currentValue) {
        //     this.innerData = null;
        //     this.resetChartData();
        // }
        if (labelChange && labelChange.currentValue != null) {
            this.clearView();
            this.initializeD3();
        }
    }

    ngAfterViewInit() {
        this.zone.runOutsideAngular(() => {
            this.initializeD3();
        });
        // this.initializeD3();
    }

    initializeD3() {
        if (!this.forceData || !this.svgRef) {
            return;
        }
        if (!this.height || !this.width) {
            return;
        }
        const svg = d3.select(this.svgRef);

        // const svgNode = svg.node();

        const color = d3.scaleOrdinal().range(colorbrewer.Set1[8]);

        // const color = d3.scaleOrdinal(d3.schemeDark2);
        this.simulation = d3
            .forceSimulation()
            .force('link', d3.forceLink().id((d: any) => d.id))
            .force('charge', d3.forceManyBody())
            .force('center', d3.forceCenter(this.width / 2, this.height / 2));

        const graph = this.forceData;
        const link = svg
            .append('g')
            .attr('class', 'links')
            .selectAll('line')
            .data(graph.links)
            .enter()
            .append('line')
            .attr('stroke-width', (d: any) => Math.sqrt(d.value))
            .attr('stroke', 'grey');
        const node = svg
            .append('g')
            .attr('class', 'nodes')
            .selectAll('circle')
            .data(graph.nodes)
            .enter()
            .append('circle')
            .attr('r', 5)
            // .attr('fill', (d: any) => color(d.group < 9 ? d.group : d.group % 8) && console.log('Group is: ', d.group))
            .attr('fill', (d: any) => {
                // const col = d.group < 9 ? d.group : d.group % 8;
                // console.log('color is: ', col);
                // return color(d.group < 9 ? d.group : d.group % 8);
                // console.log('col is: ', col, ' Type of col is: ', typeof(col));
                return color(d.group);
            })
            .call(
                d3
                    .drag()
                    .on('start', this.dragstarted)
                    .on('drag', this.dragged)
                    .on('end', this.dragended)
            );

        // const node = svg.selectAll('.node')
        //     .data(graph.nodes)
        //     .enter().append('g')
        //     .attr('class', 'node')
        //     .call(
        //         d3
        //             .drag()
        //             .on('start', this.dragstarted)
        //             .on('drag', this.dragged)
        //             .on('end', this.dragended)
        //     );
        // node.append('image')
        //     .attr('xlink:href', '/assets/images/favicon.ico')
        //     .attr('x', -8)
        //     .attr('y', -8)
        //     .attr('width', 16)
        //     .attr('height', 16);
        // Labels now working
        let label = null;
        if (this._viewLabels === true) {
            label = svg
                .selectAll('.mytext')
                .data(graph.nodes)
                .enter()
                .append('text')
                .text(function(d) {
                    return d.id;
                })
                .style('text-anchor', 'middle')
                .style('fill', '#555')
                .style('font-family', 'Arial')
                .style('font-size', 12)
                .style('cursor', 'default')
                .style('user-select', 'none')
                .style('pointer-events', 'none');
        }
        // Test code for info pop-up
        let tip;
        svg.on('click', function() {
            if (tip) {
                tip.remove();
            }
        });
        node.on('click', d => {
            d3.event.stopPropagation();
            if (tip) {
                tip.remove();
            }

            tip = svg
                .append('g')
                .attr('transform', 'translate(' + d.x + ',' + d.y + ')');

            const rect = tip
                .append('rect')
                .style('fill', 'white')
                .style('stroke', 'steelblue');

            // console.log('d.id = ', d);
            // this.updateGraphEditor(d.id);
            this.updateGraphEditor(d);
            tip
                .append('text')
                .text('Name: ' + d.id)
                .attr('dy', '1em')
                .attr('x', 5);

            tip
                .append('text')
                .text('Group: ' + d.group)
                .attr('dy', '2em')
                .attr('x', 5);

            const con = graph.links
                .filter(function(d1) {
                    // console.log('Source: ', d1.source);
                    return d1.source.id === d.id;
                })
                .map(function(d1) {
                    // console.log('Target: ', d1.source);
                    return d1.target.id + ' with weight ' + d1.value;
                });

            tip
                .append('text')
                .text('Connected to: ' + con.join(','))
                .attr('dy', '3em')
                .attr('x', 5);

            const bbox = tip.node().getBBox();
            rect.attr('width', bbox.width + 20).attr('height', bbox.height + 5);
        });
        link.on('click', d => {
            d3.event.stopPropagation();
            if (tip) {
                tip.remove();
            }

            this.updateGraphEditor(d);
        });

        //   d3.selectAll('text').each(function() {
        //     d3.select(this).textwrap(this.previousElementSibling.getBBox());
        //   });
        /////////////////////////////////
        this.simulation.nodes(graph.nodes).on('tick', ticked);
        this.simulation.force('link').links(graph.links);
        function ticked() {
            link
                .attr('x1', function(d) {
                    return d.source.x || 0;
                })
                .attr('y1', function(d) {
                    return d.source.y || 0;
                })
                .attr('x2', function(d) {
                    return d.target.x || 0;
                })
                .attr('y2', function(d) {
                    return d.target.y || 0;
                });
            node
                .attr('cx', function(d) {
                    return d.x || 0;
                })
                .attr('cy', function(d) {
                    return d.y || 0;
                });
            // .attr('transform', function(d, i) {return 'translate(' + d.x + ',' + d.y + ')'; });
            if (label != null) {
                label
                    .attr('x', function(d) {
                        return d.x;
                    })
                    .attr('y', function(d) {
                        return d.y - 10;
                    });
            }
        }
    }
    private dragstarted = (d: any) => {
        if (!d3.event.active) {
            this.simulation.alphaTarget(0.3).restart();
        }
        d.fx = d.x;
        d.fy = d.y;
    }
    private dragged = (d: any) => {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }
    private dragended = (d: any) => {
        if (!d3.event.active) {
            this.simulation.alphaTarget(0);
        }
        d.fx = null;
        d.fy = null;
    }

    private updateD3Projection() {
        if (!this.forceData || !this.svgRef) {
            return;
        }
        const svg = d3.select(this.svgRef);

        // Change the center according to the current width and height
        this.simulation
            .force('center')
            .x(this.width / 2)
            .y(this.height / 2);

        // restart the simulation
        this.simulation.alpha(0.3).restart();
    }

    resizeSvg(event: any) {
        // Get the new width and height from the click event
        this.width = event.width;
        this.height = event.height;

        // run any d3 updates outside angular zone
        if (this.height > 0 && this.width > 0) {
            this.zone.runOutsideAngular(() => {
                this.updateD3Projection();
            });
        }
    }

    onViewProviderConfigChanges(changes: ConfigChanges) {}

    getDataType(): Type<any> {
        return ForceVPData;
    }

    onData(data: any) {
        this._viewLabels = true;
        this.clearView();
        this.forceData = data;
        this.zone.runOutsideAngular(() => {
            this.initializeD3();
        });
    }

    clearView() {
        const svg = d3.select(this.svgRef);
        // remove all current content of the svg
        svg.selectAll('*').remove();
    }
    updateGraphEditor(d: any) {
        this.graphDataService.updateData(d);
    }
}
