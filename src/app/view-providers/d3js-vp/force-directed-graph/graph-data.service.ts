import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';

@Injectable()
export class GraphDataService {
    private dataObs$ = new Subject();

    constructor() {}

    getData() {
        return this.dataObs$;
    }

    updateData(data: any) {
        this.dataObs$.next(data);
    }
}
