import { Component, OnDestroy, OnInit } from '@angular/core';

import { ConfigChanges } from '../../../../lib/config';
import { BaseEditorComponent } from '../../../base-editor.component';
import { ForceDirectedGraphComponent } from './../force-directed-graph.component';
import { GraphDataService } from './../graph-data.service';

//
//  Reperesentation of data within editor config
//
interface EditorConfigData {
    viewLabels?: boolean;
}

@Component({
    selector: 'app-force-directed-graph-vp-editor',
    templateUrl: './force-directed-graph-vp-editor.component.html',
    styleUrls: ['./force-directed-graph-vp-editor.component.css']
})
export class ForceDirectedGraphVpEditorComponent extends BaseEditorComponent
    implements OnInit, OnDestroy {
    public toggleLabels = true;
    public graphData: any = {};
    public keys = [];

    constructor(private graphDataService: GraphDataService) {
        super();
    }

    ngOnInit() {
        this.graphDataService.getData().subscribe(data => {
            this.graphData = data;
            this.keys = Object.keys(this.graphData);
            // console.log('The passed data is: ', this.graphData);
        });
    }

    onToggleLabels() {
        this.toggleLabels = this.toggleLabels ? false : true;
        this.editorConfig.update<EditorConfigData>({
            viewLabels: this.toggleLabels
        });
    }
    onEditorConfigChanges(changes: ConfigChanges) {
        const forceChange = changes['viewLabels'];

        if (forceChange && forceChange.currentValue != null) {
            const forceView = <ForceDirectedGraphComponent>this.viewProvider;
            forceView.viewLabels = forceChange.currentValue;
        }
    }

    ngOnDestroy() {
        super.ngOnDestroy();
    }
}
