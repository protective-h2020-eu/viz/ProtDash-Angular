export class ForceVPData {

        nodes: {
            id: string;
            group: number;
        } [];
        links: {
            source: string;
            target: string;
            value: number;
        } [];

    constructor(nodes, links) {
        this.nodes = nodes;
        this.links = links;
    }
}
