import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CommonDirectivesModule } from '../directives/common-directives.module';
import { BasicTableVpComponent } from './basic-table-vp/basic-table-vp.component';
import { ChartJsViewsModule } from './chartjs-vp/chartjs.module';
import { ForceGraphViewsModule } from './d3js-vp/force-directed-graph.module';
import { HeatmapViewComponent } from './d3js-vp/heatmap-view/heatmap-view.component';
import { SankeyChartComponent } from './d3js-vp/sankey-chart/sankey-chart.component';
import { PlotlyVpModule } from './plotly-vp/plotly-vp.module';

@NgModule({
    imports: [
        CommonModule,
        ChartJsViewsModule,
        ForceGraphViewsModule,
        CommonDirectivesModule,
        FormsModule,
        PlotlyVpModule
    ],
    declarations: [
        SankeyChartComponent,
        BasicTableVpComponent,
        HeatmapViewComponent
    ],
    entryComponents: [
        SankeyChartComponent,
        HeatmapViewComponent,
        BasicTableVpComponent
    ]
})
export class ViewProvidersModule {}
