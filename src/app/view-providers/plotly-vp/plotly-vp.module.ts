import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlotlyVpComponentComponent } from './plotly-vp-component/plotly-vp-component.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { AccordionModule } from 'ngx-bootstrap/accordion';

@NgModule({
  imports: [
    CommonModule,
    AlertModule.forRoot(),
    AccordionModule.forRoot()
  ],
  declarations: [
    PlotlyVpComponentComponent
  ],
  entryComponents: [PlotlyVpComponentComponent]
})
export class PlotlyVpModule { }
