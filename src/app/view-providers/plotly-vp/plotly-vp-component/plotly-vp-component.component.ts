import { 
  Component,
  OnInit,
  Type,
  TemplateRef,
  ElementRef,
  ViewChild
} from '@angular/core';
import { BaseViewProviderComponent } from '../../base-view-provider.component';
import { ViewProviderDeclaration } from '../../../lib/view-provider-declaration';
import { PlotlyVpDataHolder } from './plotly-vp-data-model';

import * as Plotly from 'plotly.js';

@Component({
  selector: 'app-plotly-vp-component',
  templateUrl: './plotly-vp-component.component.html',
  styleUrls: ['./plotly-vp-component.component.css']
})

@ViewProviderDeclaration({
  id: '@vp/plotly',
  name: 'Plotly Timeline',
  dataType: PlotlyVpDataHolder
})

export class PlotlyVpComponentComponent extends BaseViewProviderComponent implements OnInit {

  private innerData: PlotlyVpDataHolder;
  public dpSet = true;
  public myAlerts = [];
  private graphRef: ElementRef;

  constructor() { 
    super();
  }

  ngOnInit() {}

  public getDataType(): Type<PlotlyVpDataHolder> {
    return PlotlyVpDataHolder;
  } 

  @ViewChild('plotlyGraph') set plotlyGraph(elementRef: ElementRef) {
    if (elementRef) {
        this.graphRef = elementRef.nativeElement;
    }
  }

  onData(data: PlotlyVpDataHolder) {
    this.myAlerts = [];
    let graphData = [];
      data.groups.forEach(group => {

        let name = group.field;
        // Detect if we are in hardcodedView to modify the names to get the "partner"
        if (data.harcodedView){
          name = name.match('^[^.]*\.[^.]*')[0];
        }

        // Graph data
        graphData.push({
          type: "bar",
          name: name,
          x: group.dates,
          y: group.count
        });

        //Compute trends EWMA and add to graph
        if (data.ewmaAlpha){
          var myTrends = this.computeEWMA(group.count, data.ewmaAlpha, group.dates);
               
          graphData.push({
            mode: 'lines+markers',
            type: 'scatter',
            name: group.field + ' trend Line',
            x: group.dates,
            y: myTrends
          });
        }
        
      });

      var layout = {
        barmode: 'stack',
        autosize: true,
        xaxis: {
          type: 'date',
          rangeselector: {buttons: [
              {
                step: 'hour',
                stepmode: 'todate',
                count: 6,
                label: '6 hours'                
              },
              {
                step: 'month',
                stepmode: 'todate',
                count: 1,
                label: '1 Month'                
              },
              {
                step: 'month',
                stepmode: 'todate',
                count: 6,
                label: '6 Month'                
              },
              {
                step: 'year',
                stepmode: 'todate',
                count: 1,
                label: '1 Year'
              },
              {
                step: 'all'
              }
            ]},
          //rangeslider: {},
          //type: 'date'
        },
        yaxis: {
          autorange: true
        }
      };

      Plotly.newPlot(this.graphRef, graphData, layout);
  }

  clearView() {
    //this.resetChartData();
  }

  computeEWMA(data2ewma, alpha, dates){
    //console.log('Alpha selected for EWMA: ' + alpha);
    var myTrends = [];
    var oldValue:number;
    var newValue:number;
    var computedEWMA:number;
    

    for(var i = 0; i < data2ewma.length; i++) {         
        // Compute EWMA values
        newValue = data2ewma[i];
        oldValue = myTrends[i - 1];

        if (i == 0){
          computedEWMA = newValue;
        }else{
          computedEWMA = Math.round(alpha * newValue + (1.0 - alpha) * oldValue);
          //computedEWMA = Math.round(oldValue + alpha * (newValue - oldValue)); 
        }
        myTrends.push(computedEWMA);

        // Alerts when difference between real value and estimated is bigger than 3 times and the real value is at least 30
        if (newValue > 30 && newValue >= computedEWMA * 3){
          var alertMessage = "ALERT! unexpected increase at: "+ dates[i] +". Expected Value: " + computedEWMA + " Real value: " + newValue;
          //console.log(alertMessage);
          this.myAlerts.push(alertMessage);
        }

    }
    return myTrends;
  }

}

