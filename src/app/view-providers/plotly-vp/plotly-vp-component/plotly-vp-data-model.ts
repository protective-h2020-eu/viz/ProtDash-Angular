import { count } from "rxjs/operators";

export class PlotlyVpDataHolder{
    
    groups?: {
        field: string;
        dates?: string[];
        count?: string[];
    }[];
    ewmaAlpha?: number;
    harcodedView?: boolean;
    
    constructor() {
        this.groups = [];
    }
}
