import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartJsViewComponent } from './chartjs-view-component/chartjs-vp.component';
import { ChartjsViewEditorComponent } from './chartjs-view-component/chartjs-vp-editor/chartjs-vp-editor.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        ChartJsViewComponent,
        ChartjsViewEditorComponent
    ],
    entryComponents: [ChartJsViewComponent, ChartjsViewEditorComponent]
})
export class ChartJsViewsModule { }
