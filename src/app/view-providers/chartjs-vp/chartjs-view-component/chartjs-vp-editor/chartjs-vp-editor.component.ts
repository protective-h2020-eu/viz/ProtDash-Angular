import { Component, OnDestroy, OnInit } from '@angular/core';

import { ConfigChanges } from '../../../../lib/config';
import { BaseEditorComponent } from '../../../base-editor.component';
import { ChartJsViewComponent } from '../chartjs-vp.component';

//
//  Reperesentation of data within editor config
//
interface EditorConfigData {
    chartType?: string;
}

@Component({
    selector: 'app-chartjs-vp-editor',
    templateUrl: './chartjs-vp-editor.component.html',
    styleUrls: ['./chartjs-vp-editor.component.css']
})
export class ChartjsViewEditorComponent extends BaseEditorComponent implements OnInit, OnDestroy {

    constructor() {
        super();
    }

    ngOnInit() {
    }

    onChartTypeChange(event) {
        this.editorConfig.update<EditorConfigData>({
            chartType: event.target.value
        });
    }

    selectionAllowed(): boolean {
        return true;
    }

    onEditorConfigChanges(changes: ConfigChanges) {
        // console.log('editor', changes);
        const chartTypeChange = changes['chartType'];

        if (chartTypeChange && chartTypeChange.currentValue) {
            const chartJsView = <ChartJsViewComponent>this.viewProvider;
            chartJsView.chartType = chartTypeChange.currentValue;
        }
    }

    ngOnDestroy() {
        super.ngOnDestroy();
    }

}
