import 'rxjs/add/operator/take';

import {
    AfterViewInit,
    Component,
    ElementRef,
    NgZone,
    OnChanges,
    OnDestroy,
    OnInit,
    SimpleChange,
    SimpleChanges,
    Type,
    ViewChild,
} from '@angular/core';
import * as Chart from 'chart.js';
import { Subscription } from 'rxjs/Subscription';

import { ConfigChanges } from '../../../lib/config';
import { ViewProviderDeclaration } from '../../../lib/view-provider-declaration';
import { BaseViewProviderComponent } from '../../base-view-provider.component';
import { ChartJsVpData } from './chartjs-data.model';
import { ChartjsViewEditorComponent } from './chartjs-vp-editor/chartjs-vp-editor.component';

@Component({
    selector: 'app-chartjs-vp',
    templateUrl: './chartjs-vp.component.html',
    styleUrls: ['./chartjs-vp.component.css']
})
@ViewProviderDeclaration({
    id: '@vp/chartjs-multiple',
    name: 'Chart JS (Multiple)',
    editorView: ChartjsViewEditorComponent,
    dataType: ChartJsVpData,
    providerConfigDefaults: {
        backgroundColors: [
            'rgba(255, 99, 132, 0.5)',
            'rgba(54, 162, 235, 0.5)',
            'rgba(153, 102, 255, 0.5)',
            'rgba(255, 159, 64, 0.5)',
            'rgba(255, 205, 86, 0.5)',
            'rgba(75, 192, 192, 0.5)',
            'rgba(201, 203, 207, 0.5)'
        ],
        borderColors: [
            'rgb(255, 99, 132)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(201, 203, 207)'
        ]
    }
})
export class ChartJsViewComponent extends BaseViewProviderComponent implements OnInit,
    OnChanges,
    OnDestroy,
    AfterViewInit {

    private innerData: ChartJsVpData;
    private chart: Chart;

    public dpSet = true;

    private _chartType = 'bar';

    get chartType() {
        return this._chartType;
    }
    set chartType(value: string) {
        const oldValue = this._chartType;
        this._chartType = value;
        this.ngOnChanges({
            'chartType': new SimpleChange(oldValue, value, false)
        });
    }

    private canvasRef: ElementRef;
    public backgroundColors: any[];
    public borderColors: any[];

    @ViewChild('canvas') set canvas(elementRef: ElementRef) {
        if (elementRef) {
            this.canvasRef = elementRef;
            this.initChartJs();
        }
    }

    constructor(private zone: NgZone) {
        super();
    }

    ngOnInit() {

    }

    ngOnDestroy() {
        // important to call parent method
        // for clean up purposes
        super.ngOnDestroy();
        if (this.chart) {
            this.chart.destroy();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        // important to call parent function as
        // it handles mapping provider change for us
        super.ngOnChanges(changes);
        const mappingChange = changes['mappingProvider'];
        const chartTypeChange = changes['chartType'];
        if (mappingChange && mappingChange.currentValue) {
            this.innerData = null;
            this.resetChartData();
        }
        if (chartTypeChange && chartTypeChange.currentValue) {
            this.updateChartType();
        }
    }

    ngAfterViewInit() {
        this.updateChartData();
    }

    onViewProviderConfigChanges(changes: ConfigChanges) {
        console.log('view provider', changes);
        const backgroundColorsChange = changes['backgroundColors'];
        const borderColorsChange = changes['borderColors'];

        if (backgroundColorsChange && backgroundColorsChange.currentValue) {
            this.backgroundColors = backgroundColorsChange.currentValue;
            this.updateChartData(false);
        }

        if (borderColorsChange && borderColorsChange.currentValue) {
            this.borderColors = borderColorsChange.currentValue;
            this.updateChartData(false);
        }
    }

    public getDataType(): Type<ChartJsVpData> {
        return ChartJsVpData;
    }

    private initChartJs() {
        if (this.canvasRef && this.canvasRef.nativeElement) {
            const ctx = this.canvasRef.nativeElement.getContext('2d');
            this.zone.runOutsideAngular(() => {
                this.chart = new Chart(ctx, {
                    type: this.chartType,
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            display: true
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    min: 0
                                }
                            }],
                            xAxes: []
                        }
                    }
                });
            });
        }
    }

    public updateChartData(animate = true) {
        // check if chart was initialized and we have some data
        if (this.chart && this.innerData) {
            this.chart.data.labels = this.innerData.labels;
            this.chart.data.datasets = this.innerData.datasets.map((dataset, datasetIndex) => {
                // create "copy" of dataset
                const styledDataset = { ...dataset };
                // if type is scatter / bubble, convert to points, so we can render it
                if (this.chartType === 'scatter') {
                    styledDataset.data = this.numToScatterPoints(styledDataset.data);
                } else if (this.chartType === 'bubble') {
                    styledDataset.data = this.numToBubblePoints(styledDataset.data);
                }
                // for pure line chart, we want to disable fill
                if (this.chartType === 'line') {
                    styledDataset.fill = dataset.fill || false;
                    styledDataset.borderWidth = dataset.borderWidth || 2;
                }
                // if this is a pie/doughnut/polarArea chart, we create an array of colors
                // for each of the data element in a dataset
                if (this.chartType === 'pie'
                    || this.chartType === 'doughnut'
                    || this.chartType === 'polarArea') {
                    let dataIndex = 0;
                    const defBackgroundColors = [];
                    for (const val of styledDataset.data) {
                        const backgroundColor = this.borderColors[dataIndex % this.backgroundColors.length];
                        defBackgroundColors.push(backgroundColor);
                        dataIndex++;
                    }
                    styledDataset.backgroundColor = dataset.backgroundColor || defBackgroundColors;
                    styledDataset.borderColor = dataset.borderColor || [];
                } else {
                    const defBackgroundColor = this.backgroundColors[datasetIndex % this.backgroundColors.length];
                    const defBorderColor = this.borderColors[datasetIndex % this.borderColors.length];

                    styledDataset.backgroundColor = dataset.backgroundColor || defBackgroundColor;
                    styledDataset.borderColor = dataset.borderColor || defBorderColor;
                    styledDataset.borderWidth = dataset.borderWidth || 1;
                }

                return styledDataset;
            });

            if (animate) {
                this.chart.update();
            } else {
                this.chart.update(0);
            }
        }
    }

    private resetChartData() {
        // check if chart was initialized
        if (this.chart) {
            this.chart.data.labels = [];
            this.chart.data.datasets = [];
            this.chart.update();
        }
    }

    private updateChartType() {
        if (this.chart) {
            this.chart.destroy();
        }
        this.initChartJs();
        this.updateChartData();
    }

    onData(data: ChartJsVpData) {
        this.innerData = data;
        this.updateChartData();
    }

    clearView() {
        this.resetChartData();
    }


    /**
     * Helper function to convert number[] to {x, y}[]
     * which allows us to plot scatter charts
     */
    private numToScatterPoints(numbers: number[] | Chart.ChartPoint[]) {
        const points: Chart.ChartPoint[] = [];
        for (const value of numbers) {
            if (!this.isChartPoint(value)) {
                points.push({
                    x: value,
                    y: value
                });
            } else {
                points.push(value);
            }
        }
        return points;
    }

    /**
     * Helper function to convert number[] to {x, y, r}[]
     * which allows us to plot bubble charts
     */
    private numToBubblePoints(numbers: number[] | Chart.ChartPoint[]) {
        // looks like the typings for Chart.ChartPoint doesn't have "r" property for radius
        // therefore we use any
        const points: any[] = [];
        for (const value of numbers) {
            if (!this.isChartPoint(value)) {
                points.push({
                    x: value,
                    y: value,
                    r: 10
                });
            } else {
                points.push(value);
            }
        }
        return points;
    }

    /**
     * Type guard for Chart.ChartPoint
     */
    isChartPoint(arg: any): arg is Chart.ChartPoint {
        return arg.x !== undefined || arg.y !== undefined;
    }
}
