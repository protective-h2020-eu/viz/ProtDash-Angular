import * as Chart from 'chart.js';

export class ChartJsVpData implements Chart.ChartData {
    labels?: Array<string | string[]>;
    datasets?: Chart.ChartDataSets[];

    constructor() {
        this.labels = [];
        this.datasets = [];
    }
}
