import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ViewProviderDeclaration } from '../../lib/view-provider-declaration';
import { BaseViewProviderComponent } from '../base-view-provider.component';
import { BasicTableVpData } from './basic-table-vp-data.model';
import { ConfigChanges } from '../../lib/config';

@Component({
    selector: 'app-basic-table-vp',
    templateUrl: './basic-table-vp.component.html',
    styleUrls: ['./basic-table-vp.component.css'],
    changeDetection: ChangeDetectionStrategy.Default
})
@ViewProviderDeclaration({
    id: '@vp/basic-table',
    name: 'Basic Table',
    dataType: BasicTableVpData
})
export class BasicTableVpComponent extends BaseViewProviderComponent implements OnInit {

    public hasValues = false;
    public headers: string[] = [];
    public items: any[] = [];

    constructor(private cdr: ChangeDetectorRef) {
        super();
    }

    ngOnInit() {
    }

    onData(data: BasicTableVpData) {
        this.headers = data.headers;
        this.items = data.items;
        // tell angular we actually updated something
        this.cdr.detectChanges();
    }

    isObject(value: any) {
        return typeof value === 'object';
    }

    hasHeaders(): boolean {
        return this.headers && this.headers.length > 0;
    }

    hasItems(): boolean {
        return this.items && this.items.length > 0;
    }

    getDataType() {
        return BasicTableVpData;
    }

    clearView() {
        this.headers = [];
        this.items = [];
    }
}
