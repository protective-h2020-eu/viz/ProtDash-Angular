import 'rxjs/add/operator/map';

import { Type } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';

import { JsonDataHolder } from '../data-providers/sample-json-dp/json-dp-holder';
import { MappingProvider } from '../lib/mapping-provider';
import { MappingProviderDeclaration } from '../lib/mapping-provider-declaration';
import { TypeHelpers } from '../lib/type-helpers';
import { ForceVPData } from '../view-providers/d3js-vp/force-directed-graph/force-vp-data.model';
import { HeatmapVPData } from './../view-providers/d3js-vp/heatmap-view/heatmap-vp-data.model';
import { SankeyVPData } from './../view-providers/d3js-vp/sankey-chart/sankey-vp-data.model';

@MappingProviderDeclaration({
    id: '@mp/json-to-D3js',
    name: 'Json to D3JS',
    dataProviderDataTypes: [JsonDataHolder],
    viewProviderDataTypes: [ForceVPData, SankeyVPData, HeatmapVPData]
})
export class JsonToD3Mapping implements MappingProvider {
    private source: Subject<any>;

    constructor() {
        this.source = new Subject<any>();
    }

    input(): Observer<any> {
        return this.source;
    }
    output<T>(type: Type<T>): Observable<T> {
        return this.source.map<any, T>(data => {
            if (data instanceof JsonDataHolder) {
                if (TypeHelpers.typeEquals(type, ForceVPData)) {
                    return <any>this.JsonToForce(data);
                } else if (TypeHelpers.typeEquals(type, SankeyVPData)) {
                    return <any>this.JsonToSankey(data);
                } else if (TypeHelpers.typeEquals(type, HeatmapVPData)) {
                    return <any>this.JsonToHeatmap(data);
                }
            }
        });
    }
    supportsViewType<T>(type: Type<T>): boolean {
        return (
            TypeHelpers.typeEquals(type, ForceVPData) ||
            TypeHelpers.typeEquals(type, SankeyVPData) ||
            TypeHelpers.typeEquals(type, HeatmapVPData)
        );
    }

    supportsDataType<T>(type: Type<T>): boolean {
        return TypeHelpers.typeEquals(type, JsonDataHolder);
    }
    JsonToForce(holder: JsonDataHolder) {
        return new ForceVPData(holder.json.nodes, holder.json.links);
    }
    JsonToSankey(holder: JsonDataHolder) {
        return new SankeyVPData(holder.json.nodes, holder.json.links);
    }
    JsonToHeatmap(holder: JsonDataHolder) {
        return new HeatmapVPData(holder.json.data);
    }
}
