import { JsonToD3Mapping } from './Json-to-D3js';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoToSeriesMapping } from './demo-to-chartjs';
import { NeonToMultiMapping } from './neon-to-multi';
import { NeonTimeseriesToPlotly } from './neon-timeseries-to-plotly';
import { DemoToSeriesMappingTwo } from './demo-to-chartjs-2';

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        DemoToSeriesMapping,
        DemoToSeriesMappingTwo,
        NeonToMultiMapping,
        JsonToD3Mapping,
        NeonTimeseriesToPlotly
    ],
    declarations: []
})
export class MappingProvidersModule { }
