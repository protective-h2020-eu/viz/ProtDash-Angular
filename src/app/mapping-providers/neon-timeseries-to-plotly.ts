import { Type } from '@angular/core';
import * as neon from 'neon-framework';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';

import { NeonTimeseriesDataHolder } from '../data-providers/neon-timeseries-dp/neon-timeseries-data-holder';
import { MappingProvider } from '../lib/mapping-provider';
import { MappingProviderDeclaration } from '../lib/mapping-provider-declaration';
import { TypeHelpers } from '../lib/type-helpers';
import { PlotlyVpDataHolder } from '../view-providers/plotly-vp/plotly-vp-component/plotly-vp-data-model';
import { BasicTableVpData } from '../view-providers/basic-table-vp/basic-table-vp-data.model';

/**
 * Official typings don't include fields inside Query class
 * but we need them for our mapping provider
 * therefore we declare them here
*/
interface NeonQuery extends neon.query.Query {
    aggregateArraysByElement?: boolean;
    aggregates?: {
        field: string;
        name: string;
        operation: string;
    }[];
    fields: string[];
    filter: neon.query.Filter;
    groupByClauses: {
        field: string;
        prettyField: string;
        type: string;
    }[];
    isDistinct: boolean;
    limitClause: neon.query.LimitClause;
    sortClauses: {
        fieldName: string;
        sortOrder: -1 | 1;
    }[];
}

@MappingProviderDeclaration({
    id: '@mp/neon-timeseries-to-plotly',
    name: 'Neon Timeseries to Plotly',
    dataProviderDataTypes: [NeonTimeseriesDataHolder],
    viewProviderDataTypes: [PlotlyVpDataHolder]
})
export class NeonTimeseriesToPlotly implements MappingProvider {

    private source = new Subject<NeonTimeseriesDataHolder>();
    public found: boolean = false;

    input(): Observer<NeonTimeseriesDataHolder> {
        return this.source;
    }

    output<T>(type: Type<T>): Observable<T> {
        return this.source.map<NeonTimeseriesDataHolder, T>(data => {
            if (TypeHelpers.typeEquals(type, PlotlyVpDataHolder)) {
                return <any>this.mapNeonToPlotly(data);
            }
            if (TypeHelpers.typeEquals(type, BasicTableVpData)) {
                return <any>this.mapNeonToBasicTable(data);
            }
        });
    }

    supportsDataType<T>(type: Type<T>): boolean {
        return TypeHelpers.typeEquals(type, NeonTimeseriesDataHolder);
    }

    supportsViewType<T>(type: Type<T>): boolean {
        return TypeHelpers.typeEquals(type, PlotlyVpDataHolder)
            || TypeHelpers.typeEquals(type, BasicTableVpData);
    }

    private mapNeonToBasicTable(dataHolder: NeonTimeseriesDataHolder) {
        const neonQuery: NeonQuery = <NeonQuery>dataHolder.query;
        const neonData: any[] = dataHolder.data;
        const vpData = new BasicTableVpData([],[]);
        const queryField = neonQuery.groupByClauses.pop().field;

        if (Array.isArray(neonData) && neonData.length > 0) {
                vpData.headers = ["Source", "Last report", "Count"];
                neonData.forEach(element => {
                    let selectedGroupByField = element[queryField];
                    
                    if (vpData.items.length > 0){
                        // If groupd array is not empty, search if the field is already added                    
                        selectedGroupByField.forEach(field => {
                            this.found = false;

                            vpData.items.forEach(item => {  
                                if (item.Source === field.toString()){
                                    item.Count = item.Count + element.count
                                    this.found = true;
                                }
                            });  
                            if (!this.found){
                                vpData.items.push({
                                    "Source": field.toString(),
                                    "Last report": element.date,
                                    "Count": element.count
                                });
                            }                      
                        });
                    }else{
                        //If the array is empty, push the first values
                        selectedGroupByField.forEach(field => {
                            vpData.items.push({
                                "Source": field.toString(),
                                "Last report": element.date,
                                "Count": element.count
                            });
                        });                   
                    }
                });
            }else{
                vpData.headers = _.keys(neonData[0]);
                vpData.items = neonData;
            }
        return vpData;
    }

    private mapNeonToPlotly(dataHolder: NeonTimeseriesDataHolder) {
        const neonQuery: NeonQuery = <NeonQuery>dataHolder.query;
        const neonData: any[] = dataHolder.data;
        const vpData = new PlotlyVpDataHolder();
        const queryField = neonQuery.groupByClauses.pop().field;
        const ewmaAlpha: number = dataHolder.ewmaAlpha;
        const hardcodedView: boolean = dataHolder.harcodedView;
        if (neonData) {
            neonData.forEach(element => {
                let selectedGroupByField = element[queryField];
                let formattedDate;

                if (element.hour != undefined){
                    formattedDate = element.year+'-'+ 
                                    element.month+'-'+
                                    element.day+' '+
                                    element.hour;
                }else if (element.day != undefined){
                    formattedDate = element.year+'-'+ 
                                    element.month+'-'+
                                    element.day;
                }else if (element.month != undefined){   
                    formattedDate = element.year+'-'+ 
                                    element.month;
                }else if (element.year != undefined){
                    formattedDate = element.year;
                }
                // When grouping, mongo groups by array, so we have to split the arrays in different groups
                this.splitNeonResultArraysForPlotly(selectedGroupByField, vpData, formattedDate, element, hardcodedView);             
            });
        }
        if (ewmaAlpha){
            vpData.ewmaAlpha = ewmaAlpha;
        }
        if (hardcodedView){
            vpData.harcodedView = hardcodedView;
        }
        return vpData;
    }

    addValuesToExistingGroup(group, field, formattedDate, element, found, hardcodedView){
        if (hardcodedView){
            field = field.match('^[^.]*\.[^.]*')[0];
        }
        
        if (group.field === field.toString()){
            var positionExistingDate = group.dates.indexOf(formattedDate);
            if (positionExistingDate> -1){
                group.count[positionExistingDate] = group.count[positionExistingDate] + element.count;
            }else{
                group.dates.push(formattedDate);
                group.count.push(element.count);
            }
            this.found = true;
        }
    } 

    addNewValuesToGroup(vpData, field, formattedDate, element, hardcodedView){
        // Detect if we are in hardcodedView to modify the names to get the "partner"
        if (hardcodedView){
          field = field.match('^[^.]*\.[^.]*')[0];
        }

        vpData.groups.push({
            field: field.toString(),
            dates: [formattedDate],
            count: [element.count]
        });
    }

    addFirstValuesToGroup(vpData, field, formattedDate, element, hardcodedView){
        if (vpData.groups.length > 0){
            vpData.groups.forEach(group => {  
                this.addValuesToExistingGroup(group, field, formattedDate, element, this.found, hardcodedView)
            });  
            if (!this.found){
                this.addNewValuesToGroup(vpData, field, formattedDate, element, hardcodedView);
            } 
        }else{
           this.addNewValuesToGroup(vpData, field, formattedDate, element, hardcodedView)
        }
        
    }

    splitNeonResultArraysForPlotly(selectedGroupByField, vpData, formattedDate, element, hardcodedView){
        if (selectedGroupByField){
            if (vpData.groups.length > 0){
                // If groupd array is not empty, search if the field is already added                    
                selectedGroupByField.forEach(field => {
                    this.found = false;
                    
                    if (field instanceof Array){
                        // If the field is an array iterate over it..
                        field.forEach(subfield => {
                            vpData.groups.forEach(group => {  
                                this.addValuesToExistingGroup(group, subfield, formattedDate, element, this.found, hardcodedView)
                            });  
                            if (!this.found){
                                this.addNewValuesToGroup(vpData, subfield, formattedDate, element, hardcodedView);
                            }
                        });
                     }else{
                        vpData.groups.forEach(group => {  
                            this.addValuesToExistingGroup(group, field, formattedDate, element, this.found, hardcodedView)
                        });  
                        if (!this.found){
                            this.addNewValuesToGroup(vpData, field, formattedDate, element, hardcodedView);
                        }
                     }                        
                });
            }else{
                //If the array is empty, push the first values
                selectedGroupByField.forEach(field => {
                    this.addFirstValuesToGroup(vpData, field, formattedDate, element, hardcodedView);            
                });                   
            }   
        }else{
            if (vpData.groups.length > 0){
                this.addValuesToExistingGroup(vpData.groups[0], 'All', formattedDate, element, true, hardcodedView);
            }else{
                this.addNewValuesToGroup(vpData, 'All', formattedDate, element, hardcodedView);
            }                
        }    
    }

}
