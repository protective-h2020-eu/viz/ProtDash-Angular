import { Type } from '@angular/core';
import * as neon from 'neon-framework';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';

import { NeonDataHolder } from '../data-providers/neon-dp/neon-data-holder';
import { MappingProvider } from '../lib/mapping-provider';
import { MappingProviderDeclaration } from '../lib/mapping-provider-declaration';
import { TypeHelpers } from '../lib/type-helpers';
import { ChartJsVpData } from '../view-providers/chartjs-vp/chartjs-view-component/chartjs-data.model';
import { BasicTableVpData } from '../view-providers/basic-table-vp/basic-table-vp-data.model';

/**
 * Official typings don't include fields inside Query class
 * but we need them for our mapping provider
 * therefore we declare them here
*/
interface NeonQuery extends neon.query.Query {
    aggregateArraysByElement?: boolean;
    aggregates?: {
        field: string;
        name: string;
        operation: string;
    }[];
    fields: string[];
    filter: neon.query.Filter;
    groupByClauses: {
        field: string;
        prettyField: string;
        type: string;
    }[];
    isDistinct: boolean;
    limitClause: neon.query.LimitClause;
    sortClauses: {
        fieldName: string;
        sortOrder: -1 | 1;
    }[];
}

@MappingProviderDeclaration({
    id: '@mp/neon-to-chartjs',
    name: 'Neon to Multi',
    dataProviderDataTypes: [NeonDataHolder],
    viewProviderDataTypes: [ChartJsVpData, BasicTableVpData]
})
export class NeonToMultiMapping implements MappingProvider {

    private source = new Subject<NeonDataHolder>();

    input(): Observer<NeonDataHolder> {
        return this.source;
    }

    output<T>(type: Type<T>): Observable<T> {
        return this.source.map<NeonDataHolder, T>(data => {
            if (TypeHelpers.typeEquals(type, ChartJsVpData)) {
                return <any>this.mapNeonToChartJs(data);
            }
            if (TypeHelpers.typeEquals(type, BasicTableVpData)) {
                return <any>this.mapNeonToBasicTable(data);
            }
        });
    }

    supportsDataType<T>(type: Type<T>): boolean {
        return TypeHelpers.typeEquals(type, NeonDataHolder);
    }

    supportsViewType<T>(type: Type<T>): boolean {
        return TypeHelpers.typeEquals(type, ChartJsVpData)
            || TypeHelpers.typeEquals(type, BasicTableVpData);
    }

    private mapNeonToBasicTable(dataHolder: NeonDataHolder) {
        const neonQuery: NeonQuery = <NeonQuery>dataHolder.query;
        const neonData: any[] = dataHolder.data;
        const vpData = new BasicTableVpData();

        if (Array.isArray(neonData) && neonData.length > 0) {
            vpData.headers = _.keys(neonData[0]);
            vpData.items = neonData;
        }

        return vpData;
    }

    private mapNeonToChartJs(dataHolder: NeonDataHolder) {
        const neonQuery: NeonQuery = <NeonQuery>dataHolder.query;
        const neonData: any[] = dataHolder.data;
        const vpData = new ChartJsVpData();

        // group by would be our labels (only one for now)
        if (neonQuery.groupByClauses.length === 1) {
            const clause = neonQuery.groupByClauses.pop();
            const groupedBy = _.groupBy(neonData, clause.field);
            vpData.labels = _.keys(groupedBy).map(v => v ? v : '(empty)');

            // aggregates in this case would be our Y values
            if (neonQuery.aggregates.length === 1) {
                const aggClause = neonQuery.aggregates.pop();
                const datasetData = [];
                _.forEach(groupedBy, (values, key) => {
                    const data = _.map(values, aggClause.name).pop() || 0;
                    datasetData.push(data);
                });
                vpData.datasets.push({
                    data: datasetData,
                    label: aggClause.name
                });
            }
        }

        return vpData;
    }
}
