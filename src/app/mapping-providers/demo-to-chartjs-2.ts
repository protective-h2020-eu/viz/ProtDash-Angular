import { Type } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';

import { DemoDataHolder } from '../data-providers/demo-dp/demo-data.model';
import { DemoTwoDataHolder } from '../data-providers/demo-dp/demo-two-data.model';
import { MappingProvider } from '../lib/mapping-provider';
import { MappingProviderDeclaration } from '../lib/mapping-provider-declaration';
import { TypeHelpers } from '../lib/type-helpers';
import { ChartJsVpData } from '../view-providers/chartjs-vp/chartjs-view-component/chartjs-data.model';

import 'rxjs/add/operator/map';
import { BasicTableVpData } from '../view-providers/basic-table-vp/basic-table-vp-data.model';



@MappingProviderDeclaration({
    id: '@mp/demo-to-chartjs-2',
    name: 'Demo to Chart JS / Basic Table (+100)',
    dataProviderDataTypes: [
        DemoDataHolder,
        DemoTwoDataHolder
    ],
    viewProviderDataTypes: [ChartJsVpData, BasicTableVpData]
})
export class DemoToSeriesMappingTwo implements MappingProvider {

    private source: Subject<any>;

    constructor() {
        this.source = new Subject<any>();
    }

    input(): Observer<any> {
        return this.source;
    }

    output<T>(type: Type<T>): Observable<T> {
        return this.source.map<any, T>(data => {
            if (data instanceof DemoDataHolder) {
                if (TypeHelpers.typeEquals(type, ChartJsVpData)) {
                    return <any>this.demoToChartJs(data);
                } else if (TypeHelpers.typeEquals(type, BasicTableVpData)) {
                    return <any>this.demoToBasicTable(data);
                }
            } else if (data instanceof DemoTwoDataHolder) {
                if (TypeHelpers.typeEquals(type, ChartJsVpData)) {
                    return <any>this.demoTwoToSeries(data);
                } else if (TypeHelpers.typeEquals(type, BasicTableVpData)) {
                    return <any>this.demoTwoToBasicTable(data);
                }
            }
        });
    }

    supportsViewType<T>(type: Type<T>): boolean {
        return TypeHelpers.typeEquals(type, ChartJsVpData)
            || TypeHelpers.typeEquals(type, BasicTableVpData);
    }

    supportsDataType<T>(type: Type<T>): boolean {
        return TypeHelpers.typeEquals(type, DemoDataHolder)
            || TypeHelpers.typeEquals(type, DemoTwoDataHolder);
    }

    demoToChartJs(demoData: DemoDataHolder): ChartJsVpData {
        const barChartData = new ChartJsVpData();
        barChartData.labels = ['2018', '2017', '2016', '2015', '2014', '2013', '2012'];
        barChartData.datasets = [
            { data: demoData.data.map(v => v + 100), label: 'Series A' }
        ];
        return barChartData;
    }

    demoTwoToSeries(demoData: DemoTwoDataHolder): ChartJsVpData {
        const barChartData = new ChartJsVpData();
        barChartData.labels = ['2018', '2017', '2016', '2015', '2014', '2013', '2012'];
        barChartData.datasets = [
            { data: demoData.numbers.map(v => v + 100), label: 'Series A' }
        ];
        return barChartData;
    }

    demoToBasicTable(demoData: DemoDataHolder): BasicTableVpData {
        const numbers = demoData.data.map(v => {
            return { 'Value': v + 100 };
        });
        return new BasicTableVpData(['Value'], numbers);
    }

    demoTwoToBasicTable(demoData: DemoTwoDataHolder): BasicTableVpData {
        const numbers = demoData.numbers.map(v => {
            return { 'Value': v + 100 };
        });
        return new BasicTableVpData(['Value'], numbers);
    }
}
