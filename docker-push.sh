#!/bin/bash

set -ex

IMAGE_NAME="protective-h2020-eu/viz/protdash-angular"
TAG="${1}"

REGISTRY="registry.gitlab.com"

docker push ${REGISTRY}/${IMAGE_NAME}
